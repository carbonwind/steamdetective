![SteamDetective logo](logo.png){width=100 height=100px}
# **SteamDetective**

Userscript that displays a quick summary of a person's reputation and additional information on their Steam profile.

## Discord server
https://discord.gg/NXA27dqy3k

## How to install

**Prerequisites:**
- A modern web browser (i.e Firefox, Chrome, etc). This means no Internet Explorer.
- Tampermonkey (https://www.tampermonkey.net/)

Once you have both prerequisites, simply go here to install the userscript: 
https://gitlab.com/carbonwind/steamdetective/-/raw/main/SteamDetective.user.js

A prompt asking you to install the script should show up.

## Project status

- Maintenance updates are done whenever a bug is found or changes break the script's functions.
- Feature updates may be done if the usefulness of the information in them is important enough.
- Some parts of the code are refactored if the current code hampers development of new features.

## Contributions

If you have programming experience and wish to help with cleaning up the code (refactoring) or adding new features/modules, you are welcome to fork this project and create pull requests.