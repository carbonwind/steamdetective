// ==UserScript==
// @name         SteamDetective
// @namespace    https://gitlab.com/carbonwind
// @homepageURL  https://gitlab.com/carbonwind/steamdetective
// @supportURL   https://gitlab.com/carbonwind/steamdetective/-/issues
// @version      1.31.2
// @description  Displays a quick summary of a person's reputation and additional information
// @author       carbonwind
// @match        https://steamcommunity.com/id/*
// @match        https://steamcommunity.com/profiles/*
// @exclude      /^https?:\/\/steamcommunity\.com\/(?:id|profiles)\/.*\/.+
// @exclude      /^https?:\/\/steamcommunity\.com\/(?:id|profiles)\/.*(\?xml=)
// @icon         data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAAAAAA6mKC9AAAAWElEQVR42oXPwQnAMAxD0VwzpsbxEFrkT1ejkgZCoD4I/MDYHvOocQcZrA2mpMILTIc08QsiCBMFXA2NqBxoTyDxgQB3rhGRneVAO+kn2mvT+zzsfvrvtw8r4zYhV5URUAAAAABJRU5ErkJggg==
// @run-at       document-end
// @grant        GM_addStyle
// @grant        GM_xmlhttpRequest
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_deleteValue
// @grant        GM_setClipboard
// @connect      backpack.tf
// @connect      mannco.store
// @connect      firepoweredgaming.com
// @connect      skial.com
// @connect      steamcommunity.com
// @connect      steamhistory.net
// @connect      steamrep.com
// @connect      steamidapi.uk
// @connect      steamtrades.com
// @connect      rep.tf
// ==/UserScript==

// Ignore ESLint warnings for BigInt and Steam's variables/functions
/* global BigInt g_steamID g_rgProfileData ShowAlertDialog */

function getID64FromBinary(p_binstr) {
    const ID64 = BigInt(`0b${p_binstr}`);
    return ID64.toString();
}

function getID64Bits(p_id64str) {
    const POW2_32 = BigInt(2 ** 32);
    let id64_num = BigInt(p_id64str);
    let lower_bits = id64_num % POW2_32;
    let higher_bits = id64_num / POW2_32;

    return {
        lower_bits: Number(lower_bits),
        higher_bits: Number(higher_bits),
        lower_bits_str: lower_bits.toString(2).padStart(32, '0'),
        higher_bits_str: higher_bits.toString(2).padStart(32, '0')
    };
}

function parseID64(p_id64) {
    const NORMAL_BITS = "00000001000100000000000000000001";

    let bit_result = getID64Bits(p_id64);
    let account_id = bit_result.lower_bits;
    let y = account_id % 2;
    let z = Math.floor(account_id / 2);

    let id2 = `STEAM_0:${y}:${z}`;
    let id3 = `[U:1:${account_id}]`;
    let id64, is_malformed;

    // Fixes the ID64 if the higher bits aren't expected
    if (bit_result.higher_bits_str != NORMAL_BITS) {
        console.log(`[DEBUG] | parseID64() -> Got high bit value of ${bit_result.higher_bits}, expected 17825793`);
        id64 = getID64FromBinary(NORMAL_BITS + bit_result.lower_bits_str);
        console.log(`[DEBUG] | parseID64() -> Malformed ID64: ${getID64FromBinary(bit_result.higher_bits_str + bit_result.lower_bits_str)} | Fixed ID64: ${id64}`);
        is_malformed = true;
    } else {
        id64 = p_id64;
    }

    return {id2, id3, id64, is_malformed};
}

// Profile information
const parse_result = parseID64(g_rgProfileData.steamid);
const profile = {
    username: "",
    id2: parse_result.id2,
    id3: parse_result.id3,
    id64: parse_result.id64,
    original_id64: g_rgProfileData.steamid,
    has_fakeid64: false,
    has_malformedid64: parse_result.is_malformed,
    custom_url: "",
    is_vacbanned: false
};

// Settings related to the script
const DEFAULT_TIMEOUT = 15000;
const steamdetective = {
    agedisclaimer_enabled: GM_getValue("agedisclaimer_enabled", true),
    details_onstartup: GM_getValue("details_onstartup", false),
    forcedfallback_enabled: GM_getValue("forcedfallback_enabled", false)
};

// Module properties
// - id_prefix: Prefix to use in HTML IDs
// - module_name: Name to use in the user interface
// - summary_name (optional): Name to display in summaries, if not set will use module_name
// - summary_enabled: Indicates if a module will show up on the profile summary (right side)
// - requires_apikey: Indicates if module needs an API key (for error messages)
// - api_timeout: Time to wait for a response (in milliseconds)
// - api_key (optional): Property used to get the API key (if applicable)
// - profile_url: URL to use in summary
const steamrep = {
    id_prefix: "sr",
    module_name: "SteamRep",
    is_enabled: GM_getValue("steamrep_enabled", true),
    summary_enabled: GM_getValue("steamrep_summaryenabled", true),
    history_enabled: GM_getValue("steamrep_historyenabled", true),
    eolmsg_enabled: GM_getValue("steamrep_eolmsgenabled", true),
    requires_apikey: false,
    api_timeout: GM_getValue("steamrep_apitimeout", DEFAULT_TIMEOUT),
    profile_url: `https://steamrep.com/profiles/${profile.id64}`
};

const bptf = {
    id_prefix: "bptf",
    module_name: "Backpack.tf",
    is_enabled: GM_getValue("bptf_enabled", true),
    summary_enabled: GM_getValue("bptf_summaryenabled", true),
    force_limitedapi: GM_getValue("bptf_forcelimitedapi", false),
    requires_apikey: GM_getValue("bptf_apikey", "") && !GM_getValue("bptf_forcelimitedapi", false) ? true : false,
    api_timeout: GM_getValue("bptf_apitimeout", DEFAULT_TIMEOUT),
    api_key: GM_getValue("bptf_apikey", ""),
    profile_url: `https://backpack.tf/u/${profile.id64}`
};

const mannco = {
    id_prefix: "mannco",
    module_name: "Mannco.store",
    is_enabled: GM_getValue("mannco_enabled", true),
    summary_enabled: GM_getValue("mannco_summaryenabled", true),
    requires_apikey: false,
    api_timeout: GM_getValue("mannco_apitimeout", DEFAULT_TIMEOUT),
    profile_url: `https://mannco.store/store/${profile.id64}`
};

const reptf = {
    module_name: "Rep.tf",
    is_enabled: GM_getValue("reptf_enabled", true),
    api_timeout: GM_getValue("reptf_apitimeout", DEFAULT_TIMEOUT),
};

const marketplace = {
    id_prefix: "mptf",
    module_name: "Marketplace.tf",
    is_enabled: GM_getValue("marketplace_enabled", true),
    summary_enabled: GM_getValue("marketplace_summaryenabled", true),
    requires_apikey: false,
    api_timeout: reptf.api_timeout,
    profile_url: `https://marketplace.tf/shop/${profile.id64}`
};

const scrap = {
    id_prefix: "scrap",
    module_name: "Scrap.tf",
    is_enabled: GM_getValue("scrap_enabled", true),
    summary_enabled: GM_getValue("scrap_summaryenabled", true),
    requires_apikey: false,
    api_timeout: reptf.api_timeout,
    profile_url: `https://scrap.tf/profile/${profile.id64}`
};

const firepowered = {
    id_prefix: "fp",
    module_name: "FirePowered",
    is_enabled: GM_getValue("firepowered_enabled", true),
    summary_enabled: GM_getValue("firepowered_summaryenabled", true),
    hide_inactivebans: GM_getValue("firepowered_hideinactivebans", false),
    requires_apikey: false,
    api_timeout: GM_getValue("firepowered_apitimeout", DEFAULT_TIMEOUT),
    profile_url: `https://firepoweredgaming.com/sourcebanspp/index.php?p=banlist&advSearch=${profile.id2}&advType=steamid`
};

const skial = {
    id_prefix: "skial",
    module_name: "Skial",
    is_enabled: GM_getValue("skial_enabled", true),
    summary_enabled: GM_getValue("skial_summaryenabled", true),
    hide_inactivebans: GM_getValue("skial_hideinactivebans", false),
    requires_apikey: false,
    api_timeout: GM_getValue("skial_apitimeout", DEFAULT_TIMEOUT),
    profile_url: `https://stats.skial.com/#summary/player/SteamID/All/${profile.id3}`
};

const steamhistory = {
    id_prefix: "sh",
    module_name: "SteamHistory.net",
    summary_name: "SteamHistory",
    is_enabled: GM_getValue("steamhistory_enabled", true),
    summary_enabled: GM_getValue("steamhistory_summaryenabled", false),
    requires_apikey: false,
    api_timeout: GM_getValue("steamhistory_apitimeout", DEFAULT_TIMEOUT),
    profile_url: `https://steamhistory.net/id/${profile.id64}`
};

const steamiduk = {
    id_prefix: "sid",
    module_name: "SteamID.uk",
    is_enabled: GM_getValue("sid_enabled", true),
    summary_enabled: GM_getValue("sid_summaryenabled", false),
    requires_apikey: true,
    api_timeout: GM_getValue("sid_apitimeout", DEFAULT_TIMEOUT),
    api_key: GM_getValue("sid_apikey", ""),
    my_id: GM_getValue("sid_myid", ""), // Exclusive to SteamID.uk, required for API requests
    profile_url: `https://steamid.uk/profile/${profile.id64}`
};

const steamtrades = {
    id_prefix: "st",
    module_name: "SteamTrades.com",
    summary_name: "SteamTrades",
    is_enabled: GM_getValue("strades_enabled", false),
    summary_enabled: GM_getValue("strades_summaryenabled", true),
    requires_apikey: false,
    api_timeout: GM_getValue("strades_apitimeout", DEFAULT_TIMEOUT),
    profile_url: `https://www.steamtrades.com/user/${profile.id64}`
};

// Base styles for console logs
const log_styles = "background: black; font-size: 14px; padding: 4px;";

// HTTP status texts
const http_statusnames = {
    400: "Bad Request", 401: "Unauthorized", 403: "Forbidden",
    404: "Not Found", 405: "Method Not Allowed", 408: "Request Timeout",
    414: "URI Too Long", 429: "Too Many Requests", 500: "Internal Server Error",
    501: "Not Implemented", 502: "Bad Gateway", 503: "Service Unavailable",
    504: "Gateway Timeout"
};

// Controls how specific statuses are treated:
// - light: For statuses that are temporary or if a required API key isn't set.
// - no_account: When a site explcitly indicates there is no account or data.
// - warning: For anything that needs more attention.
// - danger: For bans.
// - special: Special roles, currently only used for SteamRep.
const statuses = {
    light: ["Unknown","Connecting","Loading","API Key Required"],
    no_account: "No account",
    normal: "Normal",
    warning: ["Caution","Unbanned","Not indexed","Missing data",
              "Unexpected data","Rep.tf get data fail","Deleted account",
              "Feature Banned",
              // SteamID statuses - Uses shorter names for summary
              "Database connectivity issue", "Database issue",
              "Invalid ID64/API Key settings", "Invalid settings",
              "Daily lookup limit reached", "Lookup limit reached",
              "API Key not set",
              "ID64 not set",
              "ID64 not found",
              "API Key Disabled",
              "Internal API error",
              "Unknown error",
              // xmlHttpRequest statuses
              "Timed out","Application error"
             ],
    danger: ["Banned","Scammer"],
    special: "Admin",
    is_httpstatus: function (p_status) {return !isNaN(p_status - parseFloat(p_status))}
};

// HTML elements (legacy)
var username_label = document.getElementsByClassName("actual_persona_name")[0]; // Steam username in profile page

// Logs variable of logged in user's ID64 in the console
console.log("%c[DEBUG] | g_steamID: " + g_steamID, log_styles);

// Website icons (all stored locally as Data URIs to avoid tracking and improve loading times)
const ARCHIVEIS_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAA3NCSVQICAjb4U/gAAACZklEQVQokW2SXUhTYRjHn/ecszPPkp05I2tJsrn1ZWXLibA0EBrUTWMXC7ULky4jr4IkchcrJt1EF13Joo8bzUEbjKi8WjkEWezDoKmRW+LamNtu2gfnnL29XZwlOnpunueB/+953+cDEUIAwOv1UhQliiL8z1QqFc/zdrsdABgACIVCqVRKkiSLxYIxblLTNB2NRrPZrCiKTqeTAQBBEAwGQzqdBgCEUBNACGlra7NarZIkNV7Q6XSJRCKZTPI8jzFmlSyFKACoY1yXJI7jpqamAoFAuVwGACT34HK53G63XHLpXShfLQCFLhhP63tPAYAoil6v12azCYJAySKdTgcApWxpePrybPcHX9fas/znB9r0bY8LAFiWrdVqJpNJkqQGQIAAwOC9wXaLZvPViqGvs298OPP044ax487jR7v9mM1mRo6UrHJudu7q6ytcXVXb2dxa+k6xB5Vn2aHrEyuxr5m1dYZpKHcdHVXH1dCRif86M3KxpbULwSHIf0PAmc0Dy3PzDE3vA4BC1bSYns+Ukjn1Kq3gSohS58IbuMZWEHWOoeHfuBvAH4J7VN2akS6hVs2v5jTHDbSyvc61nrw2tl4q6mPxbC63DxBE8db4xNH7E5cGzqcWI5abRsTgrYWQUFfTkWjfzEwoHm98RXYYYyV/IHLjYYZW99+dbuk8lvnxc+jF88qX2MvJyb2LZ/Ym+p4Tn44cfrv4fjkc7tfre98sPPF4mi6lASgUiu3t7UgkgjFu12jGRkcxxr/LZZ/PRwhxOBzNQKVSCQaDPM8DQKFY3CkU5CtECBFC/H6/VquVlX8BvK//yDJEbNYAAAAASUVORK5CYII=`;
const ARCHIVEORG_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAA3NCSVQICAjb4U/gAAABWUlEQVQokX1RQarCMBCdGaXWqvfwDLpyI+gFvIfeQnDlLbyAeACLBxAXdi0IXUSFlpY0mb+YkJ9f4b9FGF7mvcybICICgD+NMfAXRMTMUqOAiHzfYrGYzWbz+TzLssvlstvtvNKbIjNvNpvxeDyZTOq6bprGGENEiBjHcZ7np9Npu926blHfbreyLK218AVETJJktVpdr1dE7MoLh8OhqqrhcFiWJTMnSVJVVdM0o9GoKIooip7P528mANjv94/HAwDSNE3TVBillDDn81k6EbErGxBXKcTIWutrzwCAW5m1thXAC1o8facMR/XbbAtCttXRYkicmDn88jCMr11oGVEplec5ALxeL7lWSn0+HwB4v99haPfTx+OxKIp+v1/XNTPHcay11loL0+v11uv1/X53a10ul1mWGWP8YCGYudPpTKdTJ5BXBoOB1rqVLwwWRZEL8235D4joB1rD3NJ+rOl5AAAAAElFTkSuQmCC`;
const BING_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAfZJREFUOI2Vkz9oU1EUh7/30uSlf15T0GAjlhYpWDAWoaBgwUHBRTe7unQsSNWhi+BqHdx0rJMIgg4VRFrBySVUKuJQO4gStLFNja9pXpP33r33OKS1CaYSDxy45/7u+fide7mWiLAXLwtMCAxeyXCfdkNE/iSvZWF0wRcWZaWwI/FG7aC0G2HHIiJX2YxHtZHMIsV2DHQ0FroKxq4zM0GQAuf/AFLb35HAasdA8wimVodIrb4GsOZZmV7mWb7SDqAKEtpIaGOCXSlB9MYzVweXQjP1mYffon8AdAXEN4hv0NsGgBdZnpQtzYijrMe/SlMDJT4U5ACA8kFXNLqiUdsKgFN9zOZFE3QoEk5EeiM/umq43hIQboGUNVLWhJ4GYMiF+Wyy/0t/7/NOp9sb7jbcjHGpJSCogPIjlB8RbO8PO97F+uw7/HCm0Fec08FR/Nie1vSMbBGL1G6jV7+D2zlmnuY277olY6cHelh9W3VuoD+1dHDnGuGPrz7++g7DUWjO5/i49N27N9Rl2V1OnEQ8gUolmCB+q6WDMxeYHAkOb7pRR9U9kXCUX82qmEVg2Wgs3nuHltfmGDvS0NPk4LLLz4tn3Un7dLJzx4jta6goWAuQc8f7puURY5l009BYjd95L14VIQkP0PToiI2TKWYyvX8dA+A378gC8sqr9y4AAAAASUVORK5CYII=`;
const BPTF_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAqFJREFUOI2Nk8trVHcUgL/zu3fuXJ1mNI1R8VFp8FFRE7VBsaaIiqBiF6IoUmqRlm4KoitRaBZVUETqJgtF/AfUqptAKSpiDaJGLanGxMQMYXxMY5yYTDTJzNxzXIyYilI8q/PgPDjnO2KmfIwYIAgi8o5fuvutCpAPZgEioEBkBFVJ7jOYRcoqRuPb6xvsrfHfbiKMjBTo7csiKGXJ8SysmvbPLzWvqklMQWrWAuDiMb8YeI7A98gXChQKeeK+x8jwMHVzp3L+6F7+PLafVfM/oyOTXdCQqrxJtgNrPlsqYGZEkZJwBXasW0bd3Onki0rCi/h5+2YSY0IuXr1Btn8QD+VWx+PaE5nPm7A81noJZ0Aq/Yht61ey5uulJMs+oa0rRfXMGQDcbX/IdweO0/ooiwCiRZra0l+dSFdeJJtCtuz5rfDThuX+6rol/3uFTTt/ZVx5BWqKAEXxWbt4VqPzYx5/NbfwtKcXgNYHXZxuvMDtu20APMu+4Nwfl3CeR4SVLoKBQM/AUMb5IlzryNDZnQbg9r12jjde52pzCwCZnl6OnLpAmEiixQiLFFWYNan83u7J7T/6quA7wXMeAJ5zjI0HxLyS7ZwwNhZgplipMfEwPlRf/XI+3kScmmJmqJaIjAcxHj5+ilHCwwzUFFPDLCLyYpzcsmA9kSDLvsepQRiGtLR1ArBx3SrOH97N0PDI2wnUQM0oSsCGL2cfpPvWZVnxAwC+mRL4jjNX/mZiRTmL5s3heV8/fQODpNJPSD/5F99zREDtjAlN34Z39hFWjqL8za5Dxht0c6+GeJEbJAziJMKA5wM5wiBG5afj8dHc7yvzSSZ9gSzZOlqgKzf6TKol5c06cK6kq8ObXaYPyHQiU+a8w4eYGh8nhjj3nvc1RtIbNJNjfhMAAAAASUVORK5CYII=`;
const DEFAULT_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABs0lEQVR4AWL4//8/RRjO8Iucx+noO0O2qmlbUEnt5r3Juas+hsQD6KaG7dqCKPgx72Pe9GIY27btZBrbtm3btm0nO12D7tVXe63jqtqqU/iDw9K58sEruKkngH0DBljOE+T/qqx/Ln718RZOFasxyd3XRbWzlFMxRbgOTx9QWFzHtZlD+aqLb108sOAIAai6+NbHW7lUHaZkDFJt+wp1DG7R1d0b7Z88EOL08oXwjokcOvvUxYMjBFCamWP5KjKBjKOpZx2HEPj+Ieod26U+dpg6lK2CIwTQH0oECGT5eHj+IgSueJ5fPaPg6PZrz6DGHiGAISE7QPrIvIKVrSvCe2DNHSsehIDatOBna/+OEOgTQE6WAy1AAFiVcf6PhgCGxEvlA9QngLlAQCkLsNWhBZIDz/zg4ggmjHfYxoPGEMPZECW+zjwmFk6Ih194y7VHYGOPvEYlTAJlQwI4MEhgTOzZGiNalRpGgsOYFw5lEfTKybgfBtmuTNdI3MrOTAQmYf/DNcAwDeycVjROgZFt18gMso6V5Z8JpcEk2LPKpOAH0/4bKMCAYnuqm7cHOGHJTBRhAEJN9d/t5zCxAAAAAElFTkSuQmCC`;
const DISPENSER_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAABRFBMVEUAAACEExCDEg+MHRtmERJnEgxwExJzFhlyFRhqFRRhFBBQCwdVBwp9Cgt7Bwl6BAB4AQCIKihmGhk/CwlSDQphCw2eDAmcCgibBwCoODXjxcTVt7a3m5noyci3Yl+nMC2sNTauNzhvGx1DBwqbBgfszczct7jlwMHbvbzq1diUDgvHf4HpysnixMLCeXWvNzOTDAqNAQCiMzHYs7TgwsDZu7qVEAySCgnNi4nrxcZuGReTRz2QCAiVHRpvKSd+VU7PlJbMkpS2nI7RlphsGBZZDAdtS0JqCw1vGhhxMSttWFBHCgZbDglkDxBQGx5iDAddERJKIR5kDwk5BQh/DARqHRxrHx1wIyBwJCWGFhdqCgRPJSKVPj9APDo6NjU7NzZSKSp0FhROEhA2MjE3LS40MC81MTBRGxpbFxM+AwNIEw1IExLfQvxRAAAAAXRSTlMAQObYZgAAAKtJREFUGBkFwTFKxFAUQNF7n+//6CRkZIhgKa7AfdiKhStzNZaCja7BatqAgxll4vccEZsgAKymAiACaq+CoIA6qaqqqk46qrSl9zs0gFoXYFsO2xECqPVqYLyYWWYIuKGVsuMIxwECfruOn8/9DJyucfI2T0UV1Y9Y7t77y7d1s8k8O/cV79UG4Qrp1+gD2oAQQHxSAfQPQh0eVbAhtDAOXS2ZL1lKZpZ8/gdaDCWUu0mvMQAAAABJRU5ErkJggg==`;
const ETF2L_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAwtJREFUOI2lk11sU3UYxn///zk97Wk3u3Vrx+bYHOB0E+cIG2zihhPIAtFMDd6oEOON4oUxi4RkEomaoKbqhVGJ2fAG0RCDERQVUZkJs4kDg3GubKOjzT5kXbcxdtLu9OMcL2QKeslz+SbP8z7J+3vhJiX+OwiGbI6OoLzVyjq3i6HvRkn7s1MPeopKzmiS8UdrbrSoS6YCN6RtNKdAydewPS7qNZW4LXD2HT+4c/uzXYkvIyTe6bczQK6zUfzbIBiyaV8JyQxrvC5W1RSJo22f2I7TETJvb8GVp1ImBcb0r8f21t3fcaLCyzcnI7C7WSCDIRuAd39BZi2i2PQBorUU8/XNYApSzzSI0WQGM25ko7F5knU7UGxwBEO2Q1wLKPr2w72dxtz0Mp+vkMMfvJHyOpk90Tfo3bS+1uwPxwr2vLxfkVLiL/ZhAYnEjNDzC+MqwKXfzrTFBkIv5LJZ910VzWSMGUbG5/ns8EFqb32eYs1EtxaYmLhM/YoNzF81GBobtv2Vtd0q4On/6qPttmW5pZTMzl3h/Z5DDISHCQ9f5MgXX9Pe1kJV5XJiYxMMDl0kk80iVW3ykRffO6Raudy6+emJjQBSSpoa1rC2fjUjkShCCCKXYnycmOHCcARFkQghWFw08QbKe204pxizU/uMufgGQFiWRUmgmKcffwxFUcjzeHitq5PSEj/f/9SHaaZpaW5EczqTd2zauf+28vLfVX9F9R/uW3xvSilxOKRq6gFsh57ZvHWbtXHLNlHgIVN5Z72rsKJGsx3j1VctfbLhgYdjtzc29WoClq5AKg2leYizU+jRcyfv9a1sHzyV5s/EU+hwRQv2F1Sf7dlzoHHrEwfurqvraV9xHUhLMGUs1MEfj+xIjv68z5kf+GH5Qy+9unj+0ydT09EWpeq+7tzlgbVNHbtO6xqnJhb+Bkm9nmuPg+yyVffEw+Fe018VmDQFC2PRkVLLmCla39FyoWtX6+fdrzxng/3P7hsaXFOZkaNMlxi6RsTMsHoxjStf43wWUrub//d/N6e/ABSTOJ9mBF3zAAAAAElFTkSuQmCC`;
const ETOOLS_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAA3NCSVQICAjb4U/gAAAByklEQVQokY2RzWsTURTFz0y+qpNCxpHgKtKFJVqTIBXSGgW71oWFSEW6EDHt0l0RhCwsLYLgwmWpH+BS0P8gFRFbSaxaXKhQNYItNkrS5nMy8+a4eGNoa4Qe7urB7553zlUyg5PljxXsTXo0hLQ2xj0rrY15/8LOesWceLhSFUrdErEDgduXB8K9PoKd9Qo8AFyHtXJDu5HbbLblpkqzHb258HOzRQqHthzp4ALn7y5ttdok8wuFfK5A8le1lZlbJrkL8EIBgKaN3oCv8PzNzMgsgOyL7IkziULJ/De3m6FuCQAgCBD0qgCw36d2AwgAR42ejap5cmRw+mWWQCyV+P673hfsArhPd8YHjk2/+lFuxFKJeCrxtdSIGJq+z//sddEtp+OgqABgaL7VW6enHn/Il8yaJU6F/UYwcO9KfOLBClAcTR4mhEvIlixhk/b2Gz1ZLF6bXyaZuf/+6dK3zuHcL3lUGVcQwnYEINJDkXPHjcn5t3NX47lPW4ufN3a0tF0StoRzIRkhcP3Ru6EjB1fXasP94e6AlN9DUzijycghvefLem38bN8Oh11VSAU8ADDcH5a7XUCPhi4GL/3PR1FBQlFABwD0aOgPV/QYSI9k7OEAAAAASUVORK5CYII=`;
const FIREPOWERED_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAA3NCSVQICAjb4U/gAAABtUlEQVQokZVSPY9SQRQ99848zZPNrkQ+3E0gYqIxMQtGC2yxtbKwpPA/yA/Q+BPo4AeQGCtaChOSzYtBuqUwFNK4FBbq233PPJl7LYZFV2KxU01u5pxz55xDzWYTlzlWRC4HUFVVJaL/vVDVCwAn0rgRHuQsQwACFIDslsyt+yrCzKvVarlcTqdTAMxsneizfX66FwsZhbIyVOThveDFK6gSkdefz+ftdjtNU1MqlRybz9nVaWwnP+y1MChzpgd3zIMnSZL0er3FYlGr1crlchiG4/HYinPvT34e7ewBAGj/+pXD8MyvmyRJt9sNgqBQKLRarUqloqrWqT6v5l7fjSEMAA7u3DZrbb1eLxaLjUYDwGw2c85ZFfn0ffXu646CBfooJzVOPCCfzw8GA29gFEX9fh+AVZGPJ98+fHEEJsWbxzdv59cKaZoOh8Msy6IoGo1GIkJE6+AMAAgpEf7kGMdxp9MhIiIyxnjHLiTNqqKq4F+qFlBVZmZmH59P0HohP3Kgt/N4shu45cQcvzw7Pd0uDlWr1U0vNh35pyye20/sRkvPc/UXz70NW//hb44NcruIRPQbXDP5e/4Mvl4AAAAASUVORK5CYII=`;
const GHOSTARCHIVE_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAADFBMVEXx9vzD1PmQr/BbgdNH4I8iAAAAWUlEQVR42mPYo7XqF8MfrVX2IIKf4Q8XkPjLtFKf4W9DynqG/0+41jPs/7JqP8Ny79//GEL5/61iCHVg4GJYOoGBkWGZ1v4FDP///1vC8GX9qxUMT7J/LQIAizglEuKez2AAAAAASUVORK5CYII=`;
const GOOGLE_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAglJREFUOI2Nk99L02EUxj/vNkthRdToFxLNJqaJESYVXtSN3ZS1CqMM6qKLQKJ/oPsF3eV1F1HRBrVcGV4FdRPeJUK0oeYaUmyzRbOxpvtuTxd+v7atIA+ci/e8z3nOe57zHmgwSQFJEUkzksq2z9ixtkZ8bSKSxvR/i0r6Z3LCQRRfxZS7claZ7u3KHPApd3lQxfE67rhDYmyCMSCo5RLfh89QSc6A2w1WebWCZwNaLtLU08fWxy+cus+NMRecniVJi+dOKtPfrUzPLhUij2TNz8lKzqkQfqjs0XZV8j8a2/FjiyPrQ1iZI61Kd+yUtZBahxSSpDC2wrImA1qZQMXo6HqTJSnhAfwAys9h3NA80FsnsLn1kw6vWTv/qkCwy829ay0AbZ46sMBUGkZUY9uaDW4DtQgXkATwbG6nBEwsxOtHPLqJRMhLIuTl4mEPhbLY53M51/Mu4D1AevcdDi6e4NRUhM9LX/6qns3D/UmLchX6O9cePmW0+j0/AQzErvO1mOVbpcjdQyMca+3FZVxMp6e5/SaFL32DPT6LJzdbHAK/85GiwPlSdYXg+Agfl1J4jJuyKghoMi5kCuzQcd4OhWjZCMAzY8yQQwAQB/YDvJx9zYPZGO9ycapA35YAVwOnudQ56FSOA13G/JmOsw/Rdcz+qRqXqU51yS8prNXlWrE9Ycf2NuJ/A6uf5JCErH2FAAAAAElFTkSuQmCC`;
const INVENTORYGIFT_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAIVBMVEX////y8vL///+IiIjt7OxpaWn////GxcWkpKQwMDAEBARrLJjqAAAAB3RSTlMEO5zl8Pj9kGzU+gAAAIJJREFUeNpjYGAQcktRZAACpjQgUGBgYFBLS0s3TQILlM5aEa7AIOYxq3LV1FmBDGpRK71WrVq1kMFt1dKuiMpVkxjSV01PjwyJMmPIaGmbtWpVexmD66wpq1atWpnEoLUqYtWqVUuTGKRWzQKJBDKwWpS7uLiUG4CsAIIkJEthzgAAHfku50U+VlEAAAAASUVORK5CYII=`;
const MANNCO_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAsdJREFUOI19kGtIk2EUx//Pe9k0Cy/RzTmXhguWSNQixSIYoQ0jyoj8Fgu7kEQUswILsqK+FBUVgZXEDDS6mOVGpbUSMygKs1olpF3casuttTZve9+dPgzXjNYPHnjO4Xl+nPMHALS2Emy26LHfJixfvqHw3g1KAwCbjWA0btHX1FzX2O2Ev2E7t18ssXdcvexyeYWkJAWJIk8et3/qpBRlSKNRPQkEQlqX06MWRX4sJ0ftqD10c2lZGQvFDPnzSi5VVu6it45estnaqampmXw+P715/Y5KSyqors5Cg4M+6uv7RFXb9lBBfukniyVuEm2eob7afIDGiURiVxoZGaV4ZDlCM6bpaeumM2vG/3MMoAhFAADBYAgcp8RG0w4AgFKpQEbaAszTGaKPOYY8bRYGBj5kxwTxgfh8fgC5eOvoi/XCYRmfP3onBscQ/qeAMQYAEEQ+1hMFDgrln5pJEoaHfPrWVsoCAI4AxnFRjyAIE0TRAuDk6IpfIkBYp8Og/6updt+K/qqqupVCSkqS5Bz4Bqu1DW73d2jUqZDCEqzWdkiShIy0ZKROT4Ox142nP0aQvasGIAVk01qh//2z+ezY0UeLmm+dPe/x+JIVCp7NmZPX5f/p1bicnukAYQo/NlcwVfBs7TrQs25IDQ2AVgfxfpt/7+EH6XGzTsRsthge3bVckwK/0rnihVAcPAh5SILU7wRZW8B3PBzeva9ZzycSKCR2bdhgyOXM1YhcrEfkVQ9Ypgo0KxukXwZ69Vzsbm+UuUQCjufDbCwEPhyAsKoUfPlqyPUXgNERQJaATDVGg/6pCQWFS9ftFzse+8PrK8EvXgS+uBjCydNAajrwwwvWacdsXVFXwgwAoKWFcO7s5kaP42UFK5gLzMwEeX1ATw8mq3LudHQ2Gf8rGOfEiRdFt64cWR0MeFXK5MnexUvK7cdPmW4CwG/w4EhqFFDJLQAAAABJRU5ErkJggg==`;
const MPTF_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAAhFBMVEUvl9sxl9syl9szl9svldwAvZshjthHcEwwltsyl9szmNwRfdsLp4kzl9spk9szmNslj9cGnX4LpYgNu5otldozmNwNsZKtwrsfPv8Huo+RyroMvJoAuIeHybM1m980mNw0md02nOE3n+Uav58PporV09SIvq5IrJRnxKueybyzzMS90svC3L2oAAAAHnRSTlOmyeHurv5WALrS6jvy+HbcYEvf5pzu8+8vgfvJaGh79MtMAAAArklEQVQYlVXP2xKCMAwE0CitoYDijUG8bJsCXv///2yQF/chkz2TlxCn7Kqqattqpzvp2A/DcB8f+wlWmxUf+t7ex+eBU6E8RsMJXqNlE2NOObycut6+z91JPCYIBV/s58JFUFjAezFsz2zEeywUACmvt1IABQccHTLmDO4IOHIBxBBjBEwIjmoFSudCCjUtFbgJoWGF5QzbGLczFF6By1L/gC8o+8GUBBmt/2H9BQA9Ebsy06ZIAAAAAElFTkSuQmCC`;
const REDDIT_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABn0lEQVR4AWySA6wdURRFp2ZcRTVi1w1q27YdJ7Vt27YV1HYbfNs2HtY/N+cmj5Os8d6Hjv9BX6eOMEC4JiQJLkuycF0YaP7xKQLFzYSDQq4A4cm1/zQLJ74fVjSgllBT7qvpvb6/ryYqrm1dCcEI98+HQ0vgwlp4eR7GNoE+DlZT27F15YSIzU9jGsOR5SraPgVenPMZqGagYxsGht6WXtZgSSfYOd08C9WgXw397gt0zbHdhv41YGYHoT1sHg+vLkL0L4j8AY+Pw9phMKUFTG0N/apjDZKMgcu4sqwLpMVCdgoU5kJpIfaAsmIozIH0OEiOhKVdsJm4Ag3yMgDg8kbYMEqFpUVyPxpu7ASAgmxY2SPAIEnQtEyXAb48gUfHwFUJleVwew98uA8Ab276jzPJ10TjuH0qVFZARRnE/ddohTl6X14Kbhfsm+ffyGu+MZquD6kPl9ZDThr8/2CiKf/fa3nXt8PQhiFj9C2S+dC/po7v3Gp4ehqeCWaJlnfT1FXsW6Swq9zbUjUcfJBaXPlhIqmZidjs/Bgq5oGenQGmGkYQ+gIA9QAAAABJRU5ErkJggg==`;
const RGL_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAh1JREFUOI11k8trU1EQh7859+amebSxD6yCILZVUYqCC0sxirpSwRara8UuXEr9C/wDBIsLQURBAqILQVeCUAShiq9CEdRiKeKjCKbUlDZXc1/jIubRJP5WhzlzhpnvN0dokN4ZOej17p1y+rJFPj2B5c8gAiCgr+Tsw2P1+XZjAX4t2PaZ244mu5ziu8cEro+IAEom6XQ0ppumAqqIWIgYUqevYto3oqrlBlqouQBo5SBiaBs61/KuOoLmRvcDFoVFKK2C/2fw38yoKvam3di9Owm+z0Jbul1zo0OEoWFpHowg7q3RUskPncTwOLH+LIhgkt2qICK1tvV3Qb25KfG/vCYqLpE+dQUCD9Hc2PKK63cSS5A5f79CHICosMjqgwkS2QvEBg6DVWZTmUVCv+ZCtJYncpcxqW5KM/cI8gsE32bQMMB9OolM38Dq2kqsP4uz6zjiJMoMqjA2D2JS3fhf3+JO30TiaUSkbKFlQ+gT/JwnWFogPniyBrECy9lxBIDSm7vVx40SEdJjk2DF6mxUBI2wtw2jgUe48gNjpMkuwpImj17C6umrxawYNqKzG5JOBx8fge+Ssb32gqfb6203iU5SJy6LlX9f5OX1uSppjZrXS3Njh1ZW155JWwZ7yz7ie0YwPQOI7cDzay8kO3GgPr/5L+Q/WJmLs2DHa0VVaxAa1LzKYlDPXR9qAfT/HSBo5COhvz5sxVr8BPgL9KC++Hv3ClgAAAAASUVORK5CYII=`;
const SCRAP_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAAt1BMVEVHcEwWiugAf//qTDsXieZBpvXbJCT/AAAAqv/ANSfINynBNSYjk+4ViObFMyEwmu8Piuv0RSzBNiTANCXDNSXBNSYWieYPi+4XieYNjfI+pPXTPzEejul3ZJHnSTc0rf/EMh8YiOVCer+jSlHiSDjANibBNScWieazLh4/pfVApfXANSfOPSzoSjjnSjfsSDQ3q/2pTlKXWWvWKQphc60XiOhIfMHNLxYIj/MYiebBNSbFNyjENCK2oB+jAAAAOXRSTlMAKAj+/P4HAgP9JahN24D8/OVa0EzcW4qto7z68RO84Pp+ZGT9kvLJMq2TafCsk07rf1OlVGt/3dzjtFwPAAAAuUlEQVQYlU2OxxaCQAxFA4zMhCKgKChgA3vvZuD/v0sYXHhXyT057wUEOIy5QxACWgRExIhGej2JVmjxmjFyNfhjmBJ54/G2lULXQUs7DTv4JWUxed5lOnWU0J2nS+RHt2vOVei8afks7tWs6gJXwvdH+RsRZSPqm8Xr0Z2hHOxDs145nIMgKJLSztoCDiejMAzjYIFpciWs42SSlHLV+39105eIfdtSobwGTFtKlEslVBKH3gAxBP4FPYIP3EjUE/QAAAAASUVORK5CYII=`;
const SEARX_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAAdVBMVEVHcEwUFBQHBwcAAAAFBQUAAAAAAAAAAAAAAAAFBQUAAAAAAAATExMTExMAAAAAAAAAAAAAAAAAAAAAAAAUFBQAAAD////t7e1ycnLOzs7b29vV1dVra2uxsbEkJCS6urr29vbk5OSSkpLBwcGJiYmjo6Ofn58+KB04AAAAFXRSTlMA/lKRSJ68qPHUcXfe8f315cA2PreujiKhAAAAh0lEQVQYlWWPiQ7CMAxD067XNm7weu3m+P9PZFBgGrMURXmKZJtopfM0vDz9bq6oNAAOgvI3KKAx2mqop51egEf1Uo19Agq2H+62j5AJaGatg7MBRQIZYmhaH64QHx+MPgbvnfkaa9y6rnHIaSZosTPYzGmlyogMtn8lcjCxJEcwviQSl1X5JxdvCFsM3V/oAAAAAElFTkSuQmCC`;
const SKIAL_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAmBJREFUOI2Vk81LVGEUxn/nvde5o6PjjB9jyiBqkBUhGEJtbBFCi8xlIG1t07bWtWgV0R8QCOGioqJFJETmJumDFklglIiVn6mNpjN+zZ3rvafFTDqSm87u8D7nvA/P8xyJXnl0wwmVdKOBz/+UGMvNeYO2EyrpNpbdsW9eAxTwVAAoEUVQEKt4HicENhr4GvigAaAoQspKQkUjJMpABFaysDZPtTeFJT4gBXjg28WsVCFV0c7T3mZ6jgfYSOElwpuZWjofNFC99A6LPbammHaKOP0Xmuhp3WZgFEL3skj/NnfeCh31WUZ6E6zYyfxPhSpioFASIxnfYSbt0fdCkMUhytTl2tdmUmttOLYLktunZdECAX+D2VU4FHa5e87h+mgXSwsrsLHAraHXoC4JywMxBywQQ02wzOXhZQbOxzkaT/OkC365URbTdTyfDHj5eRyy3/PCFsqKnL7YJ2KSkLeqbHOe+xMW79O1lGo5voFEZIP2hEtjXRPD0xtEdtJ5Fqrz+1zAWKSkkjJ3lsWxcW5+iUJpAyRbuH3SoyWWIVx7mGB2blf93QWqAW60laudx/iZ8Xn44ROx7WnI/GZtBuZaj1AZUpJhYfVgEZWcWviOUl3lc+lMB5OpEwSB0lQTxjM5MhphdvkHUTnARhFD+foEzz4mONtWRbwy4FTM2QVuZsO8GlujIjOOHOgCglGP9ekRHqeaCcfqSVSEEYGl9SzZ1QWcrSmM7uxzwUaMJcbKZ1vy0SzdmkS3vrFYiLJBKf17TIWDEmOhqpbt5rxBJwT/nrNi0KIO0D2Iqlpuzhv8A1Ss++oiBIaEAAAAAElFTkSuQmCC`;
const SR_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAEtJREFUOI1jYBgFjEjs/9jkP2ER50PSx4SmmRENo2iCaUQ2lImBQsCCxkd2LiMDEYBqYQAzDMPv2DQhA5LCAF8g/kfDMBcRZchIBwC/LRIyZvwz+wAAAABJRU5ErkJggg==`;
const STEAMCOLLECTOR_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAcNJREFUOI2l079LlXEUx/HX97nXUsgiJbQhKDFyKbxk5Q+KoMYgoikCibagrcH+gcigIShwamiVfoC0FAQu9xItLgUOLdHvJDELTbvP82246PNQZkFnOh8OnzfnnO/5BojoH43VhEoIWqwTMVrMmHp6OQwFhIihazENiSRm61nzCAkxk1VHQikcGo3Vcsngv5qLkHqqVk6o/Mlcj8z84Huks0xLqTBKRkKlXJw54uVSI5/JqLRy/QhbW7j/glvTDLQSVroIWspF86eUeydZrlNq4uhevi4xt8DNXi6+p2eMgU05ZBWwnHGwg8P78jZvPKJtAzvauPSA8fM8Ps3wBF0bfwHMpxzfmZun39Hdzom+ht6/h9tPOHeMD3dzQLJi+JjSsz0HfPpCV0FvbmZ6lhIUlr4KmE/paM8L/bsZm8z1+FPO9vF6tth3MQ0o8WaOLKNzC1dOcfUhM8sM99K7iwt36G3OAWHwWowrorag8RwRTTw7w4HuRu3VZ65MMPmWbRsKgIHRuLDW/WeR50t8W86BlY2/HFO0WM6YKie/n3IS2NeMZmtGSEhTU//9mZKA6kgo1VO1GC3+zRyjxXqqVh0JpYCffrKfRkNiO78AAAAASUVORK5CYII=`;
const STEAMHISTORY_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAJFBMVEW4eXmec3OTUVGVOjpuQkJiOjpHLi5dFhYjHR1ECgoZAAAAAAA08fiKAAAANklEQVR42mPYDQUMxDF2dEMZiWreYWBGQLmYKJgxWYSZFcxoYdJUBTOKzasqwYxVq1avIs4KALGRU6kNYQe5AAAAAElFTkSuQmCC`;
const STEAMLADDER_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAABP0lEQVQoz33QvWrCUBiA4W+wdnbURd2dLIK9Abu7CrY34ODq0CK04OQNdOvP5jk5JhxtHJREpxg5ngwhQl0MLVioS5c0Il/HGGjLcwMvL6g14mtfxq1TlucRp2zcaV/EV2vQW09wgSKUO/l5ZCfCBU6w9woknKOD8hcOzpGEQBlHE+211OLstYkcKYNhRmmytl5YJeRJZJXQC6ytNIcZ0FOszho8a4I8YgLPsgar6ymgj+phjJaQ93GWGKN6oA9AAuvPSAtJAOTdRIFLlN9xSxRoInmD/hV1uT/ryIq8OFKZdbhP3f4lPMEozfMAIhYpAIDnR+lngEGO3ihdveQm5WnETeolpUuvBzkgLxynaG/kKM7eTJEjGQLZ/7t6D8SboI0ikB9xIrBxjMQDrUo8bWu0nKI8izhFo6VtiadVfwAriWTb6bbagwAAAABJRU5ErkJggg==`;
const STEAMDB_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAAAAAA6mKC9AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQflBw0XMwwuo7iyAAAA4UlEQVQY02NgwAGYmKAMRgYGBhY1cxVBhud3Lt36zcDAyMAgURwuyfJr25VvfB/mvWZgYWCw8uH982byrl8cmiX/uxhYGBhO5vBwf7spEqqpIfePgYGFgYEhi+kkQ4yD8L8PZ9YwMLAwMPgalbIpv1j19sbXXveZDCwMDKJSVhPX/v3PLFEgI8bAwMjAoL5U/tWt5wySamIPo28ysDAwOAhl8agLMtzc9KXT4SYDCwODmJzlhKX/GJgkCuQgWqTL/b/ces4goc6zsfMpAyMDAwObppmKIMP7O6eu/8LiOQwAAOC0QgrKG2WwAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIxLTA3LTEzVDIzOjUwOjU4KzAwOjAwqRiy6gAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMS0wNy0xM1QyMzo1MDo1OCswMDowMNhFClYAAAAASUVORK5CYII=`;
const STEAMIDUK_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAAbFBMVEUYTLUYTLYVS7YYTLYAH68XTLYYTLcXS7YAOLIAOrIVSbYSRrIYTbf///8SS7bN1u2PodUATrp+ltJcfskBQ7Xz9/0ANrPBzOjf5vUAK7LV3fBvis3r8Pm5x+chXb+rud8wacRAccYAAK6arNs8D6eEAAAADHRSTlPsypm76/jIxsbrmXuFhwQPAAAAAWJLR0QAiAUdSAAAAJFJREFUGJVVz4kOgyAQBNC1NR6tg8glIGq0//+PXUmT4oSE7MuSDPSkW15Ewy3NBaKAlsi4yRkTnAkhOIZeWVil7apSSrNpqfaQiBqjhE9YugvULjPMI1RH/QdYz+UPxAPmcmP1B2wBdYT+QfR5g19g14iSb7tdxaZjc2LKcYJhEIa7C8FH5Or3z1X0aNoi1fsLczIOQYfGvX0AAAAASUVORK5CYII=`;
const STEAMTRADES_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAS1BMVEUAAAD8sgfjZU1NpBUVfsf8swfjZE9MpRUVgMj7sQbhYkxMpBQTfcf7sAbhY01LpBUUfcb7sQbgYkxLpBUUfsf6sAXgYkxLpBQTfcZQxMYbAAAAFXRSTlMASUlJSUpKSkrk5OTk5ubm5ufn5+c/OHvaAAAAV0lEQVR42nXMRwrAMBADQHc7xb3+/6UxLATWsLpIzEGMiEpJIUhrpRMaAtdex6jIGCUsEYLYFeeMAGGMAFB/KLtsfSyAKbchn7n3HIHv3Z+QEeh8aeLtA/IfA5EtgV/9AAAAAElFTkSuQmCC`;
const STEAM_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAAmJLR0QA/4ePzL8AAAAldEVYdGRhdGU6Y3JlYXRlADIwMjEtMDYtMTZUMTg6MTk6MTcrMDA6MDChxVOHAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIxLTA2LTE2VDE4OjE5OjE3KzAwOjAw0JjrOwAAAXdJREFUOE+lkzGqwkAQhidBEaK2gpWIpakVvYSgR7CwEAQDgcTKAwhpxBvkLukERbHSShDR0sJqXv5x41vN8zV+8Du7k5nfDZM1OIa+AQZgNBpxPp/nQqHAxWLxRdlsFn/CmUxGalCbIAZIoOCTms0mu67LjUbjmUtMxACueoOu3W4nhYvFQuJ2u5U8eoAY4NjvjZDnebzf7yWCyWTCh8OBHceR50B+8Z7vzdB6vZZiHd/3eblcynNgxgusEF6o1Wpk2zbdbjeVeYDay+WidkQyRsMw1PYX3bTb7VK1WqXT6URhGFKlUpH1/X5X54iZz+dsWRa3Wi2Ookhl07TbbTk+Xhs8DXRmsxlfr1e1Y16tVtKk618DEB9b4ng8TjWnDJAIgkASCZ1Oh4fDYaoxEUYPxCCXy0lys9nwdDrlXq/H5/M51aQr+ZBkjIPBAIHq9Todj0cql8tUKpUk94l+v/9YiE0Mvm1MIR4pm6b556WC3i/Tl9eZ6Add4b/skypHSwAAAABJRU5ErkJggg==`;
const TWITTER_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAaBJREFUOI3Vkb9rU2EUhp/z3XubS9pUwYoNRaKDk6ODFBysODkJDi5qogVHQekfoAgu+g84aAImgUI3VxV0du6sWKNWEJq0eu/9fhynhKRNd32n7xy+9+E958B/oaWN/plqc3fjRKv/eNhbeP6zCiAAx1uDayW4utWo3NxvrrYGT2fS8pq6AkyE2uyLiOxkmb+/fffoGwNgPMvR7NyNpVb//X5AkiRrIf+NeovaDEx0UpXTUUkujBIoUOvmSgiot97DB6f64kfjSKfWLVRtNgJKFOOde/u1MX8ZwAAce7lXV1e8Uw0gEsVRvJImpXatm0+Yh3KKHb4NwOL58muTzl5CA6ii3qGuQG1+cKMmRtV0JgCbZ+VXMdh7pGiOyOHnEEFdXmzfmWtPAAB6q5WHAjuImW4GJJ7Be22P90a/BXBBOgRvp6WQKCHY4nPv9vzqVADAt0blgXPyhKBhPLYkKcHZT1v1yqkDYIDaqz8XfXDPolJ6DgzqChABY1Cb73ql2atX7k0da7xYbPaviMh1JCwEbzYljta/3yp/PHQp/4T+AgbtsdyJATuvAAAAAElFTkSuQmCC`;
const UGC_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAAAAAA6mKC9AAAAAXNCSVQI5gpbmQAAAExJREFUGJWtyrkNgDAQBdHhkLZmanAJNEcBliBzOATmdETAS/5qtMy8JBrRhi+iQIlVRRUQROoF/fmpda/QHTuSJbMJLN79Z4nhIaYdsukY6jT/2okAAAAASUVORK5CYII=`;
const VAC_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAIeSURBVDhPpVNLTxNhFD0zfScCZU1tbe2CaRt8JJLIAleVqDtJJJGkS9xIIrLpr8GfgRvEVTfy0OgaaKSdhoaWYrHvjvdcMoCPlZzJzdzvm++e79xzM0Y8EXdwDZgejwcM0zT/N84Tl2QwGGA4HGr0ej0Nd81wL3TDmLQmtQXHcfTwzMMZHFWPYBomErcT6HV7OCgeyAFgfDyMT1tb8Pv9Kp8wrJSlBCzOpDN4NDuLkZFRiRt6A2/tdDo4aTTQ/NHExuYGisUivF7vOUE6k1YCHnr25Cl8wh4KBuEPBOAVAkcIukLebrfRljM/z87w4eMmAvKdUA+IsbFRtFotnNTrsG0b9VoNpVIJhxK8vVqtonZ8rKpCoRAMw9AwLxJ5HkxPw7IsvF1dVZmP5+bwcnER+3t7eD4/j9fLy2gIWTAQvDSexUwced6vr+PFwgK2xSiqSKVSSCaTKJfLiMVieLW0hH6/D7siCkVps9mEMXVnymHCuH/3Ht6srODd2hqy2awa2zg9RSIeR6FQQC6XQz6fx5dvX+H3+bR1I3Iz4lAF538rGkM4HFaHWczR8hvfqlLeHLFdqeiauPCAG/syb86YOV1mcO3m3KeprvGEEhAuyfbujs6eKrgnB+ATud1uFzufdzV3awgjGov+9jMNRWZf5EcmIhITSva9dIiymMq+rxYTfxEQ7JWF9IWgMsr+s5i4bOYK3HbYv+vJv4oB4BfUc/nIFeL+dgAAAABJRU5ErkJggg==`;
const YANDEX_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAAS1BMVEVHcEz9Phv9Pxz/QR78Ph39Lgv8LQv9PBn9PRr8Phz////8AQD8KgD8Ngb8GAD+y8j9opz+4uD8aFn+t7P9gXf+zsv/7+/+1NL8UDm2iGRRAAAACnRSTlMA6f///Fhaxc3s30eCWwAAAJFJREFUGJVlT9sWwyAIU9BaQVYvXbf//9LBtHtZHjwmhhicU8SUQ8hpcws7IigQ98m90sLMBdDPdwCurbWhino2441eg54PHYsuIUinwUyHCcllADmplilAdkHPQm05IEyh0vW+hWwV+KB+j2iohYxfaLRadGnG99vNikmvXUo9BWZ5j1JEbbKq/y+n9ef60e4flbMGxZ/yNYwAAAAASUVORK5CYII=`;
const YOUTUBE_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAPZJREFUOI3FkrFKA0EYhL89sngBkWDwWts8jN1VqW1TSF7CTgRb3yKkSvqAWFjYWqWJKC4Yi7u93I6FGAKu4YyCAwvLzyzzz8zCf8N8Xp7grGvMMdbum3bbYq3d4Imi8PJ+Fapq6aT5EVwC8AonAYJAPz0eTqnhZpfHAtVwlwgOo+b6/SYRZARYRBWmU8k5Kc+3bfBCDc9RwnisNWYzKctiGbwljbrqdKDX+zIWmO8tTCZSWUrD4TYLjhU8RAmDQZMmFtRw+4sa7xMPV41yiCDA9fore7gI0AUO9tI0xdrWR04AGLyvqrIsAiwTcDmcj+BxV/G/wzt9WBUFg/DKewAAAABJRU5ErkJggg==`;

function logXhr(p_module, p_readystate, p_status, p_details = "") {
    const getStatusColor = (p_status) => {
        if (p_status === 0) return "white";
        if (p_status === 200) return "limegreen";
        return "yellow";
    };

    console.log(
        "%c[DEBUG] |%c" + p_module.module_name + "%c" + (p_details ? `(${p_details}) ` : "") + "xmlhttpRequest -> readyState = " + p_readystate + ", status =%c" + p_status,
        log_styles,
        `${log_styles}color:#87CEFA;`,
        log_styles,
        `${log_styles}color:${getStatusColor(p_status)};`
    );
}

function decodeHTMLEntities(p_str) {
    let text = document.createElement('textarea');
    text.innerHTML = p_str;
    let decoded = text.value;
    text.innerHTML = decoded;
    return text.value;
}

function toTitleCase(p_str) {
  return p_str.replace(
    /\w\S*/g, function(p_txt) { return p_txt.charAt(0).toUpperCase() + p_txt.substr(1).toLowerCase(); }
  );
}

function timeAgo(p_timestamp) {
    const now = Math.floor(Date.now() / 1000);
    const seconds_past = now - p_timestamp;

    if (seconds_past < 60) {
        return `${seconds_past} second${seconds_past !== 1 ? 's' : ''} ago`;
    }
    if (seconds_past < 3600) {
        const minutes = Math.floor(seconds_past / 60);
        return `${minutes} minute${minutes !== 1 ? 's' : ''} ago`;
    }
    if (seconds_past <= 86400) {
        const hours = Math.floor(seconds_past / 3600);
        return `${hours} hour${hours !== 1 ? 's' : ''} ago`;
    }
    if (seconds_past <= 2592000) {
        const days = Math.floor(seconds_past / 86400);
        return `${days} day${days !== 1 ? 's' : ''} ago`;
    }
    if (seconds_past <= 31536000) {
        const months = Math.floor(seconds_past / 2592000);
        return `${months} month${months !== 1 ? 's' : ''} ago`;
    }
    const years = Math.floor(seconds_past / 31536000);
    return `${years} year${years !== 1 ? 's' : ''} ago`;
}

function checkSourcebanDataPresent(p_data, p_type = "Username") {
    let data = p_data.trim();

    if (p_type === "Username") {
        return data != '<i><font color="#677882">no nickname present</font></i>';
    } else if (p_type === "Ban reason") {
        return data != '<i><font color="#677882">no reason present</font></i>';
    } else {
        alert(`Invalid p_type specified for checkMissingBanData: ${p_type}`);
        return true;
    }
}

function addFlag(p_hexcolor, p_text) {
    let account_info = document.getElementById("account-info");
    let flag = document.createElement("div");
    flag.style.background = `${p_hexcolor}2b`;
    flag.style.color = `${p_hexcolor}`;
    flag.style.border = `1px solid ${p_hexcolor}`;
    flag.className = "account-flag";
    flag.textContent = p_text;
    account_info.insertBefore(flag, account_info.firstChild);
}

function openSettingsModal() {
    const settings_modal = document.getElementById("settings-modal");
    const responsive_header = document.getElementsByClassName("responsive_header")[0];
    settings_modal.style.display = "block";
    responsive_header.style.zIndex = "0";
    document.querySelector("body").classList.add("scroll-lock");
}

// Gets Steam profile information in XML format, also updates the status for Steam ID details
function getSteamInfo() {
    console.log(`%c[DEBUG] | Calling getSteamInfo()`, log_styles);

    let account_infobox = document.getElementById("account-info");
    let account_loadinglabel = document.getElementById("account-loading-label");
    let account_loadingdescription = document.getElementById("account-loading-description");
    account_loadinglabel.textContent = "Connecting to Steam...";

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://steamcommunity.com/profiles/${profile.id64}?xml=1`,
        timeout: 15000, // Time to wait (in milliseconds) for a response
        headers: {
            "Content-Type": "application/xml",
            "Accept": "application/xml"
        },
        onreadystatechange: function(res) {
            if (res.readyState === 2) {
                account_loadinglabel.textContent = "Loading Steam info...";
            } else if (res.readyState === 4) {
                if (res.status === 200) {
                    parseSteamInfo(res.responseText);
                } else if (res.status >= 400 && res.status <= 499) {
                    account_loadinglabel.textContent = `Client error (${res.status})`;
                    account_loadinglabel.className = "color-warning";
                    account_loadingdescription.textContent = "Steam's servers might be having issues or you are currently being rate limited.";
                } else if (res.status >= 500 && res.status <= 599) {
                    account_loadinglabel.textContent = `Server error (${res.status})`;
                    account_loadinglabel.className = "color-warning";
                    account_loadingdescription.textContent = "Steam's servers might be having issues or undergoing maintenance.";
                } else {
                    account_loadinglabel.textContent = `Unexpected status (${res.status})`;
                    account_loadinglabel.className = "color-error";
                    account_loadingdescription.textContent = `This should never happen unless you're debugging this script or using an application to modify server responses.`;
                }
            }
        },
        ontimeout: function() {
            account_loadinglabel.textContent = `Timed out`;
            account_loadinglabel.className = "color-warning";
            account_loadingdescription.textContent = `Steam did not respond quickly enough.`;
        },
        onerror: function() {
            account_loadinglabel.textContent = `Application error`;
            account_loadinglabel.className = "color-error";
            account_loadingdescription.textContent = `Please report this issue if it persists.`;
        }
    });
}

function parseSteamInfo(p_info) {
    console.log(`%c[DEBUG] | Calling parseSteamInfo()`, log_styles);

    let account_infobox = document.getElementById("account-info");

    var dp = new DOMParser();
    var parsed_xml = dp.parseFromString(p_info, "text/xml");

    let use_fallback = false; // Used to determine whether or not we are forced to use alternatives to the XML API for some information

    let privacy_state = "";
    let member_since = "Unknown";
    let limited_account = false;
    let community_banned, tradeban_state, vac_banned;
    let profileban_text = document.querySelector(".profile_ban");

    if (parsed_xml.getElementsByTagName("steamID64").length <= 0) {
        console.log("%c[DEBUG] | Missing expected Steam data, using fallback mode.", "color: orange; font-size: 15px;");
        use_fallback = true;
    }

    if (steamdetective.forcedfallback_enabled) {
        console.log("%c[DEBUG] | Forced fallback option is enabled.", "color: #ff8efd; font-size: 15px;");
        use_fallback = true;
    }

    profile.custom_url = "";
    profile.username = username_label.innerText;

    if (parsed_xml.getElementsByTagName("privacyState").length > 0) {
        privacy_state = parsed_xml.getElementsByTagName("privacyState")[0].childNodes[0].nodeValue;
    }

    if (privacy_state === "public") {
        member_since = parsed_xml.getElementsByTagName("memberSince")[0].childNodes[0].nodeValue;
        profile.custom_url = parsed_xml.getElementsByTagName("customURL")[0].childNodes[0].nodeValue;
    } else {
        if (!use_fallback) {
            member_since = "Private";
        } else {
            member_since = "Unknown";
        }

        // Since the XML data is missing, this gets the custom URL from the address bar instead
        if (document.location.href.includes("/id/")) {
            profile.custom_url = document.location.pathname.split("/")[2];
        } else {
            profile.custom_url = "";
        }
    }

    if (parsed_xml.getElementsByTagName("vacBanned").length > 0) {
        vac_banned = parsed_xml.getElementsByTagName("vacBanned")[0].childNodes[0].nodeValue != 0;
        profile.is_vacbanned = vac_banned;
    }

    if (parsed_xml.getElementsByTagName("isLimitedAccount").length > 0) {
        limited_account = parsed_xml.getElementsByTagName("isLimitedAccount")[0].childNodes[0].nodeValue != 0;
    } else if (!use_fallback) {
        limited_account = true;
    }

    if (parsed_xml.getElementsByTagName("tradeBanState").length > 0) {
        tradeban_state = parsed_xml.getElementsByTagName("tradeBanState")[0].childNodes[0].nodeValue;

        // Workaround for trade bans/probations not showing on profiles that have them.
        if (profileban_text && tradeban_state === "None") {
            console.log("%c[DEBUG] | Detected Steam XML trade ban/probation bug, using workaround.", "color: orange; font-size: 15px;");
            if (profileban_text.innerText === "Currently trade banned") {
                tradeban_state = "Banned";
            } else if (profileban_text.innerText === "Currently on trade probation") {
                tradeban_state = "Probation";
            }
        }
    } else if (use_fallback && profileban_text) {
        if (profileban_text.innerText === "Currently trade banned") {
            tradeban_state = "Banned";
        } else if (profileban_text.innerText === "Currently on trade probation") {
            tradeban_state = "Probation";
        }
    } else {
        tradeban_state = "Unknown";
    }

    // Since there's no API for community bans that doesn't require an API key, we check for community ban page characteristics instead.
    // This has the limitation that community banned profiles that are private when banned are indistinguishable from normal private profiles.
    if (document.getElementsByClassName("profile_private_info").length > 0) {
        if (document.getElementsByClassName("profile_private_info")[0].innerText.length == 0 &&
            document.querySelectorAll(".private_profile .profile_header_bg_texture").length > 0) {
            community_banned = true;
        } else {
            community_banned = false;
        }
    }

    function checkJoinDate(p_membersince) {
        if (!use_fallback) {
            // If account is less than a year old, add the current year since Steam doesn't include it in that case.
            if (p_membersince.indexOf(", ") === -1 && p_membersince != "Private") {
                return `${p_membersince}, ${new Date().getFullYear()}`;
            } else if (p_membersince === "Private") {
                return `<span class='color-warning'>${p_membersince}</span>`;
            } else {
                return p_membersince;
            }
        } else {
            return "<span class='color-warning'>Unknown</span>";
        }
    }

    function detectDeceptiveURL(p_url) {
        if (profile.custom_url != profile.id64) {
            let fakeid64_regex = /^(profiles)?7[0-9]{3}/i;
            if (fakeid64_regex.test(profile.custom_url)) {
                p_url = `<span class="color-warning">${profile.custom_url}</span>`;
                profile.has_fakeid64 = true;
            } else {
                p_url = p_url.replace("I", `<span class="color-warning text-bold">I</span>`);
            }
        }

        return p_url;
    }

    function adjustURLStyling(p_url) {
        let classes = "";
        if (profile.custom_url === "") {
            classes += "font-default ";
        }
        if (p_url.length > 22) {
            classes += "url-long ";
        }
        if (p_url.length > 28) {
            classes += "url-max-long ";
        } else if (p_url.length > 23) {
            classes += "url-extra-long ";
        }
        return classes.trimEnd();
    }

    account_infobox.innerHTML =
    `
	${limited_account ? "<div id='limited-account-indicator'>Limited account</div>" : ""}
	${community_banned ? "<div id='community-ban-indicator'>Community banned</div>" : ""}
	${tradeban_state === "Banned" || tradeban_state === "Probation" ? "<div id='trade-ban-indicator'>Trade " + tradeban_state.toLowerCase() + "</div>" : ""}
    ${vac_banned ? "<div id='vac-ban-indicator'>VAC banned</div>" : ""}
	<table id="account-data-table">
		<tr><td class="account-label-cell corner-topleft-5px">ID2:</td><td class="account-data-cell corner-topright-5px">${profile.id2}</td></tr>
		<tr><td class="account-label-cell label-clickable" id="label-id3">ID3:</td><td class="account-data-cell" id="account-data-id3">${profile.id3}</td></tr>
		<tr>
            <td class="account-label-cell label-clickable" id="label-id64">ID64:</td>
            <td class="account-data-cell" id="account-data-id64">
                ${profile.has_malformedid64 ? `<span class="color-warning"><b>Bad:</b> ${profile.original_id64}</span>
                                               <br>
                                               <span class="color-fixed"><b>Good:</b> ${profile.id64}</span>`: profile.original_id64}
            </td>
        </tr>
		<tr>
            <td class="account-label-cell label-clickable" id="label-url">URL:</td>
            <td class="account-data-cell ${adjustURLStyling(profile.custom_url)}" id="account-data-url">
                ${profile.custom_url === "" ? `<span class="color-warning">N/A</span>` : detectDeceptiveURL(profile.custom_url)}
            </td>
        </tr>
		<tr><td class="account-label-cell corner-bottomleft-5px">Join:</td><td class="account-data-cell corner-bottomright-5px">${checkJoinDate(member_since)}</td></tr>
	</table>
	<div id="acc-age-disclaimer" ${!steamdetective.agedisclaimer_enabled ? "class='hidden'" : ""}>Account age is not an indicator of trustworthiness on its own. <br> <a style="color:#fffc45" href="https://forums.steamrep.com/pages/obviousalts/" target="_blank" rel="noreferrer">Click here for more details</a></div>
    `;

    let label_id3 = document.getElementById("label-id3");
    let label_id64 = document.getElementById("label-id64");
    let label_url = document.getElementById("label-url");
    label_id3.onclick = () => copySteamDetail("id3");
    label_id64.onclick = () => copySteamDetail("id64");
    label_url.onclick = () => copySteamDetail("custom_url");

    if (profile.has_malformedid64) {
        addFlag("#ffa500", "Malformed ID64");
    }
    if (profile.has_fakeid64) {
        addFlag("#ffa500", "Fake ID64 URL");
    }

    if (use_fallback && steamdetective.forcedfallback_enabled) {
        addFlag("#ff8efd", "Fallback mode (Debug forced)");
    } else if (use_fallback) {
        addFlag("#9eD0ff", "Fallback mode (missing XML data)");
    }

    getModuleInfo();
    addLinks();
}

function copySteamDetail(p_detail) {
    if (p_detail === "id3") {
        GM_setClipboard(`https://steamcommunity.com/profiles/${profile.id3}`);
        ShowAlertDialog('ID3 link copied', `<b>https://steamcommunity.com/profiles/${profile.id3}</b> has been copied to your clipboard.`);
    } else if (p_detail === "id64") {
        GM_setClipboard(`https://steamcommunity.com/profiles/${profile.id64}`);
        ShowAlertDialog('ID64 link copied', `<b>https://steamcommunity.com/profiles/${profile.id64}</b> has been copied to your clipboard.`);
    } else if (p_detail === "custom_url") {
        if (profile.custom_url) {
            GM_setClipboard(`https://steamcommunity.com/id/${profile.custom_url}`);
            ShowAlertDialog('Custom URL copied', `<b>https://steamcommunity.com/id/${profile.custom_url}</b> has been copied to your clipboard.`);
        } else {
            ShowAlertDialog('No custom URL', `<b>${g_rgProfileData.personaname}</b> has no custom URL. Nothing has been copied.`);
        }
    } else {
        ShowAlertDialog('Error', `An invalid detail was copied. Report the issue if you see this message.`);
    }
}

function getModuleInfo() {
    if (steamrep.is_enabled) {
        getSRInfo(profile.id64);
    }
    if (bptf.is_enabled) {
        getBPTFInfo(profile.id64, bptf.api_key);
    }
    if (mannco.is_enabled) {
        getManncoInfo(profile.id64);
    }
    if (reptf.is_enabled && (marketplace.is_enabled || scrap.is_enabled)) {
        getRepTfInfo(profile.id64);
    }
    if (firepowered.is_enabled) {
        getFirePoweredInfo(profile.id2, firepowered.hide_inactivebans);
    }
    if (skial.is_enabled) {
        checkSkialPresence(profile.id3);
    }
    if (steamhistory.is_enabled) {
        getSteamHistoryInfo(profile.id64);
    }
    if (steamiduk.is_enabled) {
        getSteamIdUkInfo(steamiduk.my_id, steamiduk.api_key, profile.id64);
    }
    if (steamtrades.is_enabled) {
        getSteamTradesInfo(profile.id64);
    }
}

function addLinks()
{
    let research_info = document.createElement("div");
    research_info.id = "research-info";

    research_info.innerHTML = `<hr class="splitter"> <h1 class="reputation-menu-title">Research</h1>`;
    research_info.append(buildResearchLink(ETOOLS_ICON, "eTools.ch (Multi-search)", "https://www.etools.ch/searchSubmit.do?query=", true, true, true, true, ['"', '"']));
    research_info.append(buildResearchLink(SEARX_ICON, "Searx.one (Multi-search)", "https://searx.one/search?q=", true, true, true, true, ['"', '"']));
    research_info.append(buildResearchLink(STEAMDB_ICON, "SteamDB.info", "https://steamdb.info/calculator/", false, false, true, false));
    research_info.append(buildResearchLink(STEAMLADDER_ICON, "SteamLadder.com", "https://steamladder.com/profile/", false, false, true, false));
    research_info.append(buildResearchLink(STEAMCOLLECTOR_ICON, "SteamCollector.com", "https://steamcollector.com/profiles/", false, false, true, false));
    research_info.append(buildResearchLink(STEAM_ICON, "SteamLevel.net", "https://steamlevel.net/profile/", false, false, true, false));
    research_info.append(buildResearchLink(STEAMHISTORY_ICON, "SteamHistory.net", "https://steamhistory.net/id/", false, false, true, false));
    research_info.append(buildResearchLink(STEAMTRADES_ICON, "SteamTrades.com", "https://www.steamtrades.com/user/", false, false, true, false));
    research_info.append(buildResearchLink(INVENTORYGIFT_ICON, "Inventory.gift", "https://www.inventory.gift/user/", false, false, true, false));
    research_info.append(buildResearchLink(DISPENSER_ICON, "Dispenser.tf", "https://dispenser.tf/id/", false, false, true, false));
    research_info.append(buildResearchLink(UGC_ICON, "UGC League", "https://www.ugcleague.com/players_page.cfm?player_id=", false, false, true, false));
    research_info.append(buildResearchLink(RGL_ICON, "RGL.gg", "https://rgl.gg/Public/PlayerProfile.aspx?p=", false, false, true, false));
    research_info.append(buildResearchLink(ETF2L_ICON, "ETF2L", "https://etf2l.org/search/", false, false, true, false));
    research_info.append(buildResearchLink(FIREPOWERED_ICON, "FirePowered Bans", "https://firepoweredgaming.com/sourcebanspp/index.php?p=banlist&advSearch=", true, false, false, false, ["", "&advType=steamid"]));
    research_info.append(buildResearchLink(DEFAULT_ICON, "MVM Lobby", "https://mvmlobby.tf/profile/", false, false, true, false));
    research_info.append(document.createElement("br"));

    research_info.append(buildResearchLink(REDDIT_ICON, "Reddit", "https://old.reddit.com/search?q=", false, true, true, true));
    research_info.append(buildResearchLink(TWITTER_ICON, "Twitter", "https://twitter.com/search?q=", true, true, true, true, ["", "&f=live"]));
    research_info.append(buildResearchLink(YOUTUBE_ICON, "YouTube", "https://www.youtube.com/results?search_query=", true, true, true, true));

    research_info.innerHTML += '<hr class="splitter" style="margin-top: 25px;"><h1 class="reputation-menu-title">Archives</h1>';

    research_info.append(buildArchiveLink(ARCHIVEIS_ICON, "Archive.is", "https://archive.is/"));
    research_info.append(buildArchiveLink(ARCHIVEORG_ICON, "Wayback Machine", "https://web.archive.org/web/*/", ["","*"]));
    research_info.append(buildArchiveLink(GHOSTARCHIVE_ICON, "Ghost Archive", "https://ghostarchive.org/search?term="));
    research_info.append(buildArchiveLink(BING_ICON, "Bing Cache (Search)", "https://www.bing.com/search?q=url:"));
    research_info.append(buildArchiveLink(YANDEX_ICON,"Yandex Cache (Search)","https://www.yandex.com/search/?text="));

    document.getElementById("reputation-menu").appendChild(research_info);
}

function toggleDetails()
{
    let toggle_btn = document.getElementById("toggle-details-btn");
    let settings_btn = document.getElementById("settings-btn");

    let r = document.getElementById("reputation-menu");

    if (r.classList.value === "hidden") {
        toggle_btn.textContent = "Hide links and details";
        r.classList.remove("hidden");
    } else {
        toggle_btn.textContent = "View links and details";
        r.classList.add("hidden");
    }
}

function getSRInfo(p_id64) {
    if (steamrep.eolmsg_enabled) {
        let content = document.getElementById(`${steamrep.id_prefix}-content`);
        let notice = document.createElement("div");
        notice.id = `${steamrep.id_prefix}-eol-notice`;
        notice.innerHTML = `<b>NOTICE:</b> SteamRep's site and forums will be shut down at the end of 2024,<br>
                            which includes name history. You will still be able to check bans until June 15, 2025.</br>
                            Check <a id="${steamrep.id_prefix}-eol-link" href="https://forums.steamrep.com/pages/eol/" target="_blank" rel="noreferrer">https://forums.steamrep.com/pages/eol/</a>
                            for more details.`;
        content.prepend(notice);
    }

    if (steamrep.history_enabled) {
        let steamrep_container = document.getElementById(`${steamrep.id_prefix}-info-container`);
        let history_container = document.createElement("div");
        history_container.className = "history-table-container";
        history_container.id = `${steamrep.id_prefix}-history-container`;
        steamrep_container.appendChild(history_container);
    }

    function addSRLinks() {
        let content = document.getElementById(`${steamrep.id_prefix}-content`);
        content.innerHTML += `<br>
                              <a class="${steamrep.id_prefix}-research-link module-research-link" href="https://steamrep.com/profiles/${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SR_ICON});"></i>SteamRep profile page</a>
                              <a class="${steamrep.id_prefix}-research-link module-research-link" href="https://forums.steamrep.com/search/search/?keywords=${p_id64}&o=date" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SR_ICON});"></i>SteamRep Forums search</a>`;
    }

    updateRepContainer(steamrep, "Connecting");
    updateSummaryStatus(steamrep, "Connecting");

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://steamrep.com/api/beta4/reputation/${p_id64}/?tagdetails=1&extended=1&json=1`,
        timeout: steamrep.api_timeout, // Time to wait (in milliseconds) for a response
        onreadystatechange: function(res) {
            logXhr(steamrep, res.readyState, res.status, "Bans API");
            if (res.readyState === 2) {
                updateRepContainer(steamrep, "Loading");
                updateSummaryStatus(steamrep, "Loading");
            } else if (res.readyState === 4) {
                if (res.status === 200) {
                    let json_response = JSON.parse(res.responseText);

                    function addSRTags() {
                        let tags = document.getElementById("sr-tags");
                        Object.values(json_response.steamrep.reputation.tags).forEach(val => {
                            if (val.length > 1) {
                                for (let i = 0; i < val.length; i++) {
                                    tags.innerHTML += (`${val[i].name} (${convertTimestamp(val[i].timestamp)}), `);
                                }
                            } else {
                                tags.innerHTML += (`${val.name} (${convertTimestamp(val.timestamp)})`);
                            }
                        });

                        tags.innerHTML = tags.innerHTML.replace(/[,]\s$/, '');
                    }

                    if (json_response.hasOwnProperty("steamrep")) {
                        // Scammer
                        if (json_response.steamrep.reputation.summary === "SCAMMER") {
                            updateRepContainer(steamrep, "Scammer");
                            updateSummaryStatus(steamrep, "Scammer");
                            addSRTags();
                            username_label.classList.add("username-label-banned");
                        }
                        // Admin
                        else if (json_response.steamrep.reputation.summary === "ADMIN") {
                            updateRepContainer(steamrep, "Admin");
                            updateSummaryStatus(steamrep, "Admin");
                            addSRTags();
                            username_label.classList.add("username-label-special");
                        }
                        // Caution
                        else if (json_response.steamrep.reputation.summary === "CAUTION") {
                            updateRepContainer(steamrep, "Caution");
                            updateSummaryStatus(steamrep, "Caution");
                            addSRTags();
                        }
                        // Normal
                        else if (json_response.steamrep.reputation.summary === "none") {
                            updateRepContainer(steamrep, "Normal");
                            updateSummaryStatus(steamrep, "Normal");
                        }

                        let content = document.getElementById(`${steamrep.id_prefix}-content`);

                        let report_count = parseInt(json_response.steamrep.stats.unconfirmedreports.reportcount);
                        if (report_count > 0) {
                            content.innerHTML += `<br><p class="${steamrep.id_prefix}-warning-label">${report_count} unconfirmed report(s) on the forums</p>`;
                        }
                        let srbanned_friends = parseInt(json_response.steamrep.stats.bannedfriends);
                        if (srbanned_friends > 0) {
                            content.innerHTML += `<br><p class="${steamrep.id_prefix}-warning-label">${srbanned_friends} SteamRep banned friend(s) - number may be inaccurate if friends are private</p>`;
                        }
                    }
                } else {
                    updateRepContainer(steamrep, res.status);
                    updateSummaryStatus(steamrep, res.status);
                }

                addSRLinks();
            }
        },
        ontimeout: function() {
            updateRepContainer(steamrep, "Timed out");
            updateSummaryStatus(steamrep, "Timed out");
            addSRLinks();
        },
        onerror: function() {
            updateRepContainer(steamrep, "Application error");
            updateSummaryStatus(steamrep, "Application error");
            addSRLinks();
        }
    });

    if (steamrep.history_enabled) {
        getSRHistory(p_id64);
    }
}

function getSRHistory(p_id64) {
    let table = document.createElement("table");
    table.id = `${steamrep.id_prefix}-history-table`;
    table.className = "history-table";

    let table_head = document.createElement("thead");
    table_head.innerHTML = `
         <tr>
            <th colspan="3" id="${steamrep.id_prefix}-table-head" class='history-table-head'>History</th>
         </tr>`;

    let table_body = document.createElement("tbody");
    table_body.id = `${steamrep.id_prefix}-table-body`;
    table.appendChild(table_head);
    table.appendChild(table_body);

    document.getElementById(`${steamrep.id_prefix}-history-container`).appendChild(table);
    document.getElementById(`${steamrep.id_prefix}-table-head`).textContent = "History - Connecting...";

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://steamrep.com/util.php?op=getHistoryEntries&id=${p_id64}`,
        timeout: steamrep.api_timeout, // Time to wait (in milliseconds) for a response
        cookiePartition: { topLevelSite: 'https://steamrep.com' },
        onreadystatechange: function(res) {
            logXhr(steamrep, res.readyState, res.status, "Name history");
            if (res.readyState === 2) {
                document.getElementById(`${steamrep.id_prefix}-table-head`).textContent = "History - Loading...";
            } else if (res.readyState === 4) {
                if (res.status === 200) {
                    document.getElementById(`${steamrep.id_prefix}-table-head`).textContent = "History";
                    let dp = new DOMParser();
                    let html_response = dp.parseFromString(res.responseText,"text/html");
                    let entries = html_response.getElementsByTagName("option");
                    let opted_out = html_response.getElementsByClassName("listdisclaimer").length >= 1;

                    if (opted_out) {
                        table_head.innerHTML += `
                        <tr>
                            <th id="history-optedout" colspan="3">User has opted out of sharing historical data with the public.<br>Only the last 30 days are shown.</th>
                        </tr>`;
                    }
                    if (entries.length > 0) {
                        table_body.innerHTML += "<tr style='background:#001c8a;position:sticky;top:0;'><td>Date seen</td><td>Name</td><td>Approx. Friends/SR Banned Friends</td></tr>";

                        for (let i = 0; i < entries.length; i++) {
                            let data = entries[i].firstChild.nodeValue;

                            let table_row = document.createElement("tr");
                            let date_cell = document.createElement("td");
                            let name_cell = document.createElement("td");
                            let friendcount_cell = document.createElement("td");

                            date_cell.textContent = data.match(/\d{4}-\d{2}-\d{2} \d{2}:\d{2}/)[0];
                            try {
                                name_cell.textContent = data.match(/(?<![0-9])\ .+\ (?![0-9])/)[0];
                            } catch {
                                name_cell.innerHTML = "<i>Warning: Empty/Unexpected format</i>";
                                name_cell.style.background = "#ff4800";
                                console.log(`[DEBUG] | SR History: Error regex matching (name) in entry #${i}: ${entries[i].firstChild.nodeValue} , leaving cell empty`);
                            }
                            friendcount_cell.textContent = data.match(/[0-9]+\/[0-9]+$/);

                            table_row.appendChild(date_cell);
                            table_row.appendChild(name_cell);
                            table_row.appendChild(friendcount_cell);
                            table_body.appendChild(table_row);
                        }
                    } else {
                        let table_row = document.createElement("tr");
                        let table_cell = document.createElement("td");
                        if (!opted_out) {
                            table_cell.innerHTML = "No SR history found.<br>The account was freshly made or has never actively traded on Steam before.";
                        } else {
                            table_cell.innerHTML = "No public SR history found.<br>The account has not actively traded on Steam in the past 30 days.";
                        }

                        table_cell.id = "nohistory-cell";
                        table_cell.colSpan = 3;
                        table_row.appendChild(table_cell);
                        table_body.appendChild(table_row);
                    }
                } else {
                    updateTableStatus(steamrep, res.status, "History");
                }
            }
        },
        ontimeout: function() {
            updateTableStatus(steamrep, "Timed out", "History");
        },
        onerror: function() {
            updateTableStatus(steamrep, "Application error", "History");
        }
    });
}

function getBPTFInfo(p_id64, p_apikey) {
    const important_bans = ["steamrep_scammer", "all features", "price suggestions", "suggestion comments", "trust", "issue tracker", "classifieds", "profile customizations", "reports", "item search"];

    function addBpLinks() {
        const content = document.getElementById("bptf-content");
        const links = document.createElement("div");
        links.innerHTML = `
                          <a class="module-research-link" href="https://backpack.tf/u/${profile.id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Profile</a>
                          <a class="module-research-link" href="https://backpack.tf/profiles/${profile.id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Backpack</a>
                          <a class="module-research-link" href="https://forums.backpack.tf/steam.php?steamid=${profile.id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Forums</a>
                          <a class="module-research-link bp-trust-btn" href="https://backpack.tf/trust/${profile.id64}?mode=for" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Trust (received)</a>
                          <a class="module-research-link bp-trust-btn" href="https://backpack.tf/trust/${profile.id64}?mode=from" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Trust (given)</a>
                          <a class="module-research-link bp-issue-btn" href="https://backpack.tf/issues?text=${profile.id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Issues (recieved)</a>
                          `;
        content.appendChild(links);
    }

    function buildBanListItem(p_type, p_timetext, p_reasontext) {
        const ban_listitem = document.createElement("li");
        const ban_title = document.createElement("b");
        ban_title.textContent = `${p_type} (${p_timetext})`;
        ban_listitem.appendChild(ban_title);

        const ul = document.createElement('ul');
        const ban_details = document.createElement('li');
        ban_details.textContent = p_reasontext;

        ul.appendChild(ban_details);
        ban_listitem.appendChild(ul);

        return ban_listitem;
    }

    function addApiWarning() {
        const warning = document.createElement("div");
        warning.id = `${bptf.id_prefix}-api-warning`;
        warning.innerHTML = `You are currently using the limited API of backpack.tf, which gives less information.<br>
                         To view additional information, you must set the BP.tf API key in the `;
        const link = document.createElement("a");
        link.className = "highlighted-link";
        link.id = `${bptf.id_prefix}-opensettings-link`;
        link.innerText = "settings menu";
        link.onclick = function() {
            openSettingsModal();
            switchSettingSection("Modules");
        };
        warning.appendChild(link);
        const dot = document.createTextNode(".");
        warning.appendChild(dot);
        const target = document.getElementById(`${bptf.id_prefix}-reputation-description`);
        target.appendChild(warning);
    }

    function addBpDetail(p_type, p_data) {
        const detail = document.createElement("div");
        detail.className = `${bptf.id_prefix}-detail-container`;

        if (p_type === "Last visit") {
            const lastvisit_label = document.createElement("b");
            lastvisit_label.textContent = "Last visited: ";
            const visit_time = document.createElement("span");
            visit_time.textContent = p_data ? convertTimestampPrecise(p_data) : "User has never visited backpack.tf before";
            visit_time.className = p_data ? "" : "color-warning";
            detail.appendChild(lastvisit_label);
            detail.appendChild(visit_time);
        } else if (p_type === "Premium") {
            const premium_label = document.createElement("b");
            premium_label.textContent = "Premium: ";
            const premium_status = document.createElement("span");
            premium_status.textContent = p_data ? "Yes" : "No";
            if (p_data) {
                detail.classList.add("detail-premium");
            }
            detail.appendChild(premium_label);
            detail.appendChild(premium_status);
        } else if (p_type === "Donations") {
            const donated_label = document.createElement("b");
            donated_label.textContent = "Donated: ";
            const donation_value = document.createElement("span");
            donation_value.textContent = p_data ? p_data.toFixed(2) + "$" : "0$";
            detail.appendChild(donated_label);
            detail.appendChild(donation_value);
        } else if (p_type === "Current inventory") {
            const inv_label = document.createElement("b");
            inv_label.textContent = "Inventory: ";
            const inv_valuetext = document.createElement("span");
            if (p_data.has_inventory) {
                inv_valuetext.textContent = `Worth ${p_data.inv_value} refined (updated @ ${p_data.inv_updatedat ? convertTimestampPrecise(p_data.inv_updatedat) : "N/A"})`;
            } else {
                inv_valuetext.textContent = "No public TF2 inventory / never updated";
                inv_valuetext.classList.add("color-warning");
            }

            detail.appendChild(inv_label);
            detail.appendChild(inv_valuetext);
        } else if (p_type === "Inventory stats") {
            if (!p_data.has_inventory) { return; }

            const stats_text = document.createElement("span");

            const bp_rank = document.createElement("b");
            bp_rank.textContent = "BP Rank: ";
            const bp_rankvalue = document.createTextNode(p_data.bptf_rank ? `#${p_data.bptf_rank}` : "N/A");
            stats_text.appendChild(bp_rank);
            stats_text.appendChild(bp_rankvalue);

            stats_text.appendChild(document.createTextNode(" | "));

            const slots = document.createElement("b");
            slots.textContent = "Slots: ";
            const slots_content = document.createTextNode(`${p_data.used_slots}/${p_data.total_slots}`);
            stats_text.appendChild(slots);
            stats_text.appendChild(slots_content);

            stats_text.appendChild(document.createTextNode(" | "));

            const pure_keys = document.createElement("b");
            pure_keys.textContent = "Pure keys: ";
            const pure_keysvalue = document.createTextNode(p_data.pure_keys);
            stats_text.appendChild(pure_keys);
            stats_text.appendChild(pure_keysvalue);

            stats_text.appendChild(document.createTextNode(" | "));

            const pure_ref = document.createElement("b");
            pure_ref.textContent = "Pure refined: ";
            const pure_refvalue = document.createTextNode(p_data.pure_ref);
            stats_text.appendChild(pure_ref);
            stats_text.appendChild(pure_refvalue);

            detail.appendChild(stats_text);
        } else if (p_type === "Trust") {
            const total_trusts = p_data.positive_trusts + p_data.negative_trusts;
            const pos_pct = (total_trusts ? p_data.positive_trusts / total_trusts * 100 : 0).toFixed(2);
            const neg_pct = (total_trusts ? p_data.negative_trusts / total_trusts * 100 : 0).toFixed(2);

            const trust_label = document.createElement("b");
            trust_label.textContent = "Trust: ";
            const trust_content = document.createElement("span");
            trust_content.textContent = `+${p_data.positive_trusts} (${pos_pct}%) / -${p_data.negative_trusts} (${neg_pct}%)`;

            if (pos_pct > 0 && neg_pct == 0) { // Only positive trusts
                detail.classList.add('detail-positive');
            } else if (pos_pct > 0 || neg_pct > 0) { // Mixed/negative trusts
                if (pos_pct >= neg_pct) {
                    detail.classList.add('detail-mixed'); // Mixed (more or equal positive trusts compared to negative trusts)
                } else {
                    detail.classList.add('detail-negative'); // Negative (more negative trusts than positive)
                }
            }

            detail.appendChild(trust_label);
            detail.appendChild(trust_content);
        } else if (p_type === "Bans") {
            if (!p_data.is_srbanned && p_data.ban_list.length === 0) { return; }

            const bans_title = document.createElement("b");
            bans_title.textContent = "Current site bans: ";
            bans_title.id = `${bptf.id_prefix}-sitebans-title`;
            const bans_list = document.createElement("ul");
            bans_list.id = `${bptf.id_prefix}-ban-list`;

            if (p_data.is_srbanned) {
                bans_list.appendChild(buildBanListItem("SteamRep", "Permanent", "User is automatically banned from all features due to their SteamRep ban."));
            }

            for (const ban of p_data.ban_list) {
                const type = toTitleCase(ban.type);
                let reason = ban.reason ? ban.reason : "N/A";
                let creation = ban.creation;
                let expiration = ban.expiration;
                let ban_times;
                if (creation && expiration) {
                    ban_times = expiration !== -1 ? `Ends @ ${convertTimestampPrecise(expiration)}, banned ${timeAgo(creation)}` : `Permanent, banned ${timeAgo(creation)}`;
                } else if (expiration) {
                    ban_times = expiration !== -1 ? `Ends @ ${convertTimestampPrecise(expiration)}` : "Permanent";
                } else {
                    ban_times = "End time unknown";
                }

                bans_list.appendChild(buildBanListItem(type, ban_times, reason));
            }

            detail.appendChild(bans_title);
            detail.appendChild(bans_list);
        } else {
            alert(`Programming error: addBpDetailContainer -> Invalid type specified: ${p_type}`);
            return;
        }

        const target = document.getElementById(`${bptf.id_prefix}-reputation-description`);
        target.appendChild(detail);
    }

    function getExtendedInfo(p_id64, p_apikey) {
        GM_xmlhttpRequest({
            method: "GET",
            url: `https://api.backpack.tf/api/users/info/v1?steamids=${p_id64}&key=${p_apikey}`,
            timeout: bptf.api_timeout, // Time to wait (in milliseconds) for a response
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            onreadystatechange: function(res) {
                logXhr(bptf, res.readyState, res.status, "User info API");
                if (res.readyState === 2) {
                    updateRepContainer(bptf, "Loading");
                    updateSummaryStatus(bptf, "Loading");
                } else if (res.readyState === 4) {
                    if (res.status === 200) {
                        const json_response = JSON.parse(res.responseText);
                        let last_visit,
                            donated,
                            premium,
                            has_inventory, inv_value = 0, inv_updatedat,
                            bptf_rank, used_slots = 0, total_slots = 0, pure_keys = 0, pure_ref = 0,
                            positive_trusts = 0, negative_trusts = 0,
                            is_srbanned,
                            has_fullban;
                        let ban_list = [];

                        Object.values(json_response.users).forEach(val => {
                            console.log("[DEBUG] | Backpack.tf (User info API) response:", val);

                            last_visit = val?.last_online;
                            premium = val?.premium;
                            donated = val?.donated ?? 0;

                            const inventory = val?.inventory?.[440];
                            if (inventory) {
                                has_inventory = true;
                                inv_value = inventory?.value ?? 0;
                                inv_updatedat = inventory?.updated;
                                bptf_rank = inventory?.ranking;
                                pure_keys = inventory?.keys ?? 0;
                                pure_ref = Math.floor((inventory?.metal ?? 0) * 100) / 100;
                                used_slots = inventory?.slots?.used ?? 0;
                                total_slots = inventory?.slots?.total ?? 0;
                            }

                            positive_trusts = val?.trust?.positive ?? 0;
                            negative_trusts = val?.trust?.negative ?? 0;

                            if (val.hasOwnProperty("bans")) {
                                for (let ban_type of important_bans) {
                                    if (val.bans.hasOwnProperty(ban_type)) {
                                        const ban_data = val.bans[ban_type];
                                        if (ban_type === "steamrep_scammer") {
                                            is_srbanned = true;
                                        } else if (ban_type === "all features") {
                                            has_fullban = true;
                                            ban_list.push({type: ban_type, reason: ban_data?.reason, expiration: ban_data?.end});
                                        } else {
                                            ban_list.push({type: ban_type, reason: ban_data?.reason, expiration: ban_data?.end});
                                        }
                                    }
                                }
                            }
                        });

                        if (is_srbanned || has_fullban) {
                            updateRepContainer(bptf, "Banned");
                            updateSummaryStatus(bptf, "Banned");
                            username_label.classList.add("username-label-banned");
                        } else if (ban_list.length > 0) {
                            updateRepContainer(bptf, "Feature Banned");
                            updateSummaryStatus(bptf, "Feature Banned");
                        } else if (last_visit) {
                            updateRepContainer(bptf, "Normal");
                            updateSummaryStatus(bptf, "Normal");
                        } else {
                            updateRepContainer(bptf, "No account");
                            updateSummaryStatus(bptf, "No account");
                        }

                        addBpDetail("Last visit", last_visit);
                        addBpDetail("Premium", premium);
                        addBpDetail("Donations", donated);
                        addBpDetail("Current inventory", {has_inventory, inv_value, inv_updatedat});
                        addBpDetail("Inventory stats", {has_inventory, bptf_rank, used_slots, total_slots, pure_keys, pure_ref});
                        addBpDetail("Trust", {positive_trusts, negative_trusts});
                        addBpDetail("Bans", {is_srbanned, ban_list});

                        updateRepCountSummary(bptf, positive_trusts, negative_trusts);
                    } else {
                        updateRepContainer(bptf, res.status);
                        updateSummaryStatus(bptf, res.status);
                    }
                }
            },
            ontimeout: function() {
                updateRepContainer(bptf, "Timed out");
                updateSummaryStatus(bptf, "Timed out");
            },
            onerror: function() {
                updateRepContainer(bptf, "Application error");
                updateSummaryStatus(bptf, "Application error");
            }
        });
    }

    function getLimitedInfo(p_id64) {
        GM_xmlhttpRequest({
            method: "GET",
            url: `https://backpack.tf/api/IGetUsers/v3?steamid=${p_id64}`,
            timeout: bptf.api_timeout, // Time to wait (in milliseconds) for a response
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            onreadystatechange: function(res) {
                logXhr(bptf, res.readyState, res.status, "IGetUsers v3 API");
                if (res.readyState === 2) {
                    updateRepContainer(bptf, "Loading");
                    updateSummaryStatus(bptf, "Loading");
                } else if (res.readyState === 4) {
                    if (res.status === 200) {
                        const json_response = JSON.parse(res.responseText);
                        let is_srbanned, has_fullban, has_inventory, inv_value, inv_updatedat, positive_trusts, negative_trusts;
                        let ban_list = [];

                        Object.values(json_response.response.players).forEach(val => {
                            console.log("[DEBUG] | Backpack.tf (IGetUsers v3 API) response:", val);

                            inv_value = val?.backpack_value?.[440] ? val?.backpack_value?.[440].toFixed(2) : 0;
                            inv_updatedat = val?.backpack_update?.[440];
                            has_inventory = inv_value || inv_updatedat;

                            positive_trusts = val?.backpack_tf_trust?.for ?? 0;
                            negative_trusts = val?.backpack_tf_trust?.against ?? 0;

                            is_srbanned = val?.steamrep_scammer ?? false;

                            if (val.hasOwnProperty("backpack_tf_bans")) {
                                for (let ban_type of important_bans) {
                                    if (val.backpack_tf_bans.hasOwnProperty(ban_type)) {
                                        const ban_data = val.backpack_tf_bans[ban_type];
                                        if (ban_type === "all features") {
                                            has_fullban = true;
                                            ban_list.push({type: ban_type, reason: ban_data?.reason, creation: ban_data?.start, expiration: ban_data?.end});
                                        } else {
                                            ban_list.push({type: ban_type, reason: ban_data?.reason, creation: ban_data?.start, expiration: ban_data?.end});
                                        }
                                    }
                                }
                            }
                        });

                        if (is_srbanned || has_fullban) {
                            updateRepContainer(bptf, "Banned");
                            updateSummaryStatus(bptf, "Banned");
                            username_label.classList.add("username-label-banned");
                        } else if (ban_list.length > 0) {
                            updateRepContainer(bptf, "Feature Banned");
                            updateSummaryStatus(bptf, "Feature Banned");
                        } else {
                            updateRepContainer(bptf, "Normal");
                            updateSummaryStatus(bptf, "Normal");
                        }

                        addApiWarning();
                        addBpDetail("Current inventory", {has_inventory, inv_value, inv_updatedat});
                        addBpDetail("Trust", {positive_trusts, negative_trusts});
                        addBpDetail("Bans", {is_srbanned, ban_list});

                        updateRepCountSummary(bptf, positive_trusts, negative_trusts);
                    } else {
                        updateRepContainer(bptf, res.status);
                        updateSummaryStatus(bptf, res.status);
                    }
                }
            },
            ontimeout: function() {
                updateRepContainer(bptf, "Timed out");
                updateSummaryStatus(bptf, "Timed out");
            },
            onerror: function() {
                updateRepContainer(bptf, "Application error");
                updateSummaryStatus(bptf, "Application error");
            }
        });
    }

    addBpLinks();
    updateRepContainer(bptf, "Connecting");
    updateSummaryStatus(bptf, "Connecting");
    if (p_apikey && !bptf.force_limitedapi) {
        getExtendedInfo(p_id64, p_apikey);
    } else {
        getLimitedInfo(p_id64);
    }
}

function getManncoInfo(p_id64)
{
    let content = document.getElementById(`${mannco.id_prefix}-content`);
    let links_html = `<br><a class="module-research-link" href="https://mannco.store/store/${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${MANNCO_ICON});"></i>Store</a>`;
    updateRepContainer(mannco, "Connecting");
    updateSummaryStatus(mannco, "Connecting");

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://mannco.store/API/userBanSteamDetective.php?id=${p_id64}`,
        timeout: mannco.api_timeout,
        onreadystatechange: function(res) {
            if (res.readyState === 2) {
                updateRepContainer(mannco, "Loading");
                updateSummaryStatus(mannco, "Loading");
            } else if (res.readyState === 4) {
                if (res.status === 200) {
                    let raw_response = res.responseText;
                    // Replace parts of the response text to be JSON parsable
                    raw_response = raw_response.replace('ok:', '"ok":');
                    raw_response = raw_response.replace('result:', '"result":');
                    raw_response = raw_response.replace('reason:', '"reason":');

                    const json_response = JSON.parse(raw_response);
                    if (json_response.hasOwnProperty("ok")) {
                        if (json_response.result === "banned") {
                            updateRepContainer(mannco, "Banned");
                            updateSummaryStatus(mannco, "Banned");
                            document.getElementById("mannco-banreason").textContent = json_response.reason;
                            content.innerHTML += links_html;
                        } else if (json_response.result === "ok") {
                            updateRepContainer(mannco, "Normal");
                            updateSummaryStatus(mannco, "Normal");
                            content.innerHTML += links_html;
                        } else if (json_response.result === "NF") {
                            updateRepContainer(mannco, "No account");
                            updateSummaryStatus(mannco, "No account");
                        }
                    }
                } else {
                    updateRepContainer(mannco, res.status);
                    updateSummaryStatus(mannco, res.status);
                    content.innerHTML += links_html;
                }
            }
        },
        ontimeout: function() {
            updateRepContainer(mannco, "Timed out");
            updateSummaryStatus(mannco, "Timed out");
            content.innerHTML += links_html;
        },
        onerror: function() {
            updateRepContainer(mannco, "Application error");
            updateSummaryStatus(mannco, "Application error");
            content.innerHTML += links_html;
        }
    });
}

// Gets Marketplace and Scrap.tf info
function getRepTfInfo(p_id64) {
    let mptf_content = document.getElementById(`${marketplace.id_prefix}-content`);
    let mptf_linkshtml = `<br><a class="module-research-link" href="https://marketplace.tf/shop/${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${MPTF_ICON});"></i>Shop</a>`;
    if (marketplace.is_enabled) { mptf_content.insertAdjacentHTML("beforeend", mptf_linkshtml); }
    let scrap_content = document.getElementById(`${scrap.id_prefix}-content`);
    let scrap_linkshtml = `<br><a class="module-research-link" href="https://scrap.tf/profile/${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SCRAP_ICON});"></i>Profile</a>`;

    updateRepContainer(marketplace, "Connecting");
    updateSummaryStatus(marketplace, "Connecting");
    updateRepContainer(scrap, "Connecting");
    updateSummaryStatus(scrap, "Connecting");

    GM_xmlhttpRequest({
        method: "POST",
        url: `https://rep.tf/api/bans?str=${p_id64}`,
        timeout: reptf.api_timeout, // Time to wait (in milliseconds) for a response
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        onreadystatechange: function(res) {
            logXhr(reptf, res.readyState, res.status);
            if (res.readyState === 2) {
                updateRepContainer(marketplace, "Loading");
                updateSummaryStatus(marketplace, "Loading");
                updateRepContainer(scrap, "Loading");
                updateSummaryStatus(scrap, "Loading");
            } else if (res.readyState === 4) {
                if (res.status === 200) {
                    let json_response = JSON.parse(res.responseText);
                    console.log("[DEBUG] | Rep.tf response:", json_response);

                    // Marketplace.tf
                    if (marketplace.is_enabled) {
                        if (json_response.mpBans.banned === "good") {
                            updateRepContainer(marketplace, "Normal");
                            updateSummaryStatus(marketplace, "Normal");
                        } else if (json_response.mpBans.banned === "bad") {
                            updateRepContainer(marketplace, "Banned");
                            updateSummaryStatus(marketplace, "Banned");
                            document.getElementById(`${marketplace.id_prefix}-reputation-description`).innerHTML = json_response.mpBans.message.replace(/<\/b>:/g, ":<\/b>");
                        } else if (json_response.mpBans.message.indexOf("Failed to get data") != -1) {
                            updateRepContainer(marketplace, "Rep.tf get data fail");
                            updateSummaryStatus(marketplace, "Rep.tf get data fail");
                        }
                    }

                    // Scrap.tf
                    if (scrap.is_enabled) {
                        if (json_response.stfBans.banned === "good" && json_response.stfBans.message.indexOf("Member since") != -1) {
                            updateRepContainer(scrap, "Normal");
                            updateSummaryStatus(scrap, "Normal");
                            document.getElementById(`${scrap.id_prefix}-membership`).innerHTML = json_response.stfBans.message.replace('No active bans.<br/>','')
                                                                                                                              .replace(/<a[^>]*>(.*?)<\/a>/g, '$1');
                            scrap_content.insertAdjacentHTML("beforeend", scrap_linkshtml);
                        } else if (json_response.stfBans.banned === "good" && json_response.stfBans.message.indexOf("Member since") === -1) {
                            updateRepContainer(scrap, "No account");
                            updateSummaryStatus(scrap, "No account");
                        } else if (json_response.stfBans.banned === "bad") {
                            updateRepContainer(scrap, "Banned");
                            updateSummaryStatus(scrap, "Banned");

                            let ban_html = json_response.stfBans.message.replace(/<\/b>:/g, ":<\/b>")
                                                                        .replace(/<a[^>]*>(.*?)<\/a>/g, '$1');

                            document.getElementById(`${scrap.id_prefix}-banreason`).innerHTML = ban_html;

                            if (json_response.stfBans.message.indexOf("Member since") != -1) {
                                scrap_content.insertAdjacentHTML("beforeend", scrap_linkshtml);
                            }
                        } else if (json_response.stfBans.message.indexOf("Failed to get data") != -1) {
                            updateRepContainer(scrap, "Rep.tf get data fail");
                            updateSummaryStatus(scrap, "Rep.tf get data fail");
                            scrap_content.insertAdjacentHTML("beforeend", scrap_linkshtml);
                        }
                    }
                } else {
                    updateRepContainer(marketplace, res.status);
                    updateSummaryStatus(marketplace, res.status);
                    updateRepContainer(scrap, res.status);
                    updateSummaryStatus(scrap, res.status);
                    if (scrap.is_enabled) { scrap_content.insertAdjacentHTML("beforeend", scrap_linkshtml); }
                }
            }
        },
        ontimeout: function () {
            updateRepContainer(marketplace, "Timed out");
            updateSummaryStatus(marketplace, "Timed out");
            updateRepContainer(scrap, "Timed out");
            updateSummaryStatus(scrap, "Timed out");
            if (scrap.is_enabled) { scrap_content.insertAdjacentHTML("beforeend", scrap_linkshtml); }
        },
        onerror: function() {
            updateRepContainer(marketplace, "Application error");
            updateSummaryStatus(marketplace, "Application error");
            updateRepContainer(scrap, "Application error");
            updateSummaryStatus(scrap, "Application error");
            if (scrap.is_enabled) { scrap_content.insertAdjacentHTML("beforeend", scrap_linkshtml); }
        }
    });
}

function checkSkialPresence(p_id3) {
    let content = document.getElementById(`${skial.id_prefix}-content`);
    let links_html = `<br><a class="module-research-link" href="https://stats.skial.com/#summary/player/SteamID/All/${p_id3}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SKIAL_ICON});"></i>Stats</a>
                      <a class="module-research-link" href="https://www.skial.com/sourcebans/index.php?p=banlist&advSearch=${p_id3}&advType=steamid" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SKIAL_ICON});"></i>Bans</a>`;
    let id = p_id3.replace(/\[|\]|(U:1:)/g, "");

    updateRepContainer(skial, "Connecting");
    updateSummaryStatus(skial, "Connecting");

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://stats.skial.com/api?type=search_player&page=0&id=${id}`,
        timeout: skial.api_timeout, // Time to wait (in milliseconds) for a response
        cookiePartition: { topLevelSite: 'https://stats.skial.com' },
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        onreadystatechange: function(res) {
            logXhr(skial, res.readyState, res.status, "Player search");
            if (res.readyState === 2) {
                updateRepContainer(skial, "Loading");
                updateSummaryStatus(skial, "Loading");
            }
            else if (res.readyState === 4) {
                if (res.status === 200) {
                    let json_response = JSON.parse(res.responseText);

                    if (json_response.hasOwnProperty("Results")) {
                        let results = json_response.Results;

                        if (results.length > 0) {
                            updateRepContainer(skial, "Normal");
                            updateSummaryStatus(skial, "Normal");

                            content.insertAdjacentHTML("beforeend", links_html);

                            // Server history
                            let skial_servers = document.createElement("div");
                            skial_servers.id = "skial-servers";
                            content.appendChild(skial_servers);

                            let table = document.createElement("table");
                            table.id = "skial-servers-table";
                            table.className = "history-table";

                            let table_head = document.createElement("thead");
                            table_head.innerHTML = `
                                                   <tr>
                                                       <th colspan="3" class="history-table-head" id="skial-servers-table-head">Visited servers</th>
                                                   </tr>
                                                   <tr style='background:#001c8a;position:sticky;top:0;'><td>Server type</td><td>Username</td><td>Last seen</td></tr>
                                                   `;
                            let table_body = document.createElement("tbody");
                            table.appendChild(table_head);
                            table.appendChild(table_body);

                            document.getElementById("skial-servers").appendChild(table);

                            for (let r = 0; r < results.length; r++) {
                                let table_row = document.createElement("tr");
                                let servername_cell = document.createElement("td");
                                let username_cell = document.createElement("td");
                                let date_cell = document.createElement("td");

                                servername_cell.textContent = results[r].Game;
                                username_cell.textContent = results[r].Name;
                                date_cell.textContent = convertTimestampPrecise(results[r].Time);

                                table_row.appendChild(servername_cell);
                                table_row.appendChild(username_cell);
                                table_row.appendChild(date_cell);
                                table_body.appendChild(table_row);
                            }

                            // Bans
                            let skial_bans = document.createElement("div");
                            skial_bans.id = `${skial.id_prefix}-bans`;
                            content.appendChild(skial_bans);

                            table = document.createElement("table");
                            table.id = `${skial.id_prefix}-bans-table`;
                            table.className = "history-table";

                            table_head = document.createElement("thead");
                            table_head.innerHTML = `
                                                    <tr>
                                                        <th colspan="4" class="history-table-head" id="${skial.id_prefix}-bans-table-head">Bans</th>
                                                    </tr>
                                                    <tr style='background:#001c8a;position:sticky;top:0;'><td>Name</td><td>Reason</td><td>Ban date</td><td>Ban length</td></tr>
                                                    `;
                            table_body = document.createElement("tbody");
                            table_body.id = `${skial.id_prefix}-bans-table-body`;
                            table.appendChild(table_head);
                            table.appendChild(table_body);

                            document.getElementById(`${skial.id_prefix}-bans`).appendChild(table);
                            getSkialBans(p_id3, skial.hide_inactivebans);
                        } else {
                            updateRepContainer(skial, "No account");
                            updateSummaryStatus(skial, "No account");
                        }
                    } else {
                        updateRepContainer(skial, "Missing data");
                        updateSummaryStatus(skial, "Missing data");
                    }
                } else {
                    updateRepContainer(skial, res.status);
                    updateSummaryStatus(skial, res.status);
                }
            }
        },
        ontimeout: function() {
            updateRepContainer(skial, "Timed out");
            updateSummaryStatus(skial, "Timed out");
            content.insertAdjacentHTML("beforeend", links_html);
        },
        onerror: function() {
            updateRepContainer(skial, "Application error");
            updateSummaryStatus(skial, "Application error");
            content.insertAdjacentHTML("beforeend", links_html);
        }
    });
}

function getSkialBans(p_id3, p_hideinactive = false) {
    document.getElementById(`${skial.id_prefix}-bans-table-head`).textContent = "Bans - Connecting...";
    GM_xmlhttpRequest({
        method: "GET",
        url: `https://www.skial.com/sourcebans/index.php?p=banlist&hideinactive=${p_hideinactive}&advSearch=${p_id3}&advType=steamid`,
        timeout: skial.api_timeout, // Time to wait (in milliseconds) for a response
        cookiePartition: { topLevelSite: 'https://www.skial.com' },
        onreadystatechange: function(res) {
            logXhr(skial, res.readyState, res.status, `Bans`);
            if (res.readyState === 2) {
                document.getElementById(`${skial.id_prefix}-bans-table-head`).textContent = "Bans - Loading...";
            } else if (res.readyState === 4) {
                if (res.status === 200) {
                    let dp = new DOMParser();
                    let html_response = dp.parseFromString(res.responseText,"text/html");
                    let bans = html_response.querySelectorAll("#banlist .listtable .listtable > tbody");
                    let table_body = document.getElementById(`${skial.id_prefix}-bans-table-body`);

                    if (bans.length > 0) {
                        let active_bans = 0, inactive_bans = 0;
                        for (let i = 0; i < bans.length; i++) {
                            try {
                                if (bans[i].childElementCount === 13 || bans[i].childElementCount === 15) {
                                    let table_row = document.createElement("tr");
                                    let username_cell = document.createElement("td");
                                    let reason_cell = document.createElement("td");
                                    let date_cell = document.createElement("td");
                                    let length_cell = document.createElement("td");
                                    let date_timestamp = bans[i].querySelector(".timestamp");
                                    let reason_index;

                                     if (bans[i].childElementCount === 13) {
                                        // Active ban
                                        reason_index = 8;
                                        active_bans++;
                                    } else {
                                        // Inactive/unbanned
                                        reason_index = 10;
                                        inactive_bans++;
                                    }

                                    if (checkSourcebanDataPresent(bans[i].children[1].children[1].innerHTML, "Username")) {
                                        username_cell.textContent = decodeHTMLEntities(bans[i].children[1].children[1].textContent);
                                    } else {
                                        username_cell.innerHTML = '<span class="color-warning">N/A</span>';
                                    }

                                    if (checkSourcebanDataPresent(bans[i].children[reason_index].children[1].innerHTML, "Ban reason")) {
                                        reason_cell.textContent = decodeHTMLEntities(bans[i].children[reason_index].children[1].textContent);
                                    } else {
                                        reason_cell.innerHTML = '<span class="color-warning">N/A</span>';
                                    }

                                    if (date_timestamp) {
                                        date_cell.textContent = convertTimestampPrecise(date_timestamp.getAttribute("data-utc"));
                                    } else {
                                        date_cell.innerHTML = '<span class="color-warning">N/A</span>';
                                    }

                                    length_cell.textContent = bans[i].children[6].children[1].textContent;

                                    table_row.appendChild(username_cell);
                                    table_row.appendChild(reason_cell);
                                    table_row.appendChild(date_cell);
                                    table_row.appendChild(length_cell);
                                    table_body.appendChild(table_row);
                                }
                            } catch (e) {
                                console.log(`%c[DEBUG] | Skial bans parsing failed at ban index ${i} | ${e}`, "color: red; font-size: 12px;");
                            }
                        }

                        if (active_bans) {
                            updateRepContainer(skial, "Banned");
                            updateSummaryStatus(skial, "Banned");
                        } else {
                            updateRepContainer(skial, "Unbanned");
                            updateSummaryStatus(skial, "Unbanned");
                        }
                    } else {
                        let table_row = document.createElement("tr");
                        let noban_cell = document.createElement("td");
                        noban_cell.textContent = "No bans found.";
                        noban_cell.className = "error-cell";
                        noban_cell.colSpan = 4;
                        table_row.appendChild(noban_cell);
                        table_body.appendChild(table_row);
                    }

                    document.getElementById(`${skial.id_prefix}-bans-table-head`).textContent = "Bans";
                } else {
                    updateTableStatus(skial, res.status, "Bans", "bans", 4);
                }
            }
        },
        ontimeout: function() {
            updateTableStatus(skial, "Timed out", "Bans", "bans", 4);
        },
        onerror: function() {
            updateTableStatus(skial, "Application error", "Bans", "bans", 4);
        }
    });
}

function getFirePoweredInfo(p_id2, p_hideinactive = false) {
    function getBanTimestamp(p_datestring) {
        const date = new Date(p_datestring + " GMT");
        return Math.floor(date.getTime() / 1000);
    }

    let content = document.getElementById(`${firepowered.id_prefix}-content`);
    let links_html = `<br><a class="module-research-link" href="${firepowered.profile_url}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${FIREPOWERED_ICON});"></i>Bans</a>`;
    content.insertAdjacentHTML("beforeend", links_html);
    updateRepContainer(firepowered, "Connecting");
    updateSummaryStatus(firepowered, "Connecting");

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://firepoweredgaming.com/sourcebanspp/index.php?p=banlist&hideinactive=${p_hideinactive}&advSearch=${p_id2}&advType=steamid`,
        timeout: firepowered.api_timeout, // Time to wait (in milliseconds) for a response
        onreadystatechange: function(res) {
            if (res.readyState === 2) {
                updateRepContainer(firepowered, "Loading");
                updateSummaryStatus(firepowered, "Loading");
            } else if (res.readyState === 4) {
                if (res.status === 200) {
                    let dp = new DOMParser();
                    let html_response = dp.parseFromString(res.responseText,"text/html");
                    let bans = html_response.querySelectorAll("#banlist .listtable .listtable > tbody");

                    if (bans.length > 0) {
                        let container = document.getElementById(`${firepowered.id_prefix}-info-container`);
                        let bans_container = document.createElement("div");
                        bans_container.id = `${firepowered.id_prefix}-bans`;
                        container.appendChild(bans_container);

                        let table = document.createElement("table");
                        table.id = `${firepowered.id_prefix}-bans-table`;
                        table.className = "history-table";

                        let table_head = document.createElement("thead");
                        table_head.innerHTML = `
                                               <tr>
                                                   <th colspan="4" class="history-table-head" id="${firepowered.id_prefix}-bans-table-head">Bans</th>
                                               </tr>
                                               <tr style='background:#001c8a;position:sticky;top:0;'><td>Name</td><td>Reason</td><td>Ban date</td><td>Ban length</td></tr>
                                               `;
                        let table_body = document.createElement("tbody");
                        table_body.id = `${firepowered.id_prefix}-bans-table-body`;
                        table.appendChild(table_head);
                        table.appendChild(table_body);
                        bans_container.appendChild(table);

                        let active_bans = 0, inactive_bans = 0, unexpected_data = false;
                        for (let i = 0; i < bans.length; i++) {
                            try {
                                if (bans[i].childElementCount === 14 || bans[i].childElementCount === 16) {
                                    // Active ban
                                    let table_row = document.createElement("tr");
                                    let username_cell = document.createElement("td");
                                    let reason_cell = document.createElement("td");
                                    let date_cell = document.createElement("td");
                                    let length_cell = document.createElement("td");
                                    let reason_index, date_index = 6, length_index = 7;

                                    if (bans[i].childElementCount === 14) {
                                        reason_index = 9;
                                        if (bans[i].children[6].children[0].textContent != "Unban reason") {
                                            // Active ban
                                            active_bans++;
                                        } else {
                                            // Inactive/unbanned - SteamRep and Steam Community rows missing
                                            inactive_bans++;
                                            date_index -= 2;
                                            length_index -= 2;
                                        }
                                    } else {
                                        // Inactive/unbanned
                                        reason_index = 11;
                                        inactive_bans++;
                                    }

                                    if (checkSourcebanDataPresent(bans[i].children[1].children[1].innerHTML, "Username")) {
                                        username_cell.textContent = decodeHTMLEntities(bans[i].children[1].children[1].textContent);
                                    } else {
                                        username_cell.innerHTML = '<span class="color-warning">N/A</span>';
                                    }

                                    if (checkSourcebanDataPresent(bans[i].children[reason_index].children[1].innerHTML, "Ban reason")) {
                                        reason_cell.textContent = decodeHTMLEntities(bans[i].children[reason_index].children[1].textContent);
                                    } else {
                                        reason_cell.innerHTML = '<span class="color-warning">N/A</span>';
                                    }

                                    // Adjusts ban date and time to user's local time
                                    const ban_timestamp = getBanTimestamp(bans[i].children[date_index].children[1].textContent);
                                    if (!isNaN(ban_timestamp)) {
                                        date_cell.textContent = convertTimestampPrecise(ban_timestamp);
                                    } else {
                                        date_cell.innerHTML = '<span class="color-warning">N/A</span>';
                                    }
                                    length_cell.textContent = bans[i].children[length_index].children[1].textContent;

                                    table_row.appendChild(username_cell);
                                    table_row.appendChild(reason_cell);
                                    table_row.appendChild(date_cell);
                                    table_row.appendChild(length_cell);
                                    table_body.appendChild(table_row);
                                } else {
                                    unexpected_data = true;
                                    console.log(`%c[DEBUG] | FirePowered ban parsing found unexpected data at ban index ${i}, bans[i].childElementCount = ${bans[i].childElementCount}`, "color: red; font-size: 12px;");
                                    break;
                                }
                            } catch (e) {
                                console.log(`%c[DEBUG] | FirePowered bans parsing failed at ban index ${i} | ${e}`, "color: red; font-size: 12px;");
                            }
                        }

                        if (active_bans) {
                            updateRepContainer(firepowered, "Banned");
                            updateSummaryStatus(firepowered, "Banned");
                        } else if (!unexpected_data) {
                            updateRepContainer(firepowered, "Unbanned");
                            updateSummaryStatus(firepowered, "Unbanned");
                        } else {
                            updateRepContainer(firepowered, "Unexpected data");
                            updateSummaryStatus(firepowered, "Unexpected data");
                        }
                    } else {
                        updateRepContainer(firepowered, "Normal");
                        updateSummaryStatus(firepowered, "Normal");
                    }
                } else {
                    updateRepContainer(firepowered, res.status);
                    updateSummaryStatus(firepowered, res.status);
                }
            }
        },
        ontimeout: function() {
            updateRepContainer(firepowered, "Timed out");
            updateSummaryStatus(firepowered, "Timed out");
        },
        onerror: function() {
            updateRepContainer(firepowered, "Application error");
            updateSummaryStatus(firepowered, "Application error");
        }
    });
}

function getSteamIdUkInfo(p_myid, p_apikey, p_profileid64) {
    let content = document.getElementById(`${steamiduk.id_prefix}-content`);
    let links_html = `<br><a class="module-research-link" href="https://steamid.uk/profile/${p_profileid64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${STEAMIDUK_ICON});"></i>Profile</a>`;

    if (p_myid != "" && p_apikey != "") {
        updateRepContainer(steamiduk, "Connecting");
        updateSummaryStatus(steamiduk, "Connecting");

        GM_xmlhttpRequest({
            method: "GET",
            url: `https://steamidapi.uk/v2/steamid.php?myid=${p_myid}&apikey=${p_apikey}&input=${p_profileid64}`,
            timeout: steamiduk.api_timeout, // Time to wait (in milliseconds) for a response
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            onreadystatechange: function(res) {
                if (res.readyState === 2) {
                    updateRepContainer(steamiduk, "Loading");
                    updateSummaryStatus(steamiduk, "Loading");
                } else if (res.readyState === 4) {
                    if (res.status === 200) {
                        var json_response = JSON.parse(res.responseText);

                        if (json_response.hasOwnProperty("auth")) {
                            if (json_response.profile_bans.steamid_ban != 1) {
                                updateRepContainer(steamiduk, "Normal");
                                updateSummaryStatus(steamiduk, "Normal");
                            } else if (json_response.profile_bans.steamid_ban == 1) {
                                updateRepContainer(steamiduk, "Banned");
                                updateSummaryStatus(steamiduk, "Banned");
                            }

                            let optout_label = "";

                            if (json_response.steamid_data.steamid_optout == 1) {
                                optout_label = `<div class="sid-detail-container detail-negative" style="margin-bottom:-15px;">
                                                    <p style='display:inline;'>This user has opted out of SteamID and has had historical data removed.</p>
                                                </div>`;
                            }

                            content.innerHTML = `<div class="sid-detail-container">
                                                     <p style='display:inline;'><b>Daily profile lookups:</b> ${json_response.auth.daily_count}/${json_response.auth.daily_limit}</p>
                                                 </div>
                                                 ${optout_label}`;

                            if (json_response.steamid_data.steamid_optout != 1) {
                                content.innerHTML += `<div class="sid-detail-container" style="margin-bottom:-15px;">
                                                          <p><b>Name changes recorded:</b> ${json_response.steamid_data.name_history_count}</p>
                                                          <p><b>Custom URL changes recorded:</b> ${json_response.steamid_data.url_changes}</p>
                                                      </div>
                                                     `;
                            }
                        } else if (json_response.hasOwnProperty("error")) {
                            if (json_response.error.hasOwnProperty("errorid")) {
                                if (json_response.error.errorid == 0) {
                                    updateRepContainer(steamiduk, "Database connectivity issue");
                                    updateSummaryStatus(steamiduk, "Database issue");
                                } else if (json_response.error.errorid == 1) {
                                    updateRepContainer(steamiduk, "ID64 not found");
                                    updateSummaryStatus(steamiduk, "ID64 not found");
                                } else if (json_response.error.errorid == 3) {
                                    updateRepContainer(steamiduk, "Invalid ID64/API Key settings");
                                    updateSummaryStatus(steamiduk, "Invalid settings");
                                } else if (json_response.error.errorid == 4) {
                                    updateRepContainer(steamiduk, "Daily lookup limit reached");
                                    updateSummaryStatus(steamiduk, "Lookup limit reached");
                                } else if (json_response.error.errorid == 9) {
                                    updateRepContainer(steamiduk, "API Key Disabled");
                                    updateSummaryStatus(steamiduk, "API Key Disabled");
                                } else if (json_response.error.errorid == 10) {
                                    updateRepContainer(steamiduk, "Internal API error");
                                    updateSummaryStatus(steamiduk, "Internal API error");
                                }
                            } else {
                                updateRepContainer(steamiduk, "Unknown error");
                                updateSummaryStatus(steamiduk, "Unknown error");
                            }
                        }
                    } else {
                        updateRepContainer(steamiduk, res.status);
                        updateSummaryStatus(steamiduk, res.status);
                    }

                    content.innerHTML += links_html;
                }
            },
            ontimeout: function() {
                updateRepContainer(steamiduk, "Timed out");
                updateSummaryStatus(steamiduk, "Timed out");
                content.innerHTML += links_html;
            },
            onerror: function() {
                updateRepContainer(steamiduk, "Application error");
                updateSummaryStatus(steamiduk, "Application error");
                content.innerHTML += links_html;
            }
        });
    } else if (p_myid === "" && p_apikey === "") {
        updateRepContainer(steamiduk, "API Key Required");
        updateSummaryStatus(steamiduk, "API Key Required");
        content.innerHTML += links_html;
    } else if (p_myid === "" && p_apikey != "") {
        updateRepContainer(steamiduk, "ID64 not set");
        updateSummaryStatus(steamiduk, "ID64 not set");
        content.innerHTML += links_html;
    } else if (p_myid != "" && p_apikey === "") {
        updateRepContainer(steamiduk, "API Key not set");
        updateSummaryStatus(steamiduk, "API Key not set");
        content.innerHTML += links_html;
    }
}

function getSteamTradesInfo(p_id64) {
    let content = document.getElementById(`${steamtrades.id_prefix}-content`);
    let info = document.getElementById(`${steamtrades.id_prefix}-info-container`);
    let links_html = `<br><a class="module-research-link" href="https://www.steamtrades.com/user/${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${STEAMTRADES_ICON});"></i>Profile</a>
                          <a class="module-research-link" href="https://www.steamtrades.com/trades/search?user=${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${STEAMTRADES_ICON});"></i>Trades</a>`;
    updateRepContainer(steamtrades, "Connecting");
    updateSummaryStatus(steamtrades, "Connecting");

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://www.steamtrades.com/user/${p_id64}`,
        timeout: steamtrades.api_timeout, // Time to wait (in milliseconds) for a response
        onreadystatechange: function(res) {
            if (res.readyState === 2) {
                updateRepContainer(steamtrades, "Loading");
                updateSummaryStatus(steamtrades, "Loading");
            } else if (res.readyState === 4) {
                if (res.status === 200) {
                    let dp = new DOMParser();
                    let html_response = dp.parseFromString(res.responseText,"text/html");
                    let regdate_text = html_response.querySelector("div.sidebar_table:nth-child(7) > div:nth-child(1) > div:nth-child(2)");
                    let notification = html_response.querySelector(".notification");

                    if (notification) {
                        if (notification.innerText === "This account has been permanently removed.") {
                            updateRepContainer(steamtrades, "Deleted account");
                            updateSummaryStatus(steamtrades, "Deleted account");
                            content.innerHTML += `<br><a class="module-research-link" href="https://www.steamtrades.com/user/${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${STEAMTRADES_ICON});"></i>Profile</a>`;
                            return;
                        }
                    }

                    if (regdate_text) {
                        if (regdate_text.innerText != "-") {
                            let table_container = document.createElement("div");
                            table_container.id = `${steamtrades.id_prefix}-details`;
                            info.appendChild(table_container);

                            let table = document.createElement("table");
                            table.id = `${steamtrades.id_prefix}-details-table`;

                            let table_body = document.createElement("tbody");
                            table_body.id = `${steamtrades.id_prefix}-details-table-body`;

                            let registration_row = document.createElement("tr");
                            registration_row.id = `${steamtrades.id_prefix}-registration-row`;
                            registration_row.innerHTML = "<td>Registered</td>";
                            let registration_cell = document.createElement("td");
                            registration_cell.id = `${steamtrades.id_prefix}-registration-date`;
                            registration_cell.classList.toggle("color-warning");
                            registration_cell.textContent = "Unknown";
                            registration_row.appendChild(registration_cell);

                            let lastonline_row = document.createElement("tr");
                            lastonline_row.id = `${steamtrades.id_prefix}-lastonline-row`;
                            lastonline_row.innerHTML = "<td>Last online</td>";
                            let lastonline_cell = document.createElement("td");
                            lastonline_cell.id = `${steamtrades.id_prefix}-lastonline-date`;
                            lastonline_cell.classList.toggle("color-warning");
                            lastonline_cell.textContent = "No data found";
                            lastonline_row.appendChild(lastonline_cell);

                            let reputation_row = document.createElement("tr");
                            reputation_row.id = `${steamtrades.id_prefix}-reputation-row`;
                            reputation_row.innerHTML = "<td>Reputation</td>";
                            let reputation_cell = document.createElement("td");
                            reputation_cell.id = `${steamtrades.id_prefix}-reputation-date`;
                            reputation_cell.classList.toggle("color-warning");
                            reputation_cell.textContent = "No data found";
                            reputation_row.appendChild(reputation_cell);

                            table_body.appendChild(registration_row);
                            table_body.appendChild(lastonline_row);
                            table_body.appendChild(reputation_row);

                            let regdate_data = html_response.querySelector("div.sidebar_table:nth-child(7) > div:nth-child(1) > div:nth-child(2) > span:nth-child(1)");
                            let lastonline_data = html_response.querySelector("div.sidebar_table:nth-child(7) > div:nth-child(2) > div:nth-child(2) > span:nth-child(1)");
                            let lastonline_text = html_response.querySelector("div.sidebar_table:nth-child(7) > div:nth-child(2) > div:nth-child(2)");
                            let positive_rep = html_response.querySelector(".increment_positive_review_count");
                            let negative_rep = html_response.querySelector(".increment_negative_review_count");

                            if (regdate_data) {
                                let registration_timestamp = regdate_data.getAttribute("data-timestamp");
                                if (registration_timestamp) {
                                    registration_cell.classList.toggle("color-warning");
                                    registration_cell.textContent = convertTimestampPrecise(registration_timestamp, true);
                                } else {
                                    console.log(`[DEBUG] | ${steamtrades.module_name} : No registration timestamp attribute found.`);
                                }
                            } else {
                                console.log(`[DEBUG] | ${steamtrades.module_name} : No registration date data found.`);
                            }

                            if (lastonline_data) {
                                let lastonline_timestamp = lastonline_data.getAttribute("data-timestamp");
                                if (lastonline_timestamp) {
                                    lastonline_cell.classList.toggle("color-warning");
                                    lastonline_cell.textContent = convertTimestampPrecise(lastonline_timestamp, true);
                                } else {
                                    console.log(`[DEBUG] | ${steamtrades.module_name} : No last online timestamp attribute found.`);
                                }
                            } else if (lastonline_text) {
                                if (lastonline_text.textContent === "Now") {
                                    lastonline_cell.classList.toggle("color-warning");
                                    lastonline_cell.textContent = "Now";
                                }
                            } else {
                                console.log(`[DEBUG] | ${steamtrades.module_name} : No last online data found.`);
                            }

                            if (positive_rep && negative_rep) {
                                let postrust_count = parseInt(positive_rep.innerText);
                                let negtrust_count = parseInt(negative_rep.innerText);

                                if (!isNaN(postrust_count) && !isNaN(negtrust_count)) {
                                    updateRepCountSummary(steamtrades, postrust_count, negtrust_count);
                                    let pos_percent = 0, neg_percent = 0;

                                    if (postrust_count > 0 || negtrust_count > 0) {
                                        pos_percent = postrust_count / (postrust_count + negtrust_count) * 100;
                                        neg_percent = negtrust_count / (postrust_count + negtrust_count) * 100;
                                    }

                                    reputation_cell.classList.toggle("color-warning");
                                    reputation_cell.textContent = `+${postrust_count} (${pos_percent.toFixed(2)}%) / -${negtrust_count} (${neg_percent.toFixed(2)}%)`;

                                    if (pos_percent > 0 && neg_percent == 0) {
                                        reputation_cell.classList.add('detail-positive');
                                    } else if (pos_percent > 0 || neg_percent > 0) {
                                        if (pos_percent >= neg_percent) {
                                            reputation_cell.classList.add('detail-mixed');
                                        } else {
                                            reputation_cell.classList.add('detail-negative');
                                        }
                                    }
                                }
                            } else {
                                console.log(`[DEBUG] | ${steamtrades.module_name} : Reputation data not found.`);
                            }

                            table.appendChild(table_body);
                            document.getElementById(`${steamtrades.id_prefix}-details`).appendChild(table);

                            updateRepContainer(steamtrades, "Normal");
                            updateSummaryStatus(steamtrades, "Normal");
                        } else {
                            updateRepContainer(steamtrades, "No account");
                            updateSummaryStatus(steamtrades, "No account");
                        }
                    } else if (res.finalUrl === "https://www.steamtrades.com/" || res.finalUrl === "https://www.steamtrades.com") {
                        updateRepContainer(steamtrades, "No account");
                        updateSummaryStatus(steamtrades, "No account");
                        console.log(`[DEBUG] | ${steamtrades.module_name} : Redirect detected, user likely has no account.`);
                    } else {
                        updateRepContainer(steamtrades, "Missing data");
                        updateSummaryStatus(steamtrades, "Missing data");
                    }
                } else {
                    updateRepContainer(steamtrades, res.status);
                    updateSummaryStatus(steamtrades, res.status);
                }

                content.innerHTML += links_html;
            }
        },
        ontimeout: function() {
            updateRepContainer(steamtrades, "Timed out");
            content.innerHTML += links_html;
            updateSummaryStatus(steamtrades, "Timed out");
        },
        onerror: function() {
            updateRepContainer(steamtrades, "Application error");
            content.innerHTML += links_html;
            updateSummaryStatus(steamtrades, "Application error");
        }
    });
}

function getSteamHistoryInfo(p_id64) {
    let content = document.getElementById(`${steamhistory.id_prefix}-content`);
    let info = document.getElementById(`${steamhistory.id_prefix}-info-container`);
    content.innerHTML += `<br><a class="module-research-link" href="${steamhistory.profile_url}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${STEAMHISTORY_ICON});"></i>Profile</a>`;

    function getHistoryTimestamp(p_datestring) {
        const date = new Date(p_datestring + " EST");
        return Math.floor(date.getTime() / 1000);
    }

    function buildHistoryTable(p_datatype, p_data) {
        let data, table, table_head, table_body, table_container, detail_text;

        if (p_datatype === "usernames") {
            detail_text = "Username";
        } else if (p_datatype === "names") {
            detail_text = "Real name";
        } else if (p_datatype === "urls") {
            detail_text = "URL";
        }

        table_container = document.createElement("div");
        table_container.id = `steamhistory-${p_datatype}`;
        table_container.className = "history-table-container";
        info.appendChild(table_container);

        table = document.createElement("table");
        table.id = `steamhistory-${p_datatype}-table`;
        table.className = "history-table";

        table_head = document.createElement("thead");
        table_head.innerHTML =
            `
        <tr>
            <th colspan="2" class="history-table-head" id="steamhistory-${p_datatype}-table-head">${detail_text} history</th>
        </tr>
        <tr style='background:#001c8a;position:sticky;top:0;'><td>${detail_text}</td><td>Date seen</td></tr>
        `;

        table_body = document.createElement("tbody");
        table_body.id = "steamhistory-usernames-table-body";
        table.appendChild(table_head);
        table.appendChild(table_body);

        document.getElementById(`steamhistory-${p_datatype}`).appendChild(table);

        data = p_data;

        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                try {
                    let table_row = document.createElement("tr");
                    let date_cell = document.createElement("td");
                    let detail_cell = document.createElement("td");
                    const detail_data = data[i].children[0].innerText;

                    const date_data = data[i].children[1].innerText.replace('[Estimated]', '');
                    const timestamp = getHistoryTimestamp(date_data);
                    if (date_data.startsWith("1969") || isNaN(timestamp)) {
                        date_cell.innerHTML = '<span class="color-warning">N/A</span>';
                    } else {
                        date_cell.textContent = convertTimestampPrecise(timestamp);
                    }

                    if (!detail_data.includes("[SH data hidden by request]")) {
                        if (p_datatype === "urls") {
                            detail_cell.textContent = detail_data.split('/')[4];
                        } else {
                            detail_cell.textContent = detail_data;
                            detail_cell.innerHTML = detail_cell.innerHTML.replace('[email&nbsp;protected]', '<span class="color-warning">[email&nbsp;protected]</span>');
                        }
                    } else {
                        detail_cell.textContent = "Opted out";
                        detail_cell.classList.add("optedout-cell");
                    }

                    table_row.appendChild(detail_cell);
                    table_row.appendChild(date_cell);
                    table_body.appendChild(table_row);
                } catch (e) {
                    console.log(`%c[DEBUG] | SteamHistory.net | ${detail_text} history parsing failed at row index ${i} : ${e}`, "color: red; font-size: 20px;");
                }
            }
        } else {
            let table_row = document.createElement("tr");
            let error_cell = document.createElement("td");
            if (p_datatype === "usernames") {
                error_cell.textContent = `No username history found.`;
            } else if (p_datatype === "names") {
                error_cell.textContent = `No real name history found.`;
            } else if (p_datatype === "urls") {
                error_cell.textContent = `No URL history found.`;
            }
            error_cell.className = "color-warning";
            error_cell.colSpan = 2;
            table_row.appendChild(error_cell);
            table_body.appendChild(table_row);
        }
    }

    updateRepContainer(steamhistory, "Connecting");
    updateSummaryStatus(steamhistory, "Connecting");

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://steamhistory.net/id/${p_id64}`,
        timeout: steamhistory.api_timeout, // Time to wait (in milliseconds) for a response
        cookiePartition: { topLevelSite: 'https://steamhistory.net' },
        onreadystatechange: function(res) {
            if (res.readyState === 2) {
                updateRepContainer(steamhistory, "Loading");
                updateSummaryStatus(steamhistory, "Loading");
            } else if (res.readyState === 4) {
                if (res.status === 200) {
                    let dp = new DOMParser();
                    let html_response = dp.parseFromString(res.responseText,"text/html");
                    let tables = html_response.getElementsByClassName("table-sm");

                    if (tables.length === 6) {
                        updateRepContainer(steamhistory, "Normal");
                        updateSummaryStatus(steamhistory, "Normal");

                        buildHistoryTable("usernames", tables[0].children[1].children);
                        buildHistoryTable("names", tables[1].children[1].children);
                        buildHistoryTable("urls", tables[2].children[1].children);
                    } else {
                        updateRepContainer(steamhistory, "Not indexed");
                        updateSummaryStatus(steamhistory, "Not indexed");
                    }
                } else {
                    updateRepContainer(steamhistory, res.status);
                    updateSummaryStatus(steamhistory, res.status);
                }
            }
        },
        ontimeout: function() {
            updateRepContainer(steamhistory, "Timed out");
            updateSummaryStatus(steamhistory, "Timed out");
        },
        onerror: function() {
            updateRepContainer(steamhistory, "Application error");
            updateSummaryStatus(steamhistory, "Application error");
        }
    });
}

function convertTimestamp(p_timestamp, p_fullmonths = false)
{
    let months, date;
    if (p_fullmonths) {
        date = new Date(p_timestamp);
        months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    } else {
        date = new Date(p_timestamp * 1000);
        months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    }
    let year = date.getFullYear();
    let month = months[date.getMonth()];
    let day = date.getDate();

    if (p_fullmonths) {
        return `${month} ${day}, ${year}`;
    } else {
        return `${day} ${month} ${year}`;
    }
}

function convertTimestampPrecise(p_timestamp, p_fullmonths = false, p_isunixtimestamp = true)
{
    let months, date;
    if (p_fullmonths) {
        months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    } else {
        months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    }

    if (p_isunixtimestamp) {
        date = new Date(p_timestamp * 1000);
    } else {
        date = new Date(p_timestamp);
    }

    if (p_fullmonths) {
        return `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()} @ ${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
    } else {
        return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()} ${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
    }
}

function buildLinkButton(p_siteurl, p_extraparams = ["", ""], p_idtype, p_isarchive = false) {
    let link = document.createElement("a");
    let ids = {"ID2": profile.id2, "ID3": profile.id3, "ID64": profile.id64};

    link.className = "research-link";

    if (p_idtype === "URL") {
        link.href = p_siteurl + `${p_isarchive ? "https://steamcommunity.com/id/" : ""}` + p_extraparams[0] + profile.custom_url + p_extraparams[1];
    } else {
        link.href = p_siteurl + `${p_isarchive ? "https://steamcommunity.com/profiles/" : ""}` + p_extraparams[0] + ids[p_idtype] + p_extraparams[1];
    }

    link.textContent = p_idtype;
    link.target = "_blank";
    link.rel = "noreferrer";

    return link;
}

function buildArchiveLink(p_siteicon = "", p_sitename = "No site name set", p_siteurl = "https://example.com/", p_extraparams = ["",""])
{
    let site_container = document.createElement("div");
    site_container.className = "research-site-container";

    if (p_siteicon != "") {
        let icon = document.createElement("i");
        icon.className = "research-icon";
        icon.style.backgroundImage = `url(${p_siteicon})`;
        site_container.appendChild(icon);
    }

    let name = document.createElement("span");
    name.className = "research-site-label";
    name.textContent = p_sitename;
    site_container.appendChild(name);

    let link_container = document.createElement("div");
    link_container.className = "research-link-container";
    site_container.appendChild(link_container);

    link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "ID3", true));
    link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "ID64", true));
    if (profile.custom_url != "") {
        link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "URL", true));
    }

    return site_container;
}

function buildResearchLink(p_siteicon = "", // Data URI or Icon URL used (if applicable)
                            p_sitename = "No site name set",
                            p_siteurl = "https://example.com/", // Base site URL, without parameters
                            p_acceptsid2 = false,
                            p_acceptsid3 = false,
                            p_acceptsid64 = false,
                            p_acceptscustomurl = false,
                            p_extraparams = ["",""]) // Extra parameters/characters to add at the beginning and/or end of the Steam ID/URL
{

    let site_container = document.createElement("div");
    let accepted_formats = p_acceptsid2 + p_acceptsid3 + p_acceptsid64 + p_acceptscustomurl;

    if (accepted_formats === 1) {
        site_container = document.createElement("a");
        site_container.classList.add("simple-site");
    }

    site_container.classList.add("research-site-container");

    if (p_siteicon != "") {
        let icon = document.createElement("i");
        icon.className = "research-icon";
        icon.style.backgroundImage = `url(${p_siteicon})`;
        site_container.appendChild(icon);
    }

    let name = document.createElement("span");
    name.className = "research-site-label";
    name.textContent = p_sitename;
    site_container.appendChild(name);

    if (accepted_formats > 1) {
        let link_container = document.createElement("div");
        link_container.className = "research-link-container";
        site_container.appendChild(link_container);

        if (p_acceptsid2) {
            link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "ID2"));
        }

        if (p_acceptsid3) {
            link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "ID3"));
        }

        if (p_acceptsid64) {
            link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "ID64"));
        }

        if (p_acceptscustomurl && profile.custom_url != ""){
            link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "URL"));
        }
    } else {
        if (p_acceptsid2) {
            site_container.href = p_siteurl + p_extraparams[0] + profile.id2 + p_extraparams[1];
            site_container.target = "_blank";
            site_container.rel = "noreferrer";
        } else if (p_acceptsid3) {
            site_container.href = p_siteurl + p_extraparams[0] + profile.id3 + p_extraparams[1];
            site_container.target = "_blank";
            site_container.rel = "noreferrer";
        } else if (p_acceptsid64) {
            site_container.href = p_siteurl + p_extraparams[0] + profile.id64 + p_extraparams[1];
            site_container.target = "_blank";
            site_container.rel = "noreferrer";
        } else if (p_acceptscustomurl && profile.custom_url != "") {
            site_container.href = p_siteurl + p_extraparams[0] + profile.custom_url + p_extraparams[1];
            site_container.target = "_blank";
            site_container.rel = "noreferrer";
        }
    }

    return site_container;
}

function buildSummary(p_module) {
    console.log(`%c[DEBUG] | Calling buildSummary() for ${p_module.module_name} | Summary%c${p_module.summary_enabled ? "enabled" : "DISABLED"}`,
                log_styles,
                `${log_styles}${p_module.summary_enabled ? "color:limegreen;" : "color:red;font-weight:bold;"}`);

    if (p_module.summary_enabled) {
        let summary_container = document.getElementById("summary-container");
        let summary = document.createElement("div");
        let summary_title = document.createElement("a");
        let summary_status = document.createElement("span");

        summary_title.textContent = p_module.summary_name ? p_module.summary_name : p_module.module_name;
        summary_title.id = `${p_module.id_prefix}-summary-title`;
        summary_title.className = "summary-title";
        summary_title.href = p_module.profile_url;
        summary_title.target = "_blank";
        summary_title.rel = "noreferrer";

        summary_status.textContent = "Unknown";
        summary_status.id = `${p_module.id_prefix}-summary-status`;
        summary_status.className = "summary-status summary-status-light";

        summary.appendChild(summary_title);
        summary.appendChild(summary_status);

        summary.id = `${p_module.id_prefix}-summary`;
        summary.className = "site-summary";
        summary_container.appendChild(summary);
    }
}

function updateSummaryStatus(p_module, p_status) {
    if (p_module.is_enabled && p_module.summary_enabled) {
        let summary_status = document.getElementById(`${p_module.id_prefix}-summary-status`);

        if (statuses.light.includes(p_status)) {
            summary_status.className = "summary-status summary-status-light";
        } else if (p_status === statuses.normal) {
            summary_status.className = "summary-status summary-status-normal";
        } else if (p_status === statuses.no_account) {
            document.getElementById(`${p_module.id_prefix}-summary-title`).onclick = function() {alert("User has no account on the website.");};
            summary_status.className = "summary-status summary-status-noaccount";
        } else if (statuses.danger.includes(p_status)) {
            summary_status.className = "summary-status summary-status-danger";
        } else if (p_status === statuses.special) {
            summary_status.className = "summary-status summary-status-special";
        } else if (statuses.is_httpstatus(p_status) || statuses.warning.includes(p_status)) {
            summary_status.className = "summary-status summary-status-warning";
        } else {
            alert(`Invalid summary status specified for ${p_module.module_name} , status: ${p_status}. Report the issue if you see this message.`);
        }

        if (statuses.is_httpstatus(p_status)) {
            if (p_status >= 400 && p_status <= 599) {
                summary_status.textContent = `Error ${p_status}`;
            } else {
                summary_status.textContent = `HTTP status ${p_status}`;
            }
        } else {
            summary_status.textContent = p_status;
        }
    }
}

function buildRepCountSummary(p_module, p_reptype = "Reputation") {
    if (p_module.summary_enabled) {
        let summary_container = document.getElementById("summary-container");

        let rep_summary = document.createElement("table");
        rep_summary.id = `${p_module.id_prefix}-rep-summary`;
        rep_summary.className = "rep-summary hidden";

        let rep_summary_title = document.createElement("td");
        rep_summary_title.id = `${p_module.id_prefix}-rep-summary-title`;
        rep_summary_title.className = "rep-summary-title";
        rep_summary_title.textContent = p_reptype;

        let pos_trust = document.createElement("td");
        pos_trust.textContent = "+ ?";
        pos_trust.id = `${p_module.id_prefix}-positive-trust-count`;
        pos_trust.className = "rep-summary-positive";
        let neg_trust = document.createElement("td");
        neg_trust.textContent = "- ?";
        neg_trust.id = `${p_module.id_prefix}-negative-trust-count`;
        neg_trust.className = "rep-summary-negative";

        rep_summary.appendChild(rep_summary_title);
        rep_summary.appendChild(pos_trust);
        rep_summary.appendChild(neg_trust);

        summary_container.appendChild(rep_summary);
    }
}

function updateRepCountSummary(p_module, p_poscount, p_negcount) {
    if (p_module.summary_enabled) {
        let rep_summary = document.getElementById(`${p_module.id_prefix}-rep-summary`);
        let pos_trust = document.getElementById(`${p_module.id_prefix}-positive-trust-count`);
        let neg_trust = document.getElementById(`${p_module.id_prefix}-negative-trust-count`);

        rep_summary.classList.remove("hidden");
        pos_trust.textContent = `+ ${p_poscount}`;
        neg_trust.textContent = `- ${p_negcount}`;
    }
}

function buildSummaryContainer() {
    console.log(`%c[DEBUG] | Calling buildSummaryContainer()`, log_styles);

    // Determines where to insert the HTML code
    let summary_area = document.getElementsByClassName("profile_rightcol")[0];

    let account_infobox = document.createElement("div");
    account_infobox.id = "account-info";
    account_infobox.innerHTML = `<p id="account-loading-label">Loading script...</p>
                                 <p id="account-loading-description">If you see this for more than a minute, please report the issue.</p>`;
    summary_area.insertBefore(account_infobox, summary_area.childNodes[0]);

    let summary_container = document.createElement("div");
    summary_container.id = "summary-container";
    account_infobox.parentNode.insertBefore(summary_container, account_infobox.nextSibling); // Inserts after account info

    if (steamrep.is_enabled) {
        buildSummary(steamrep);
    }

    if (bptf.is_enabled) {
        buildSummary(bptf);
        buildRepCountSummary(bptf, "Trust");
    }

    if (mannco.is_enabled) {
        buildSummary(mannco);
    }

    if (reptf.is_enabled) {
        if (marketplace.is_enabled) {
            buildSummary(marketplace);
        }
        if (scrap.is_enabled) {
            buildSummary(scrap);
        }
    }

    if (firepowered.is_enabled) {
        buildSummary(firepowered);
    }

    if (skial.is_enabled) {
        buildSummary(skial);
    }

    if (steamhistory.is_enabled) {
        buildSummary(steamhistory);
    }

    if (steamiduk.is_enabled) {
        buildSummary(steamiduk);
    }

    if (steamtrades.is_enabled) {
        buildSummary(steamtrades);
        buildRepCountSummary(steamtrades, "Reputation");
    }
}

function buildDetailsMenu() {
    console.log(`%c[DEBUG] | Calling buildDetailsMenu()`, log_styles);

    // Determines where to insert the HTML code
    let leftprofile_area = document.getElementsByClassName("profile_leftcol")[0];
    let left_infobox = document.createElement("div");

    // Buttons for details toggle and settings
    var btn_container = document.createElement("div");
    btn_container.className = "btn-container";

    let toggle_btn = document.createElement("button");
    let settings_btn = document.createElement("button");

    toggle_btn.id = "toggle-details-btn";
    if (!steamdetective.details_onstartup) {
        toggle_btn.textContent = "View links and details";
    } else {
        toggle_btn.textContent = "Hide links and details";
    }
    toggle_btn.addEventListener("click", toggleDetails);

    settings_btn.id = "settings-btn";
    settings_btn.textContent = "Settings";

    btn_container.appendChild(toggle_btn);
    btn_container.appendChild(settings_btn);

    // Details menu
    left_infobox.id = "reputation-menu";
    if (!steamdetective.details_onstartup) {
        left_infobox.className = "hidden"; // Hide on page load
    }
    left_infobox.innerHTML = "<h1 class='reputation-menu-title'>Status</h1>";

    leftprofile_area.insertBefore(left_infobox, leftprofile_area.childNodes[0]);
    leftprofile_area.insertBefore(btn_container, leftprofile_area.childNodes[0]);

    // Trading status boxes
    if (steamrep.is_enabled) {
        buildRepContainer(steamrep);
    }

    if (bptf.is_enabled) {
        buildRepContainer(bptf);
    }

    if (mannco.is_enabled) {
        buildRepContainer(mannco);
    }

    if (reptf.is_enabled) {
        if (marketplace.is_enabled) {
            buildRepContainer(marketplace);
        }
        if (scrap.is_enabled) {
            buildRepContainer(scrap);
        }
    }

    if (firepowered.is_enabled) {
        buildRepContainer(firepowered);
    }

    if (skial.is_enabled) {
        buildRepContainer(skial);
    }

    if (steamhistory.is_enabled) {
        buildRepContainer(steamhistory);
    }

    if (steamiduk.is_enabled) {
        buildRepContainer(steamiduk);
    }

    if (steamtrades.is_enabled) {
        buildRepContainer(steamtrades);
    }
}

function updateTableStatus(p_module, p_status, p_tablename, p_tableid = "", p_colspan = 3) {
    let table_head, table_body;
    if (p_tableid) {
        table_head = document.getElementById(`${p_module.id_prefix}-${p_tableid}-table-head`);
        table_body = document.getElementById(`${p_module.id_prefix}-${p_tableid}-table-body`);
    } else {
        table_head = document.getElementById(`${p_module.id_prefix}-table-head`);
        table_body = document.getElementById(`${p_module.id_prefix}-table-body`);
    }
    let title_text, statuscell_text;
    let status_descriptions = {
        400: "Error 400, request was malformed/incorrect. Report this issue if it persists.",
        401: "Error 401, request was not authorized. Report this issue if it persists.",
        403: `Error 403${p_module.requires_apikey ? ", your API key was most likely entered incorrectly" : ". Try going on the website and try again after. Report the issue if it persists"}.`,
        404: "Error 404. This should not happen in normal circumstances. Report this issue if it persists.",
        405: "Error 405. This should not happen in normal circumstances. Report this issue if it persists.",
        408: `The request sent to ${p_module.module_name} took longer than ${p_module.module_name} was prepared to wait.`,
        414: "Error 414. This should not happen in normal circumstances. Report this issue if it persists",
        429: `Error 429, you might be checking profiles too quickly.`
    };

    if (!isNaN(p_status - parseFloat(p_status))) { // Check if status is a number (for HTTP status codes)
        // Table header text
        if (p_status in http_statusnames) {
            title_text = `${p_tablename} - ${http_statusnames[p_status]}`;
        } else {
            if (p_status >= 400 && p_status <= 499) {
                title_text = `${p_tablename} - Client error (${p_status})`;
            } else if (p_status >= 500 && p_status <= 599) {
                title_text = `${p_tablename} - Server error (${p_status})`;
            } else {
                title_text = `${p_tablename} - Unexpected status code (${p_status})`;
            }
        }

        // Status cell text
        if (p_status in status_descriptions) {
            statuscell_text = status_descriptions[p_status];
        } else {
            if (p_status >= 400 && p_status <= 499) {
                statuscell_text = `A client error (${p_status}) has occured when making a request to ${p_module.module_name}. Report this issue if it persists.`;
            } else if (p_status >= 500 && p_status <= 599) {
                statuscell_text = `Error ${p_status}, ${p_module.module_name} is currently having server issues.`;
            } else {
                statuscell_text = `Unexpected HTTP status code (${p_status}), report this issue if it persists.`;
            }
        }
    } else if (p_status === "Timed out") {
        title_text = `${p_tablename} - Timed out`;
        statuscell_text = `${p_module.module_name} did not respond quickly enough.`;
    } else if (p_status === "Application error") {
        title_text = `${p_tablename} - Application error`;
        statuscell_text = `An application error occured when attempting to connect to ${p_module.module_name}.`;
    }

    table_head.innerHTML = `
                           <tr>
                               <th colspan="${p_colspan}" class="history-table-head" id="${p_module.id_prefix}-table-head">${title_text}</th>
                           </tr>
                           `;
    let table_row = document.createElement("tr");
    let table_cell = document.createElement("td");
    table_cell.textContent = statuscell_text;
    table_cell.className = "error-cell";
    table_cell.colSpan = p_colspan;
    table_row.appendChild(table_cell);
    table_body.appendChild(table_row);
}

function buildRepContainer(p_module) {
    console.log(`%c[DEBUG] | Calling buildRepContainer() for%c${p_module.module_name}`, log_styles, `${log_styles}color:#87CEFA;`);

    // Contains the title and details of a module
    let container = document.createElement("div");
    container.className = "module-info-container status-light";
    container.id = `${p_module.id_prefix}-info-container`;

    let title_container = document.createElement("div");
    title_container.className = "status-title-container";
    title_container.id = `${p_module.id_prefix}-title-container`;
    title_container.innerHTML = `<h2 id='${p_module.id_prefix}-title' class='status-title title-light'>${p_module.module_name} - Unknown</h2>`;
    container.appendChild(title_container);

    // Description and data containers
    let content = document.createElement("div");
    content.className = "status-info-container";
    content.id = `${p_module.id_prefix}-content`;
    let rep_description = document.createElement("p");
    rep_description.id = `${p_module.id_prefix}-reputation-description`;
    rep_description.className = "reputation-description";
    rep_description.innerHTML = `Unknown status - This should normally appear for less than one or two seconds.<br>Report this issue if it stays indefinitely.`;
    content.appendChild(rep_description);
    container.appendChild(content);

    // Add to "View links and details" menu
    document.getElementById("reputation-menu").appendChild(container);
}

function updateRepContainer(p_module, p_status) {
    if (p_module.is_enabled) {
        let container = document.getElementById(`${p_module.id_prefix}-info-container`);
        let title = document.getElementById(`${p_module.id_prefix}-title`);
        let content = document.getElementById(`${p_module.id_prefix}-content`);
        let rep_description = document.getElementById(`${p_module.id_prefix}-reputation-description`);

        if (statuses.light.includes(p_status)) {
            container.className = `module-info-container status-light`;
            title.className = `status-title title-light`;
            title.textContent = `${p_module.module_name} - ${p_status}`;

            if (p_status === "Connecting") {
                rep_description.textContent = `Attempting to connect to ${p_module.module_name}, this can occasionally take a while.`;
            } else if (p_status === "Loading") {
                rep_description.textContent = `Loading information, this can occasionally take a while.`;
            } else if (p_status === "API Key Required") {
                if (p_module.module_name === steamiduk.module_name) {
                    rep_description.innerHTML = `You will need to set the ID64 and API key in the settings menu to view SteamID.uk information without going on the website.<br>
                                             Your API key can be requested here:
                                             <a style="font-weight:bold;" href="https://steamid.uk/steamidapi/" target="_blank" rel="noreferrer">https://steamid.uk/steamidapi/</a><br>
                                             Do not share your API key with anyone, as it can be used to perform malicious actions under your identity.`;
                }
            } else {
                alert("Programming error: updateRepContainer() -> Light status exists but has no description defined!");
            }
        } else if (p_status === statuses.no_account) {
            container.className = `module-info-container status-noaccount`;
            title.className = `status-title title-noaccount`;
            title.textContent = `${p_module.module_name} - ${p_status}`;

            if (p_module.module_name != bptf.module_name) {
                rep_description.textContent = `User has no account or data found on the website.`;
            } else {
                rep_description.innerHTML = "";
            }
        } else if (p_status === statuses.normal) {
            container.className = `module-info-container status-normal`;
            title.className = `status-title title-normal`;
            title.textContent = `${p_module.module_name} - ${p_status}`;

            if (p_module.module_name === skial.module_name) {
                rep_description.textContent = `The user has played on Skial servers before.`;
            } else if (p_module.module_name === bptf.module_name) {
                rep_description.innerHTML = "";
            } else if (p_module.module_name === scrap.module_name) {
                rep_description.innerHTML = `<span id="${p_module.id_prefix}-membership"></span>`;
            } else {
                rep_description.textContent = `No special reputation.`;
            }
        } else if (statuses.danger.includes(p_status)) {
            container.className = `module-info-container status-danger`;
            title.className = `status-title title-danger`;
            title.textContent = `${p_module.module_name} - ${p_status}`;

            if (p_status === "Banned") {
                if (p_module.module_name === mannco.module_name) {
                    rep_description.innerHTML = `<b>Reason:</b> <span id="${p_module.id_prefix}-banreason"></span>`;
                } else if (p_module.module_name === bptf.module_name || p_module.module_name === marketplace.module_name) {
                    rep_description.innerHTML = "";
                } else if (p_module.module_name === scrap.module_name) {
                    rep_description.innerHTML = `<span id="${p_module.id_prefix}-banreason"></span>`;
                } else if (p_module.module_name === skial.module_name || p_module.module_name === firepowered.module_name) {
                    rep_description.textContent = `The user is currently banned from ${p_module.module_name} servers.`;
                }
            } else if (p_status === "Scammer") {
                // Status exclusive to SteamRep
                rep_description.textContent = "You will get banned if you trade with this person.";
                content.innerHTML += `<br><p id='${p_module.id_prefix}-tags' class='tags-scammer'><b>Tags:</b> </p>`;
            } else {
                alert("Programming error: updateRepContainer() -> Danger status exists but has no description defined!");
            }
        } else if (statuses.warning.includes(p_status)) {
            container.className = `module-info-container status-warning`;
            title.className = `status-title title-warning`;
            title.textContent = `${p_module.module_name} - ${p_status}`;

            if (p_status === "Caution") {
                rep_description.textContent = "The user may have scammed someone and/or had suspicious behavior in the past.";
                content.innerHTML += `<br><p id='${p_module.id_prefix}-tags' class='tags-caution'><b>Tags:</b> </p>`;
            } else if (p_status === "Unbanned") {
                rep_description.textContent = `User was banned from ${p_module.module_name} servers previously but is no longer banned.`;
            } else if (p_status === "Not indexed") {
                rep_description.textContent = "User was not indexed or the data expected was not found. Try again in a minute.";
            } else if (p_status === "Database connectivity issue") {
                rep_description.textContent = `${p_module.module_name} is currently having database issues.`;
            } else if (p_status === "API Key not set") {
                rep_description.innerHTML = `The ID64 appears to be set, but you also need to set the API key of your account in the settings menu.<br>
                                             Your API key can be requested here:
                                             <a style="font-weight:bold;" href="https://steamid.uk/steamidapi/" target="_blank" rel="noreferrer">https://steamid.uk/steamidapi/</a><br>
                                             Do not share your API key with anyone, as it can be used to perform malicious actions under your identity.`;
            } else if (p_status === "ID64 not set") {
                rep_description.textContent = "The API key appears to be set, but you also need to set the ID64 of your account in the settings menu.";
            } else if (p_status === "ID64 not found") {
                rep_description.innerHTML = "The profile you visited couldn't be found.<br>This means that the profile was never checked on SteamID's website before.";
            } else if (p_status === "Invalid ID64/API Key settings") {
                rep_description.innerHTML = "The ID64 and/or the API key in your settings are invalid.<br>Make sure both are correctly typed/pasted.";
            } else if (p_status === "Daily lookup limit reached") {
                rep_description.innerHTML = "You have reached your daily lookup limit.<br>You should be able to lookup more profiles tomorrow.";
            } else if (p_status === "API Key Disabled") {
                rep_description.textContent = "Your API key is disabled, contact a SteamID admin for help.";
            } else if (p_status === "Internal API error") {
                rep_description.textContent = `${p_module.module_name}'s API had an error, contact a SteamID admin if the issue persists.`;
            } else if (p_status === "Unknown error") {
                rep_description.textContent = `${p_module.module_name} gave an error response but did not specify which one.`;
            } else if (p_status === "Missing data") {
                rep_description.textContent = `The data expected for ${p_module.module_name} was not found. Report this issue if it persists.`;
            } else if (p_status === "Unexpected data") {
                rep_description.textContent = `The data parsed in ${p_module.module_name} was not expected. Report this issue if it persists.`;
            } else if (p_status === "Rep.tf get data fail") {
                rep_description.textContent = `Rep.tf failed to get ${p_module.module_name} data for this profile.`;
            } else if (p_status === "Deleted account") {
                rep_description.textContent = `User has deleted their account on ${p_module.module_name}.`;
            } else if (p_status === "Feature Banned") {
                rep_description.innerHTML = "";
            } else if (p_status === "Timed out") {
                if (p_module.module_name != marketplace.module_name && p_module.module_name != scrap.module_name) {
                    rep_description.textContent = `${p_module.module_name} did not respond quickly enough.`;
                } else {
                    rep_description.textContent = "Rep.tf did not respond quickly enough.";
                }
            } else if (p_status === "Application error") {
                rep_description.innerHTML = `An application error occured while connecting to ${p_module.module_name}.<br>
                                             This may be due to a Content Security Policy issue, report this issue if it persists.`;
            } else {
                alert("Programming error: updateRepContainer() -> Warning status exists but has no description defined!");
            }
        } else if (p_status === statuses.special) {
            // Status exclusive to SteamRep
            container.className = `module-info-container status-special`;
            title.className = `status-title title-special`;
            title.textContent = `${p_module.module_name} - ${p_status}`;
            rep_description.innerHTML = "The user is a trusted member of the community.<br>Be careful, as impersonators may post links to this profile to decieve you.";
            content.innerHTML += `<br><p id='${p_module.id_prefix}-tags' class='tags-trusted'><b>Tags:</b> </p>`;
        } else if (statuses.is_httpstatus(p_status)) {
            container.className = `module-info-container status-warning`;
            title.className = `status-title title-warning`;

            let status_descriptions = {
                400: "Error 400, request was malformed/incorrect. Report this issue if it persists.",
                401: "Error 401, request was not authorized. Report this issue if it persists.",
                403: `Error 403, you might be blocked / ratelimited${p_module.requires_apikey ? " or your API key wasn't entered correctly" : ". Try going on the website or try again later"}.`,
                404: "Error 404. This should not happen in normal circumstances. Report this issue if it persists.",
                405: "Error 405. This should not happen in normal circumstances. Report this issue if it persists.",
                408: `The request sent to ${p_module.module_name} took longer than ${p_module.module_name} was prepared to wait.`,
                414: "This should not happen in normal circumstances. Report this issue if it persists",
                429: `Error 429, you might be checking profiles too quickly.`
            };

            // Title
            if (p_status in http_statusnames) {
                title.textContent = `${p_module.module_name} - ${http_statusnames[p_status]} (${p_status})`;
            } else {
                if (p_status >= 400 && p_status <= 499) {
                    title.textContent = `${p_module.module_name} - Client Error (${p_status})`;
                } else if (p_status >= 500 && p_status <= 599) {
                    title.textContent = `${p_module.module_name} - Server Error (${p_status})`;
                } else {
                    title.textContent = `${p_module.module_name} - Unexpected HTTP status code (${p_status})`;
                }
            }

            // Description
            if (p_status in status_descriptions) {
                rep_description.textContent = status_descriptions[p_status];
            } else {
                if (p_status >= 400 && p_status <= 499) {
                    rep_description.textContent = `A client error has occured when making a request to ${p_module.module_name}. Report this issue if it persists.`;
                } else if (p_status >= 500 && p_status <= 599) {
                    rep_description.textContent = `Error ${p_status}, ${p_module.module_name} is currently having server issues.`;
                } else {
                    rep_description.textContent = "Unexpected HTTP status code, report this issue if it persists.";
                }
            }
        } else {
            alert(`Programming error: updateRepContainer() -> Status ${p_status} does not exist!`);
            container.className = `module-info-container status-warning`;
            title.className = `status-title title-warning`;
            title.textContent = `${p_module.module_name} - Invalid Status`;
            rep_description.textContent = `The status indicated in the code '${p_status}' does not exist. Report the issue if you see this.`;
        }
    }
}

function switchSettingSection(p_section) {
    document.getElementById("general-settings").classList.add("hidden");
    document.getElementById("module-settings").classList.add("hidden");
    document.getElementById("link-settings").classList.add("hidden");
    document.getElementById("debug-settings").classList.add("hidden");
    document.getElementById("general-section-button").classList.remove("section-selected");
    document.getElementById("module-section-button").classList.remove("section-selected");
    document.getElementById("link-section-button").classList.remove("section-selected");
    document.getElementById("debug-section-button").classList.remove("section-selected");

    if (p_section === "General") {
        document.getElementById("general-settings").classList.remove("hidden");
        document.getElementById("general-section-button").classList.add("section-selected");
        document.getElementById("setting-section-title").textContent = "General settings";
    } else if (p_section === "Modules") {
        document.getElementById("module-settings").classList.remove("hidden");
        document.getElementById("module-section-button").classList.add("section-selected");
        document.getElementById("setting-section-title").textContent = "Module settings";
    } else if (p_section === "Links") {
        document.getElementById("link-settings").classList.remove("hidden");
        document.getElementById("link-section-button").classList.add("section-selected");
        document.getElementById("setting-section-title").textContent = "Link settings";
    } else if (p_section === "Debug") {
        document.getElementById("debug-settings").classList.remove("hidden");
        document.getElementById("debug-section-button").classList.add("section-selected");
        document.getElementById("setting-section-title").textContent = "Debug settings";
    } else {
        alert("Invalid section specified. Report the issue if you see this.");
    }
}

function buildSettingsModal() {
    console.log(`%c[DEBUG] | Calling buildSettingsModal()`, log_styles);

    let responsive_header = document.getElementsByClassName("responsive_header")[0];

    // Defines where the settings button is
    let settings_btn = document.getElementById("settings-btn");

    // Settings modal
    let settings_modal = document.createElement("div");
    settings_modal.id = "settings-modal";
    settings_modal.className = "modal";
    settings_modal.innerHTML =
    `
    <div class="modal-content">
        <div class="settings-sidebar">
            <a id="general-section-button" class="section-selected">General</a>
            <a id="module-section-button">Modules</a>
            <a id="link-section-button">Links</a>
            <a id="debug-section-button">Debug</a>
            <p>v${GM_info.script.version}</p>
            <p>2025-03-01</p>
        </div>
        <span class="close">&times;</span>
        <div id="setting-section-title-container">
            <h1 id="setting-section-title">General settings</h1>
        </div>
        <div class="setting-element-container" id="general-settings">
            <form>
                <fieldset id="element-settings-container">
                    <h2 class="setting-title">Elements</h2>
                    <div>
                        <label for="agedisclaimer-enabled-input">Account age disclaimer:</label>
                        <input name="agedisclaimer-enabled-input" id="agedisclaimer-enabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamdetective.agedisclaimer_enabled ? "checked" : "" }>
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="behavior-settings-container">
                    <h2 class="setting-title">Behavior</h2>
                    <div>
                        <label for="details-onstartup-input">View links and details on startup:</label>
                        <input name="details-onstartup-input" id="details-onstartup-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamdetective.details_onstartup ? "checked" : "" }>
                    </div>
                </fieldset>
                <hr class="settings-splitter">
            </form>
        </div>
        <div class="setting-element-container hidden" id="module-settings">
            <form>
                <fieldset id="steamrep-settings-container">
                    <div>
                        <h2 class="setting-title">SteamRep</h2>
                        <input name="steamrep-enabled-input" id="steamrep-enabled-input" class="setting-toggle" type="checkbox" ${steamrep.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="steamrep-historyenabled-input">History:</label>
                        <input name="steamrep-historyenabled-input" id="steamrep-historyenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamrep.history_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="steamrep-eolmsgenabled-input">End of life notice:</label>
                        <input name="steamrep-eolmsgenabled-input" id="steamrep-eolmsgenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamrep.eolmsg_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="steamrep-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="steamrep-summaryenabled-input" id="steamrep-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamrep.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="steamrep-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="steamrep-timeout-input" class="setting-textbox" name="steamrep-timeout-input" value="${steamrep.api_timeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="bptf-settings-container">
                     <div>
                          <h2 class="setting-title">Backpack.tf</h2>
                          <input name="bptf-enabled-input" id="bptf-enabled-input" class="setting-toggle" type="checkbox" ${bptf.is_enabled ? "checked" : "" }>
                     </div>
                     <p style='color:#f7ff00;'>To request/get the API key, go here: <a style="font-weight:bold;" href="https://backpack.tf/developer/apikey/view" target="_blank" rel="noreferrer">https://backpack.tf/developer/apikey/view</a><br>
                     Do not share this key with anyone, as it can be used to perform malicious actions under your identity.</p>
                     <div>
                          <label for="bptf-apikey-input">Backpack.tf API Key:</label>
                          <input type="text" id="bptf-apikey-input" class="setting-textbox" name="bptf-apikey-input" value="${bptf.api_key}">
                     </div>
                     <div>
                          <label for="bptf-timeout-input">Timeout (in milliseconds):</label>
                          <input type="text" id="bptf-timeout-input" class="setting-textbox" name="bptf-timeout-input" value="${bptf.api_timeout}">
                     </div>
                     <div>
                        <label for="bptf-forcelimitedapi-input">Force use of limited API (not recommended):</label>
                        <input name="bptf-forcelimitedapi-input" id="bptf-forcelimitedapi-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${bptf.force_limitedapi ? "checked" : "" }>
                     </div>
                     <div>
                        <label for="bptf-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="bptf-summaryenabled-input" id="bptf-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${bptf.summary_enabled ? "checked" : "" }>
                     </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="mannco-settings-container">
                    <div>
                        <h2 class="setting-title">Mannco.store</h2>
                        <input name="mannco-enabled-input" id="mannco-enabled-input" class="setting-toggle" type="checkbox" ${mannco.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="mannco-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="mannco-summaryenabled-input" id="mannco-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${mannco.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="mannco-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="mannco-timeout-input" class="setting-textbox" name="mannco-timeout-input" value="${mannco.api_timeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="reptf-settings-container">
                    <div>
                        <h2 class="setting-title">Rep.tf (Marketplace.tf / Scrap.tf)</h2>
                        <input name="reptf-enabled-input" id="reptf-enabled-input" class="setting-toggle" type="checkbox" ${reptf.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="reptf-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="reptf-timeout-input" class="setting-textbox" name="reptf-timeout-input" value="${reptf.api_timeout}">
                    </div>
                    ${reptf.is_enabled && (!marketplace.is_enabled && !scrap.is_enabled) ? "<p class='color-warning'><b>WARNING:</b> Marketplace.tf and Scrap.tf are both disabled.<br>This is functionally the same as disabling Rep.tf even if it is enabled.</p>" : ""}
                    <div>
                        <label for="mptf-enabled-input"><b>Marketplace.tf</b> - Show details:</label>
                        <input name="mptf-enabled-input" id="mptf-enabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${marketplace.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="mptf-summaryenabled-input"><b>Marketplace.tf</b> - Show status summary in right-hand side:</label>
                        <input name="mptf-summaryenabled-input" id="mptf-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${marketplace.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="scrap-enabled-input"><b>Scrap.tf</b> - Show details:</label>
                        <input name="scrap-enabled-input" id="scrap-enabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${scrap.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="scrap-summaryenabled-input"><b>Scrap.tf</b> - Show status summary in right-hand side:</label>
                        <input name="scrap-summaryenabled-input" id="scrap-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${scrap.summary_enabled ? "checked" : "" }>
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="sid-settings-container">
                    <div>
                        <h2 class="setting-title">SteamID.uk</h2>
                        <input name="sid-enabled-input" id="sid-enabled-input" class="setting-toggle" type="checkbox" ${steamiduk.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="sid-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="sid-summaryenabled-input" id="sid-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamiduk.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <p style='color:#f7ff00;'>
                            To request/get the API key, go here: <a style="font-weight:bold;" href="https://steamid.uk/steamidapi/" target="_blank" rel="noreferrer">https://steamid.uk/steamidapi/</a>
                            <br> The ID64 to input is the one from the account you requested the API key with. This is not optional.
                        </p>
                        ${typeof g_steamID == 'string' ? " <h5 style='margin-bottom:5px;color:#f7ff00;'>ID64 of currently logged in account: " + g_steamID + "</h5>" : ""}
                    </div>
                    <div>
                        <label for="sid-myid-input">SteamID.uk ID64:</label>
                        <input type="text" id="sid-myid-input" class="setting-textbox" name="sid-myid-input" value="${steamiduk.my_id}">
                    </div>
                    <div>
                        <label for="sid-apikey-input">SteamID.uk API Key:</label>
                        <input type="text" id="sid-apikey-input" class="setting-textbox" name="sid-apikey-input" value="${steamiduk.api_key}">
                    </div>
                    <div>
                        <label for="sid-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="sid-timeout-input" class="setting-textbox" name="sid-timeout-input" value="${steamiduk.api_timeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="fp-settings-container">
                    <div>
                        <h2 class="setting-title">FirePowered</h2>
                        <input name="fp-enabled-input" id="fp-enabled-input" class="setting-toggle" type="checkbox" ${firepowered.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="fp-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="fp-summaryenabled-input" id="fp-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${firepowered.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="fp-hideinactivebans-input">Hide inactive bans:</label>
                        <input name="fp-hideinactivebans-input" id="fp-hideinactivebans-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${firepowered.hide_inactivebans ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="fp-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="fp-timeout-input" class="setting-textbox" name="fp-timeout-input" value="${firepowered.api_timeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="skial-settings-container">
                    <div>
                        <h2 class="setting-title">Skial</h2>
                        <input name="skial-enabled-input" id="skial-enabled-input" class="setting-toggle" type="checkbox" ${skial.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="skial-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="skial-summaryenabled-input" id="skial-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${skial.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="skial-hideinactivebans-input">Hide inactive bans:</label>
                        <input name="skial-hideinactivebans-input" id="skial-hideinactivebans-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${skial.hide_inactivebans ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="skial-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="skial-timeout-input" class="setting-textbox" name="skial-timeout-input" value="${skial.api_timeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="steamhistory-settings-container">
                    <div>
                        <h2 class="setting-title">SteamHistory.net</h2>
                        <input name="steamhistory-enabled-input" id="steamhistory-enabled-input" class="setting-toggle" type="checkbox" ${steamhistory.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="steamhistory-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="steamhistory-summaryenabled-input" id="steamhistory-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamhistory.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="steamhistory-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="steamhistory-timeout-input" class="setting-textbox" name="steamhistory-timeout-input" value="${steamhistory.api_timeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="st-settings-container">
                    <div>
                        <h2 class="setting-title">SteamTrades.com</h2>
                        <input name="st-enabled-input" id="st-enabled-input" class="setting-toggle" type="checkbox" ${steamtrades.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="st-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="st-summaryenabled-input" id="st-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamtrades.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="st-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="st-timeout-input" class="setting-textbox" name="st-timeout-input" value="${steamtrades.api_timeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="vacbanned-settings-container">
                <div>
                    <h2 class="setting-title">VACBanned.com</h2>
                </div>
                <div>
                    <p class="color-warning">
                        On August 4th, 2024, VACBanned.com has announced that they have shutdown and that all data will be deleted from their website.<br>
                        Consequently, this module is no longer available.
                    </p>
                </div>
                </fieldset>
            </form>
        </div>
        <div class="setting-element-container hidden" id="link-settings">
            <p>Nothing here... yet.</p>
        </div>
        <div class="setting-element-container hidden" id="debug-settings">
            <form>
                <fieldset id="debug-settings-container">
                    <p class="color-warning">
                        <b>WARNING:</b> Turning these options on without knowing what they do is not recommended.<br>
                        The script may not behave as intended if you enable them!
                    </p>
                    <h2 class="setting-title">Feature debugging</h2>
                    <div>
                        <label for="forcedfallback-enabled-input">Forced fallback mode:</label>
                        <input name="forcedfallback-enabled-input" id="forcedfallback-enabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamdetective.forcedfallback_enabled ? "checked" : "" }>
                    </div>
                </fieldset>
                <hr class="settings-splitter">
            </form>
        </div>
        <div id="setting-button-container">
            <button id="save-input-btn">Save</button>
            <button id="reset-input-btn">Reset</button>
        </div>
    </div>
    `;
    document.getElementsByClassName("responsive_page_content")[0].prepend(settings_modal);
    let settings_close = document.getElementsByClassName("close")[0];
    settings_btn.onclick = () => openSettingsModal();
    settings_close.onclick = function() {
        settings_modal.style.display = "none";
        responsive_header.style.zIndex = "20";
        document.querySelector("body").classList.remove("scroll-lock");
    };
    let settings_save = document.getElementById("save-input-btn");
    settings_save.addEventListener("click", saveSettings);
    let settings_reset = document.getElementById("reset-input-btn");
    settings_reset.addEventListener("click", resetSettings);

    document.getElementById("general-section-button").onclick = () => switchSettingSection("General");
    document.getElementById("module-section-button").onclick = () => switchSettingSection("Modules");
    document.getElementById("link-section-button").onclick = () => switchSettingSection("Links");
    document.getElementById("debug-section-button").onclick = () => switchSettingSection("Debug");

    window.onclick = function(event) {
        if (event.target === settings_modal) {
            settings_modal.style.display = "none";
            responsive_header.style.zIndex = "20";
            document.querySelector("body").classList.remove("scroll-lock");
        }
    };
}

function setTimeoutSetting(p_id, p_storageid) {
    if (document.getElementById(p_id).value > 0 && document.getElementById(p_id).value <= 300000) {
        GM_setValue(p_storageid, document.getElementById(p_id).value);
    } else {
        GM_setValue(p_storageid, DEFAULT_TIMEOUT); // Set at default value if invalid or too big
    }
}

function saveSettings() {
    if (confirm("Are you sure that you want to save all your current settings?")) {
        // General settings
        GM_setValue("agedisclaimer_enabled", document.getElementById("agedisclaimer-enabled-input").checked);
        GM_setValue("details_onstartup", document.getElementById("details-onstartup-input").checked);

        // Module settings
        GM_setValue("steamrep_enabled", document.getElementById("steamrep-enabled-input").checked);
        GM_setValue("steamrep_historyenabled", document.getElementById("steamrep-historyenabled-input").checked);
        GM_setValue("bptf_enabled", document.getElementById("bptf-enabled-input").checked);
        GM_setValue("bptf_forcelimitedapi", document.getElementById("bptf-forcelimitedapi-input").checked);
        GM_setValue("mannco_enabled", document.getElementById("mannco-enabled-input").checked);
        GM_setValue("reptf_enabled", document.getElementById("reptf-enabled-input").checked);
        GM_setValue("marketplace_enabled", document.getElementById("mptf-enabled-input").checked);
        GM_setValue("scrap_enabled", document.getElementById("scrap-enabled-input").checked);
        GM_setValue("firepowered_enabled", document.getElementById("fp-enabled-input").checked);
        GM_setValue("skial_enabled", document.getElementById("skial-enabled-input").checked);
        GM_setValue("steamhistory_enabled", document.getElementById("steamhistory-enabled-input").checked);
        GM_setValue("sid_enabled", document.getElementById("sid-enabled-input").checked);
        GM_setValue("strades_enabled", document.getElementById("st-enabled-input").checked);

        GM_setValue("steamrep_summaryenabled", document.getElementById("steamrep-summaryenabled-input").checked);
        GM_setValue("bptf_summaryenabled", document.getElementById("bptf-summaryenabled-input").checked);
        GM_setValue("mannco_summaryenabled", document.getElementById("mannco-summaryenabled-input").checked);
        GM_setValue("marketplace_summaryenabled", document.getElementById("mptf-summaryenabled-input").checked);
        GM_setValue("scrap_summaryenabled", document.getElementById("scrap-summaryenabled-input").checked);
        GM_setValue("steamhistory_summaryenabled", document.getElementById("steamhistory-summaryenabled-input").checked);
        GM_setValue("firepowered_summaryenabled", document.getElementById("fp-summaryenabled-input").checked);
        GM_setValue("skial_summaryenabled", document.getElementById("skial-summaryenabled-input").checked);
        GM_setValue("sid_summaryenabled", document.getElementById("sid-summaryenabled-input").checked);
        GM_setValue("strades_summaryenabled", document.getElementById("st-summaryenabled-input").checked);

        GM_setValue("bptf_apikey", document.getElementById("bptf-apikey-input").value.trim());
        GM_setValue("sid_myid", document.getElementById("sid-myid-input").value.trim());
        GM_setValue("sid_apikey", document.getElementById("sid-apikey-input").value.trim());

        GM_setValue("steamrep_eolmsgenabled", document.getElementById("steamrep-eolmsgenabled-input").checked);
        GM_setValue("firepowered_hideinactivebans", document.getElementById("fp-hideinactivebans-input").checked);
        GM_setValue("skial_hideinactivebans", document.getElementById("skial-hideinactivebans-input").checked);

        setTimeoutSetting("steamrep-timeout-input", "steamrep_apitimeout");
        setTimeoutSetting("bptf-timeout-input", "bptf_apitimeout");
        setTimeoutSetting("mannco-timeout-input", "mannco_apitimeout");
        setTimeoutSetting("reptf-timeout-input", "reptf_apitimeout");
        setTimeoutSetting("fp-timeout-input", "firepowered_apitimeout");
        setTimeoutSetting("skial-timeout-input", "skial_apitimeout");
        setTimeoutSetting("steamhistory-timeout-input", "steamhistory_apitimeout");
        setTimeoutSetting("sid-timeout-input", "sid_apitimeout");
        setTimeoutSetting("st-timeout-input", "strades_apitimeout");

        // Debug settings
        GM_setValue("forcedfallback_enabled", document.getElementById("forcedfallback-enabled-input").checked);

        alert("Saved settings.");
    }
}

function resetSettings() {
    if (confirm("Are you sure that you want to reset all your settings to default values? This cannot be undone.")) {
        document.getElementById("agedisclaimer-enabled-input").checked = true;
        document.getElementById("details-onstartup-input").checked = false;

        document.getElementById("steamrep-enabled-input").checked = true;
        document.getElementById("steamrep-summaryenabled-input").checked = true;
        document.getElementById("steamrep-eolmsgenabled-input").checked = true;
        document.getElementById("steamrep-historyenabled-input").checked = true;
        document.getElementById("steamrep-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("bptf-enabled-input").checked = true;
        document.getElementById("bptf-summaryenabled-input").checked = true;
        document.getElementById("bptf-forcelimitedapi-input").checked = false;
        document.getElementById("bptf-apikey-input").value = "";
        document.getElementById("bptf-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("mannco-enabled-input").checked = true;
        document.getElementById("mannco-summaryenabled-input").checked = true;
        document.getElementById("mannco-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("reptf-enabled-input").checked = true;
        document.getElementById("reptf-timeout-input").value = DEFAULT_TIMEOUT;
        document.getElementById("mptf-enabled-input").checked = true;
        document.getElementById("mptf-summaryenabled-input").checked = true;
        document.getElementById("scrap-enabled-input").checked = true;
        document.getElementById("scrap-summaryenabled-input").checked = true;

        document.getElementById("fp-enabled-input").checked = true;
        document.getElementById("fp-summaryenabled-input").checked = true;
        document.getElementById("fp-hideinactivebans-input").checked = false;
        document.getElementById("fp-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("skial-enabled-input").checked = true;
        document.getElementById("skial-summaryenabled-input").checked = true;
        document.getElementById("skial-hideinactivebans-input").checked = false;
        document.getElementById("skial-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("steamhistory-enabled-input").checked = true;
        document.getElementById("steamhistory-summaryenabled-input").checked = false;
        document.getElementById("steamhistory-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("sid-enabled-input").checked = true;
        document.getElementById("sid-summaryenabled-input").checked = false;
        document.getElementById("sid-myid-input").value = "";
        document.getElementById("sid-apikey-input").value = "";
        document.getElementById("sid-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("st-enabled-input").checked = false;
        document.getElementById("st-summaryenabled-input").checked = true;
        document.getElementById("st-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("forcedfallback-enabled-input").checked = false;

        GM_deleteValue("agedisclaimer_enabled");
        GM_deleteValue("details_onstartup");

        GM_deleteValue("steamrep_enabled");
        GM_deleteValue("steamrep_summaryenabled");
        GM_deleteValue("steamrep_eolmsgenabled");
        GM_deleteValue("steamrep_historyenabled");
        GM_deleteValue("steamrep_apitimeout");

        GM_deleteValue("bptf_enabled");
        GM_deleteValue("bptf_summaryenabled");
        GM_deleteValue("bptf_forcelimitedapi");
        GM_deleteValue("bptf_apikey");
        GM_deleteValue("bptf_apitimeout");

        GM_deleteValue("mannco_enabled");
        GM_deleteValue("mannco_summaryenabled");
        GM_deleteValue("mannco_apitimeout");

        GM_deleteValue("reptf_enabled");
        GM_deleteValue("reptf_apitimeout");
        GM_deleteValue("marketplace_enabled");
        GM_deleteValue("marketplace_summaryenabled");
        GM_deleteValue("scrap_enabled");
        GM_deleteValue("scrap_summaryenabled");

        GM_deleteValue("firepowered_enabled");
        GM_deleteValue("firepowered_summaryenabled");
        GM_deleteValue("firepowered_hideinactivebans");
        GM_deleteValue("firepowered_apitimeout");

        GM_deleteValue("skial_enabled");
        GM_deleteValue("skial_summaryenabled");
        GM_deleteValue("skial_hideinactivebans");
        GM_deleteValue("skial_apitimeout");

        GM_deleteValue("steamhistory_enabled");
        GM_deleteValue("steamhistory_summaryenabled");
        GM_deleteValue("steamhistory_apitimeout");

        GM_deleteValue("sid_enabled");
        GM_deleteValue("sid_summaryenabled");
        GM_deleteValue("sid_myid");
        GM_deleteValue("sid_apikey");
        GM_deleteValue("sid_apitimeout");

        GM_deleteValue("strades_enabled");
        GM_deleteValue("strades_summaryenabled");
        GM_deleteValue("strades_apitimeout");

        GM_deleteValue("forcedfallback_enabled");

        // Values that are no longer used
        GM_deleteValue("vacbanned_enabled");
        GM_deleteValue("vacbanned_summaryenabled");
        GM_deleteValue("vacbanned_apitimeout");
        GM_deleteValue("mannco_workaroundenabled");

        alert("Reset settings.");
    }
}

// Class Styles
GM_addStyle
(`
 :root {
     --color-error: red;
     --color-danger: yellow;
     --color-limited: yellow;
     --color-warning: orange;
     --color-fixed: limegreen;
 }

 #acc-age-disclaimer { padding: 10px 5px 10px 5px; background: #ffffff0d; color: white; width: 250px; display: inline-block; margin-bottom: 5px; margin-top: 5px; border-radius: 5px; }
 #account-info { background: rgba(0, 0, 0, 0.4) none repeat scroll 0% 0%; padding: 7px; text-align: center; border-radius: 5px; margin: -10px -10px 10px -10px; color: white; animation-name: popup; animation-duration: 0.3s; }
 #account-data-table { padding: 5px; width: 100%; }
 #account-data-url { font-family: monospace; font-size: 14px; }
 #account-loading-label { font-size: 16px; margin-bottom: -10px; }
 #bptf-sitebans-title {font-size: 16px;}
 #bptf-ban-list {margin-top: 3px;}
 #bptf-ban-list > li:not(:last-child) { margin-bottom: 5px; }
 #bptf-trust-summary {margin-top: -5px;}
 #sr-eol-notice, #bptf-api-warning {background: #000000bd; border-radius: 5px; padding: 15px; color: white; margin-bottom: 7px;}
 #nohistory-cell, .error-cell {color: var(--color-warning);}
 #history-optedout {background: #800; color: white; padding-top: 5px; padding-bottom: 5px;}
 #reputation-menu {background: rgba(0, 0, 0, 0.6) none repeat scroll 0% 0%; border-radius: 5px; margin-bottom: 13px; padding: 20px; animation-name: popup; animation-duration: 0.3s; }
 #save-input-btn, #reset-input-btn {width: 125px;height: 50px;background: black;border: 1px solid white;color: white;font-size: 18px;}
 #save-input-btn:hover, #reset-input-btn:hover {background: #ffffff1a}
 #save-input-btn:active #reset-input-btn:active {background: #ffffff80}
 #sr-history, #fp-bans, #skial-servers, #skial-names, #skial-bans, #st-details {max-height: 350px; overflow-y: auto; margin-top: 10px;}
 #sr-tags {display: inline;}
 .tags-scammer { color: orange; }
 .tags-trusted { color: yellow; }
 .tags-caution { color: #ef7; }

 #toggle-details-btn, #settings-btn {width: 200px; height: 40px; background: rgba(0, 0, 0, 0.6) none repeat scroll 0% 0%; margin: -14px 12px 10px 12px; border-radius: 4px; text-align: center; border: none; color: white; transition: all 0.3s;}
 #toggle-details-btn:hover, #settings-btn:hover {background: #ffffff1a}
 #toggle-details-btn:active, #settings-btn:active {background: #ffffff80}
 #trust-summary-title {width: 110px;display: inline-block;text-align: center;line-height: 25px;background: #ffffff2e;}
 #st-details-table {max-height: 300px;}
 #st-details-table tr td { text-align: left; padding-left: 15px; padding-top: 8px; padding-bottom: 8px; }
 #st-details-table tr td:first-child { font-weight: bold; background: #0005; }

 #community-ban-indicator { background: #c65d1e2b; color: #ff9a41; padding: 10px 0px 10px 0px; display: inline-block; margin-top: 5px; width: 94%; border-radius: 5px; border: 1px solid #ff9a41; }
 #trade-ban-indicator, #vac-ban-indicator { background: #a948472b; color: #ff4646; padding: 10px 0px 10px 0px; display: inline-block; margin-top: 5px; width: 94%; border-radius: 3px; border: 1px solid #ff4646; }
 #limited-account-indicator { background: #ffff002b; color: yellow; padding: 10px 0px 10px 0px; display: inline-block; margin-top: 5px; width: 94%; border-radius: 5px; border: 1px solid yellow; }

 .account-data-cell { text-align: right; background: #ffffff14; padding-right: 10px; word-wrap: anywhere; }
 .account-label-cell { text-align: left; font-weight: bold; background: #ffffff2e; padding: 9px; width: 38px; transition: background 0.2s; user-select: none; }
 .account-flag { padding: 10px 0px 10px 0px; display: inline-block; margin-top: 5px; width: 94%; border-radius: 5px;}
 .optedout-cell { background: #800; font-weight: bold; }

 .bptf-detail-container, .sid-detail-container {margin-top: 5px;background: #00000030;padding: 10px;}
 .module-research-link { background: #ffffff1a; padding: 10px; margin-right: 10px; margin-top: 10px; display: inline-block; border-radius: 4px; width: 160px; font-size: 15px;}
 .module-research-link:hover { background: #ffffff30; transition: all 0.3s;}
 .bp-trust-btn, .bp-issue-btn {width: 160px;}

 .btn-container {display: flex; flex-direction: row; justify-content: center; align-items: center}

 .close { color: white; float: right; font-size: 28px; font-weight: bold;}
 .close:hover, .close:focus {color: yellow; text-decoration: none; cursor: pointer;}

 .text-bold { font-weight: bold; }
 .font-default { font-size: inherit !important; font-family: 'Motiva Sans', Arial, Helvetica, sans-serif !important; }
 .scroll-lock { height: 100%; overflow-y: hidden; }
 .color-error { color: var(--color-error); }
 .color-limited { color: var(--color-limited); }
 .color-warning { color: var(--color-warning); }
 .color-fixed { color: var(--color-fixed); }
 .corner-topleft-5px { border-radius: 5px 0px 0px 0px; }
 .corner-topright-5px { border-radius: 0px 5px 0px 0px; }
 .corner-bottomleft-5px { border-radius: 0px 0px 0px 5px; }
 .corner-bottomright-5px { border-radius: 0px 0px 5px 0px; }
 .label-clickable:hover { background: #ffffff4a; cursor: pointer; }
 .label-clickable:active { background: #ffffff80 }

 .detail-mixed {background: #ff780057 !important;}
 .detail-negative {background: #ff000057 !important;}
 .detail-positive {background: #22c60057 !important;}
 .detail-premium {background: #770ebf57 !important;}

 .hidden {display: none !important;}

 #sh-info-container .history-table td { width: 33%; white-space: nowrap; }
 .history-table, .history-table th, .history-table td, #st-details-table { border-collapse: collapse; }
 .history-table td { padding: 4px; }
 .history-table-head {font-size: 18px; height: 38px; background: #00000036;}
 .history-table tr td:first-child {white-space: nowrap;}
 .history-table-container {max-height: 350px; overflow-y: auto; margin-top: 10px;}
 .history-table, #st-details-table { width: 100%; background: #00000030; text-align: center; overflow-y: auto; height: 100%; max-height: 350px;}

 .modal {display: none; position: fixed; z-index: 900; left: 0; top: 0; width: 100%; height: 100%; overflow: auto; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.8);}
 .modal-content { position: relative; background-color: black; color: white; margin: auto; padding: 20px; width: 1150px; height: 90vh; min-height: 300px; border: 1px solid gray; animation-name: popup-modal; animation-duration: 0.35s}

 .module-info-container { background: #ffffff1a; padding: 10px; margin-top: 10px; border-radius: 5px; color:white; margin-bottom: 20px; }

 .reputation-description { display: inline; }
 .reputation-menu-title { color: white; }

 .research-icon { display: inline-block; width: 16px; height: 16px; margin-right: 5px;}
 .research-site-container > .research-icon {transform: translateY(2px);}
 .module-research-link > .research-icon {transform: translateY(2px); margin-right: 7px;}
 .research-link { margin-left: 5px; padding: 5px; background: #ffffff1a; margin-top: 10px; display: block; width: 100%; text-align: center; margin-right: 5px; border-radius: 3px; }
 .research-link-container { display: flex; flex-direction: row; justify-content: center; align-items: center; }
 .research-link:hover, .simple-site:hover { background: #ffffff30; transition: all 0.3s;}
 .research-site-container { background: #ffffff1a; padding: 10px; margin-right: 10px; margin-top: 10px; display: inline-block; border-radius: 4px; width: 274px; font-size: 15px; color: white;}
 .research-site-label {margin-left: 6px;}

 /* Settings menu */
 form > fieldset:first-child { margin-top: 12px; }
 form > fieldset:last-child { margin-bottom: 12px; }
 #setting-button-container {height: 10%; margin-top: 15px; margin-left: 150px;max-height: 60px;}
 #settings-modal fieldset {border: none;}
 #setting-section-title-container {margin-left: 150px;padding: 0px 10px; margin-bottom: 15px;}
 .setting-textbox {width: 220px;background: white !important;color: black !important;padding: 5px;}
 .setting-title {display: inline; color: white; font-weight: bold; user-select: none;}
 .setting-toggle {appearance: none; background: #ff0000; color: white; border-radius: 5px; padding: 5px; min-width: 36px; text-align:center; font-family: "Motiva Sans",Arial,Helvetica,sans-serif;}
 .setting-toggle-sm {font-size: 10px;font-weight: bold;}
 .setting-toggle:checked {background: #00f000; color: #014401;}
 .setting-toggle:checked:after {content: "ON";}
 .setting-toggle:not(:checked):before {content: "OFF";}
 .setting-toggle:hover, #save-input-btn:hover, #reset-input-btn:hover {cursor: pointer;}
 h2 + .setting-toggle {transform: translateY(-4px);}
 .setting-element-container {border: 1px solid #757575;overflow: auto;margin-left: 150px;padding: 0px 10px;height: calc(100% - 115px);}
 .setting-element-container a, .highlighted-link, #sr-eol-link {color: #36c7ff;}
 .settings-sidebar a {padding: 10px;text-decoration: none;font-size: 20px;color: white;display: block;padding-left: 9px;background: #242424;margin-bottom: 8px;transition: all 0.2s;height: 32px;line-height: 32px;}
 .settings-sidebar a.section-selected {background:#175ca6;}
 .settings-sidebar a:hover {background:#175ca6;font-weight:bold;font-size:22px;}
 .settings-sidebar > a:first-child { margin-top: 15px; }
 .settings-sidebar p {text-align: center}
 .settings-sidebar {height: 100%;width: 150px; top: 0;left: 0;position: absolute; background: #171717;}

 .sid-subdetail {background: #00000030; padding: 10px; margin-top: 10px;}
 .sid-subtitle {font-size: 20px;}
 .sid-subtitle-container {text-align: center; padding: 10px; background: #00000036;}

 .site-summary {background: rgba(0, 0, 0, 0.3) none repeat scroll 0% 0%; margin-bottom: 5px; color:white; animation-name: popup; animation-duration: 0.35s; max-width: 268px;}

 .settings-splitter {border: 1px dashed #ffffff75; margin-top: 5px; margin-top: 12px; margin-bottom: 12px;}
 .splitter {border: 1px solid #ffffff75; margin-top: 5px; margin-bottom: 14px;}

 .sr-research-link {width: 235px;}
 .sr-warning-label {color:orange; display:inline;}

 .status-info-container {padding: 5px;}
 .status-title {font-weight: bold !important; text-shadow: none !important;}
 .status-title-container {padding: 10px; background: #00000069; border-radius: 5px; margin-bottom: 3px;}
 .status-danger { background: #f006; color: var(--color-danger); }
 .status-warning { background: #c84b00d4; }
 .status-light { background: #68696ad4; }
 .status-normal { background: #ffffff1a; }
 .status-special { background: #2cff0066; }
 .status-noaccount { background: #4b4b4bd4; }
 .status-noaccount .bptf-detail-container { background: #00000050; }
 .status-warning .bptf-detail-container { background: #00000042; }

 .summary-status {display: inline-block;width: 158px;text-align: center;height: 35px;line-height: 35px;}
 .summary-status-danger  { background: #f006; color:yellow; }
 .summary-status-light { background: #68696ad4; color:white; }
 .summary-status-noaccount { background: #4b4b4bd4; color:white; }
 .summary-status-normal  { background: none; color:white; }
 .summary-status-special { background: #2cff0066; color:white; }
 .summary-status-warning { background: #c84b00d4; color:white; }
 .summary-title {background: #ffffff0d;display: inline-block;line-height: 35px;height: 35px;width: 110px;text-align: center; transition: background 0.3s;}
 .summary-title:hover {background: #ffffff45; cursor:pointer;}

 .rep-summary {text-align: center; color: white; border-collapse: collapse; table-layout: fixed; width: 268px; animation-name: popup; animation-duration: 0.35s; margin-top: -5px; margin-bottom: 5px;}
 .rep-summary-positive {background: #1fb50057; width: 77px;}
 .rep-summary-negative {background: #ff000057; width: 77px;}
 .rep-summary-title {width: 108px;line-height: 25px;background: #69696973;}

 .title-danger { color: yellow !important; }
 .title-normal, .title-special, .title-warning, .title-light, .title-noaccount { color: white !important; }
 .trust-summary {width: 79px;display: inline-block;text-align: center;line-height: 25px;}
 .trust-summary-negative {background: #ff000057;}
 .trust-summary-positive {background: #22c60057;}

 .username-label-banned {text-decoration: line-through; color: red;}
 .username-label-special {color: rgb(130, 255, 103); font-weight: bold;}

 /* Adjustments for long URLs */
 @media only screen and (min-width: 911px) {
    .url-max-long {font-size: 11px !important;}
    .url-extra-long {font-size: 13px !important;}
    .url-long {padding-right: 0px; text-align: center;}
 }

 /* Styles for smaller screens */
 @media only screen and (max-width: 1200px) {
    .modal-content {width: 90%;}
 }

 @media only screen and (max-width: 900px) {
    .settings-sidebar {width: 100px;}
    .settings-sidebar a {padding-left: 5px;font-size: 18px;}
    .setting-element-container, #setting-section-title-container, #setting-button-container {margin-left: 100px;}
 }

 @media only screen and (max-width: 393px) {
    #save-input-btn, #reset-input-btn {width: 40%;}
    .setting-element-container {height: calc(100% - 155px);}
 }

 /* Animations */
 @keyframes popup {
  from {transform: scale(0.1);}
  to {transform: scale(1);}
 }

 @keyframes popup-modal {
  from {transform: scaleX(0.1);}
  to {transform: scaleX(1);}
 }

`);

buildSummaryContainer();
buildDetailsMenu();
buildSettingsModal();
getSteamInfo();